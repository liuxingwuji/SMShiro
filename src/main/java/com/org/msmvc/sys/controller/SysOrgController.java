package com.org.msmvc.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.org.msmvc.sys.entity.SysOrg;
import com.org.msmvc.sys.service.SysOrgService;
import org.springframework.web.servlet.support.RequestContext;

/**
 * @author Administrator
 * @desc 组织管理
 * @DATE 2017年5月22日
 */
@Controller
@RequestMapping("sysOrg")
public class SysOrgController {
	@Autowired
	private SysOrgService sysOrgService;
	
	@RequestMapping(value = "/query/{operate}")
	public ModelAndView execute(@PathVariable("operate") String operate,HttpServletRequest request,
			HttpServletResponse response) throws  Exception {
		ModelAndView model = new ModelAndView();
		Map<String, Object> params = new HashMap<String, Object>();
		if (operate.equals("toOrgList")) {//列表显示
			model.setViewName("/org/orgList");
		} else if(operate.equals("toInsertOrg")){
			model.addObject("parentCode", request.getParameter("parentCode"));
			model.setViewName("/org/insertOrg");
		}else if(operate.equals("toUpdateOrg")){
			params.put("code",request.getParameter("code"));
			model.addObject("org", sysOrgService.getOrgList(params).get(0));
			model.setViewName("/org/updateOrg");
		}
		return model;
	}
	
	@RequestMapping("/listOrg")
	@ResponseBody
	public Map<String, Object> listJson(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}
		
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
				// 当前页
		String page = request.getParameter("page");
				// ($page-1)*$pagesize
		int startIndex = (Integer.valueOf(page) - 1) * Integer.valueOf(pageSize);
		params.put("pageNum", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		Map<String, Object> map = new HashMap<String, Object>();
		map=sysOrgService.queryOrgList(params);
		return map;
		
	}
	/**
	 * 组织树展示
	 *
	 * @return
	 */
	@RequestMapping(value = "/orgTree")
	@ResponseBody
	public List orgTree(HttpServletRequest request,
						HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Map<String, Object> params = new HashMap<String, Object>();
	/*	Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}*/
		params.put("parentCode",request.getParameter("id")!=null?request.getParameter("id"):"");
		List list = sysOrgService.orgTree(params);

		return list;
	}
	/**
	 * 新增菜单
	 * @param sysOrg
	 * @return
	 */
	@RequestMapping("/insertOrg")
	@ResponseBody
	public Map insertOrg(@ModelAttribute("sysOrg")SysOrg sysOrg,HttpServletRequest request,
						 HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		map.put("parentCode",sysOrg.getParentCode());
		i = sysOrgService.insertOrg(sysOrg,map);
		//从后台代码获取国际化信息
		RequestContext requestContext = new RequestContext(request);
		if(i>0){
			flag=true;
			message=requestContext.getMessage("org_add_success");
		}else{
			message=requestContext.getMessage("org_add_false");
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 新增菜单
	 * @param sysOrg
	 * @return
	 */
	@RequestMapping("/updateOrg")
	@ResponseBody
	public Map updateOrg(@ModelAttribute("sysOrg")SysOrg sysOrg,HttpServletRequest request,
						 HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		String id=request.getParameter("ids");
		sysOrg.setId(Integer.parseInt(id));
		boolean flag=false;
		String message="";
		int i = 0;
		i = sysOrgService.updateOrg(sysOrg);
		//从后台代码获取国际化信息
		RequestContext requestContext = new RequestContext(request);
		if(i>0){
			flag=true;
			message=requestContext.getMessage("org_update_success");
		}else{
			message=requestContext.getMessage("org_update_false");
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 根据主键ID删除组织
	 * @param code
	 * @return
	 */
	@RequestMapping(value="/deleteByCode", method = RequestMethod.POST)
	@ResponseBody
	public Map deleteByID(@RequestParam("code") String code ,HttpServletRequest request,
						  HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = sysOrgService.deleteByCode(code);
		RequestContext requestContext = new RequestContext(request);
		if(i>0){
			flag=true;
			message=requestContext.getMessage("org_del_success");
		}else{
			message=requestContext.getMessage("org_del_success");
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
}
