package com.org.msmvc.sys.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.org.msmvc.sys.entity.Roles;
import com.org.msmvc.sys.service.RolesService;
/**
 * 角色管理
 * @author  zsh
 */
@Controller
@RequestMapping("mroles")
public class RolesController {

	@Autowired
	private RolesService rolesService;

	@RequestMapping(value = "/query/{operate}")
	public ModelAndView execute(@PathVariable("operate") String operate,HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		if (operate.equals("tolist")) {
			model.setViewName("/roles/listRole");
		} else if (operate.equals("add")) {
			model.setViewName("/roles/addRole");
		} else if (operate.equals("edit")) {
			String id=request.getParameter("id");
			Roles role=rolesService.getById(Integer.valueOf(id));
			model.addObject("role", role);
			model.setViewName("/roles/editRole");
		} else if (operate.equals("show")) {
			String id=request.getParameter("id");
			Roles role=rolesService.getById(Integer.valueOf(id));
			model.addObject("role", role);
			model.setViewName("/roles/showRole");
		}else if (operate.equals("authority")) {
			String id=request.getParameter("id");
			Roles role=rolesService.getById(Integer.valueOf(id));
			List<String> rAuthority=rolesService.getRoleAuthority(role.getCode());
			String u="";
			if(rAuthority.size()>0){
				for(String ur:rAuthority){
					u+=ur+",";
				}
			}
			model.addObject("RA", u);
			model.addObject("role", role);
			model.setViewName("/roles/roleAuthority");
		}
		return model;
	}

	@RequestMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> listJson(HttpServletRequest request,
			HttpServletResponse response) {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", request.getParameter("name"));

		long totalCount = rolesService.pageCounts(params); // 获取记录总数
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
		// 当前页
		String page = request.getParameter("page");
		// ($page-1)*$pagesize
		int startIndex = (Integer.valueOf(page) - 1) * Integer.valueOf(pageSize);
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		List<Roles> roles = rolesService.pageList(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", roles);
		map.put("Total", totalCount);
		return map;
	}

	
	
	/**
	 * 新增角色信息
	 * @param tree
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	public Map saveRoles(@ModelAttribute("role")Roles role){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = rolesService.insert(role);
		if(i>0){
			flag=true;
			message="角色新增成功";
		}else{
			message="角色新增失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 修改角色信息
	 * @param tree
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map updatRoles(@ModelAttribute("role")Roles role){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = rolesService.update(role);
		if(i>0){
			flag=true;
			message="角色更新成功";
		}else{
			message="角色更新失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 删除角色信息
	 * @param tree
	 * @return
	 */
	@RequestMapping(value="/deletes", method = RequestMethod.POST)
	@ResponseBody
	public Map delTree(HttpServletRequest request,HttpServletResponse response ){
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}
		boolean flag=false;
		String message="";
		int i = 0;
		try {
			i = rolesService.deleteById(params);
			if(i>0){
				flag=true;
				message="角色删除成功";
			}else{
				message="角色删除失败";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message=e.getMessage();
		}
		
		params.put("message",message);
		params.put("flag",flag);
		return params;
	}
	
	/**
	 * 角色权限保存
	 * @param tree
	 * @return
	 */
	@RequestMapping(value="/saveAuthority", method = RequestMethod.POST)
	@ResponseBody
	public Map saveRolesAuthority(HttpServletRequest request,
			HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = rolesService.insertRoleAuthority(request.getParameter("roleId"),request.getParameter("authority"));
		if(i>0){
			flag=true;
			message="角色权限更新成功";
		}else{
			message="角色权限更新失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	
	@RequestMapping(value = "/getRole")
	@ResponseBody
	public List getRole(HttpServletRequest request,
			HttpServletResponse response) {
		
		List roles = rolesService.selectAll();

		return roles;
	}
	
	/**
	 * 角色权限
	 * @param tree
	 * @return
	 */
	@RequestMapping(value="/saveUserRole", method = RequestMethod.POST)
	@ResponseBody
	public Map saveUserRole(HttpServletRequest request,
			HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = rolesService.insertUserRole(request.getParameter("userId"),request.getParameter("roleIds"));
		if(i>0){
			flag=true;
			message="用户角色保存成功";
		}else{
			message="用户角色保存失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	
}
