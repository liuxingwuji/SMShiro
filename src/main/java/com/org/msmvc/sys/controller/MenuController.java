package com.org.msmvc.sys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.org.msmvc.sys.entity.Menu;
import com.org.msmvc.sys.entity.SysUser;
import com.org.msmvc.sys.service.MenuService;
/**
 * 菜单管理
 * @author  zsh
 */
@Controller
@RequestMapping("menu")
public class MenuController {

	@Autowired
	private MenuService menuService;

	
	/* String currentUser = (String)request.getSession().getAttribute("currentUser");  
     System.out.println("当前登录的用户为[" + currentUser + "]");  
     request.setAttribute("currUser", currentUser)*/
	
	@RequestMapping(value = "/query/{operate}")
	public ModelAndView execute(@PathVariable("operate") String operate,HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		if (operate.equals("tolist")) {
			model.setViewName("/menu/listMenu");
		} else if (operate.equals("add")) {
			model.addObject("parentId", request.getParameter("parentId"));
			model.setViewName("/menu/addMenu");
		} else if (operate.equals("edit")) {
			//String id=request.getParameter("id");
			//Menu tree=menuService.getById(Integer.valueOf(id));
			String menuId=request.getParameter("menuID");
			Menu tree=menuService.findByMenuId(menuId);
			model.addObject("menu", tree);
			model.setViewName("/menu/editMenu");
		} else if (operate.equals("show")) {
			String id=request.getParameter("id");
			Menu tree=menuService.getById(Integer.valueOf(id));
			model.addObject("menu", tree);
			model.setViewName("/menu/showMenu");
		}else if(operate.equals("listTree")){
			model.setViewName("/menu/menuTree");
		}
		return model;
	}

	@RequestMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> listJson(HttpServletRequest request,
			HttpServletResponse response) {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("treeName", request.getParameter("treeName"));

		long totalCount = menuService.pageCounts(params); // 获取记录总数
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
		// 当前页
		String page = request.getParameter("page");
		// ($page-1)*$pagesize
		int startIndex = (Integer.valueOf(page) - 1) * Integer.valueOf(pageSize);
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		List<SysUser> users = menuService.pageTreeList(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", users);
		map.put("Total", totalCount);
		return map;
	}

	/**
	 * 菜单树展示
	 * 
	 * @return
	 */
	@RequestMapping(value = "/showTree")
	@ResponseBody
	public List showTree(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		String login = SecurityUtils.getSubject().getPrincipal().toString();
		List<Menu>list=null;
		if(login.equals("admin")){
			list = menuService.selectAll();
		}else{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", login);
			 list = menuService.getMenuByUserId(map);
		}
		

		return list;
	}
	
	/**
	 * 新增菜单
	 * @param tree
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	public Map saveTree(@ModelAttribute("tree")Menu tree){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = menuService.insertTree(tree);
		if(i>0){
			flag=true;
			message="菜单新增成功";
		}else{
			message="菜单新增失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 修改菜单
	 * @param menu
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map updatMenu(@ModelAttribute("menu")Menu menu){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = menuService.updateTree(menu);
		if(i>0){
			flag=true;
			message="菜单更新成功";
		}else{
			message="菜单更新失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	/**
	 * 根据主键ID删除菜单
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteByID", method = RequestMethod.POST)
	@ResponseBody
	public Map delById(@RequestParam("id") String id ){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = menuService.deleteById(Integer.valueOf(id));
		if(i>0){
			flag=true;
			message="菜单删除成功";
		}else{
			message="菜单删除失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	
	/**
	 * 根据菜单编号删除菜单
	 * @param mId
	 * @return
	 */
	@RequestMapping(value="/deleteByMID", method = RequestMethod.POST)
	@ResponseBody
	public Map delByMID(@RequestParam("mId") String mId ){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = menuService.deleteByMId(mId);
		if(i>0){
			flag=true;
			message="菜单删除成功";
		}else{
			message="菜单删除失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}
	
	/**
	 * 菜单树展示
	 * 
	 * @return
	 */
	@RequestMapping(value = "/menuTree")
	@ResponseBody
	public List menuTree() {
		ModelAndView model = new ModelAndView();

		List list = menuService.menuTree("");

		return list;
	}
}
