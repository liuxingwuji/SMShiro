package com.org.msmvc.sys.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.org.util.DateUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.org.msmvc.sys.service.UserService;
import com.org.msmvc.sys.service.UtilService;

/**
 * 文件导出
 * 
 * @author zsh
 *
 */
@Controller
@RequestMapping("exports")
public class ExportController {
	@Autowired
	private UtilService utilService;
	@Autowired
	private UserService userService;
	
	/**
	 * 学时购买报表导出
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/exportUser")
	@ResponseBody
	private String exportUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, String> XlsExportColumnNameIndexMappingOf_ContractChange = new LinkedHashMap<String, String>();
		XlsExportColumnNameIndexMappingOf_ContractChange.put("帐号", "username");
		XlsExportColumnNameIndexMappingOf_ContractChange.put("姓名", "name");
		XlsExportColumnNameIndexMappingOf_ContractChange.put("性别", "sex");
		XlsExportColumnNameIndexMappingOf_ContractChange.put("邮箱", "email");
		XlsExportColumnNameIndexMappingOf_ContractChange.put("注册时间", "rtime");
		
		// 导出类型
		String xlsType = request.getParameter("xlsType");
		if (null == xlsType) {
			xlsType = "1";// 默认xls 2003
		}
		OutputStream out;
		try {
			out = response.getOutputStream();
			// 获取calendar实例;
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, -1);
			String exportFileName = "用户信息" + DateUtils.getString_yyyyMMdd_Form(c.getTime());// 导出的文件名称
			/*
			 * if (xlsType.equals("2")) { exportFileName = "三免一报名" + dateName +
			 * ".xlsx";// xlsx 2007 }
			 */
			response.reset();// 清空输出流
			response.setHeader("Content-disposition",
					"attachment; filename=" + new String(exportFileName.getBytes("GB2312"), "ISO_8859_1") + ".xls");// 设定输出文件头
			response.setContentType("application/vnd.ms-excel;charset=utf-8");
			Map<String, Object> params = new HashMap<String, Object>();// 记录集合查询条件
			Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
			String values = "";
			for (String key : getParams.keySet()) {
				values = Arrays.toString(getParams.get(key));
				params.put(key, values.substring(1, values.lastIndexOf("]")));
			}
			params.put("isPage", false);
			// 查询记录信息
			params.put("state", 1);
			List<Map<String, Object>> dataList = userService.exportUser(params);
			Workbook excel = utilService.exportExcel(xlsType, dataList, exportFileName,
					XlsExportColumnNameIndexMappingOf_ContractChange);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			excel.write(os);
			byte[] content = os.toByteArray();
			InputStream is = new ByteArrayInputStream(content);

			BufferedInputStream bis = null;
			BufferedOutputStream bos = null;
			try {
				bis = new BufferedInputStream(is);
				bos = new BufferedOutputStream(out);
				byte[] buff = new byte[2048];
				int bytesRead;
				// Simple read/write loop.
				while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
					bos.write(buff, 0, bytesRead);
				}
			} catch (final IOException e) {
				throw e;
			} finally {
				if (bis != null)
					bis.close();
				if (bos != null)
					bos.close();
			}
			return "数据导出成功";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
