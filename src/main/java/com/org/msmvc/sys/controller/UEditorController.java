package com.org.msmvc.sys.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.org.msmvc.sys.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.baidu.ueditor.ActionEnter;
/**
 * 
 * author zsh 
 * desc 信息新闻发布
 * 
 * 
 * */

@Controller
@RequestMapping("ueditor")
public class UEditorController {
	@Autowired
	NoticeService noticeService;
	@RequestMapping(value = "/query/{operate}")
	public ModelAndView execute(@PathVariable("operate") String operate,HttpServletRequest request,
								HttpServletResponse response) throws NumberFormatException, Exception {
		ModelAndView model = new ModelAndView();
		Map<String, Object> params = new HashMap<String, Object>();
		if (operate.equals("tolist")) {//列表显示
			model.setViewName("/news/listNews");
		}else if(operate.equals("toAddNotice")){//发布通知
			model.setViewName("/baidu/notice");
		}else if(operate.equals("toEditNotice")){//修改通知
			String id=request.getParameter("id");
			params.put("id",id);
			model.addObject("notices", noticeService.getNoticeDetail(params));
			model.setViewName("/baidu/noticeEdit");
		}else if(operate.equals("toShowNotice")){
			String id=request.getParameter("id");
			params.put("id",id);
			model.addObject("notices", noticeService.getNoticeDetail(params));
			model.setViewName("/news/showNews");
		}else if (operate.equals("toBannerlist")) {//网站banner列表显示
			model.setViewName("/banner/listBanner");
		}else if (operate.equals("toAddBanner")) {//网站banner列表显示
			model.setViewName("/banner/addBanner");
		}else if (operate.equals("toUpdateBanner")) {//网站banner列表显示
			model.setViewName("/banner/updateBanner");
		}else if(operate.equals("toUpBannerImg")){//密码修改
			//model.addObject("userIDS", request.getParameter("userIDS"));
			model.setViewName("/banner/upBannerImg");
		}
		return  model;
	}
	@RequestMapping({ "/ueditor"})
	@ResponseBody
    public  String ueUpload(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
		//这里就是把controller.jsp代码copy下来
        request.setCharacterEncoding( "utf-8" );
        response.setHeader("Content-Type" , "text/html");
        String roolPath = request.getSession().getServletContext().getRealPath("/");
        String configStr = new ActionEnter(request, roolPath).exec();
        return configStr;
    }


	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> listJson(HttpServletRequest request,
										HttpServletResponse response) throws Exception {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}

		long totalCount = noticeService.pageCounts(params); // 获取记录总数
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
		// 当前页
		String page = request.getParameter("page");
		//($page-1)*$pagesize
		int startIndex=(Integer.valueOf(page)-1)*Integer.valueOf(pageSize);
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		List noticeList = noticeService.pageNoticeList(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", noticeList);
		map.put("Total", totalCount);
		return map;
	}

	@RequestMapping("/pageList")
	@ResponseBody
	public Map<String, Object> jqueryPagingnation(HttpServletRequest request,
										HttpServletResponse response) throws Exception {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}
		// 每页显示的记录数
		String pageSize = request.getParameter("pageSize");
		// 当前页
	//	String page =request.getParameter("page");
		//($page-1)*$pagesize
		//int startIndex=(Integer.valueOf(page)-1)*Integer.valueOf(pageSize);
		int startIndex=Integer.valueOf(request.getParameter("pageIndex"));
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		long totalCount = noticeService.pageCounts(params); // 获取记录总数
		List noticeList = noticeService.pageNoticeList(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", noticeList);
		map.put("total", totalCount);
		return map;
	}

	@RequestMapping(value = "insertNotice", method = RequestMethod.POST)
	@ResponseBody
	public Map<String , Object> insertNotice(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));

		}
		String message="";
		boolean flag=false;
		int i=noticeService.insertNotice(params);
		if(i>0){
			flag=true;
			message="通知发布成功！！！";
		}else{
			flag=false;
			message="通知发布失败！！！";
		}
		params.put("message",message);
		params.put("flag",flag);
		return params;
	}
	@RequestMapping(value = "updateNotice", method = RequestMethod.POST)
	@ResponseBody
	public Map<String , Object> updateNotice(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));

		}
		String message="";
		boolean flag=false;
		int i=noticeService.updateNotice(params);
		if(i>0){
			flag=true;
			message="通知修改成功！！！";
		}else{
			flag=false;
			message="通知修改失败！！！";
		}
		params.put("message",message);
		params.put("flag",flag);
		return params;
	}
	@RequestMapping(value = "/delNotice", method = RequestMethod.POST)
	@ResponseBody
	public Map delNotice(@RequestParam("id") String id ) throws NumberFormatException, Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int i = 0;
		i = noticeService.deleteById(Integer.valueOf(id));
		if(i>=0){
			map.put("message","删除成功");
		}else{
			map.put("message","请确认数据是否存在");
		}
		return map;
	}

	@RequestMapping("/noticeView")
	@ResponseBody
	public Map<String, Object> noticeView(HttpServletRequest request,
										HttpServletResponse response) throws Exception {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id",request.getParameter("id"));
		//map.put("notices", noticeService.getNoticeDetail(params));

		return noticeService.getNoticeDetail(params);
	}
	/***************************************** 网站banner **********************************/

	@RequestMapping("/listBanner")
	@ResponseBody
	public Map<String, Object> listBannerJson(HttpServletRequest request,
										HttpServletResponse response) throws Exception {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));
		}

		long totalCount = noticeService.pageBannerCounts(params); // 获取记录总数
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
		// 当前页
		String page = request.getParameter("page");
		//($page-1)*$pagesize
		int startIndex=(Integer.valueOf(page)-1)*Integer.valueOf(pageSize);
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		List noticeList = noticeService.pageBannerList(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", noticeList);
		map.put("Total", totalCount);
		return map;
	}

	@RequestMapping(value = "insertBanner", method = RequestMethod.POST)
	@ResponseBody
	public Map<String , Object> insertBanner(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));

		}
		String message="";
		boolean flag=false;
		int i=noticeService.insertBanner(params);
		if(i>0){
			flag=true;
			message="Banner发布成功！！！";
		}else{
			flag=false;
			message="Banner发布失败！！！";
		}
		params.put("message",message);
		params.put("flag",flag);
		return params;
	}
	@RequestMapping(value = "updateBanner", method = RequestMethod.POST)
	@ResponseBody
	public Map<String , Object> updateBanner(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String[]> getParams = request.getParameterMap(); // 获取查询条件信息
		String values = "";
		for (String key : getParams.keySet()) {
			values = Arrays.toString(getParams.get(key));
			params.put(key, values.substring(1, values.lastIndexOf("]")));

		}
		String message="";
		boolean flag=false;
		int i=noticeService.updateBanner(params);
		if(i>0){
			flag=true;
			message="Banner修改成功！！！";
		}else{
			flag=false;
			message="Banner修改失败！！！";
		}
		params.put("message",message);
		params.put("flag",flag);
		return params;
	}

	@RequestMapping(value = "/delBanner", method = RequestMethod.POST)
	@ResponseBody
	public Map delBanner(@RequestParam("id") String id ) throws NumberFormatException, Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int i = 0;
		i = noticeService.deleteBannerById(Integer.valueOf(id));
		if(i>=0){
			map.put("message","删除成功");
		}else{
			map.put("message","请确认数据是否存在");
		}
		return map;
	}
}
