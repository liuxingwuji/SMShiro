package com.org.msmvc.sys.controller;

import com.org.msmvc.sys.entity.SysDictDetailDTO;
import com.org.msmvc.sys.service.SysDictDetailService;
import com.org.msmvc.sys.service.impl.SysDictDetailServiceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
/**
 * 数据字典详细信息
 * @author zsh
 *
 */
@Controller
@RequestMapping(value="sysDictDetail")
public class SysDictDetailController extends BaseController {
	private static final Logger logger = LoggerFactory
			.getLogger(SysDictDetailController.class);
	@Autowired
	private SysDictDetailService sysDictDetailService;

	@RequestMapping({ "/query/{operate}" })
	public ModelAndView execute(@PathVariable("operate") String operate) {
		ModelAndView model = new ModelAndView();
		if ("toQueryList".equals(operate)) {
			String dictId = getParameterString("dictId");
			String type = getParameterString("type");
			if ((dictId != null) && (dictId.length() > 0)) {
				model.addObject("dictId", dictId);
			}
			if ((type != null) && (type.length() > 0)) {
				model.addObject("type", type);
			}
			model.setViewName("/sysdict/sysDictDetail/querySysDictDetail");
		} else if ("toAdd".equals(operate)) {
			String dictId = getParameterString("dictId");
			if ((dictId != null) && (dictId.length() > 0)) {
				model.addObject("dictId", dictId);
			}
			model.setViewName("/sysdict/sysDictDetail/addSysDictDetail");
		} else if ("toUpdate".equals(operate)) {
			String id = getParameterString("id");
			model = queryOneDTO(id);
			model.setViewName("/sysdict/sysDictDetail/updateSysDictDetail");
		} else if ("toView".equals(operate)) {
			String id = getParameterString("id");
			model = queryOneDTO(id);
			model.setViewName("/sysdict/sysDictDetail/viewSysDictDetail");
		}
		return model;
	}

	@RequestMapping(value="/queryListSysDictDetail" )
	@ResponseBody
	public Map queryListSysDictDetail(HttpServletRequest request,
			SysDictDetailDTO dto)
			throws Exception {
		/**
		 * 想要在页面展现数据,必须返回ModelAndView类型,返回String是不能获取数据的
		 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dto", dto);

		// long totalCount = rolesService.pageCounts(params); // 获取记录总数
		// 每页显示的记录数
		String pageSize = request.getParameter("pagesize");
		// 当前页
		String page = request.getParameter("page");
		// ($page-1)*$pagesize
		int startIndex = (Integer.valueOf(page) - 1)
				* Integer.valueOf(pageSize);
		params.put("startIndex", startIndex);
		params.put("pageSize", Integer.valueOf(pageSize));
		List<SysDictDetailDTO> list = sysDictDetailService.searchSysDictDetailByPaging(params);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Rows", list);
		map.put("Total", list.size());
		return map;
	}

	/*public ModelAndView querySysDictMaxOrderBy(String dictId)
			throws AbaboonException {
		ModelAndView model = new ModelAndView();
		try {
			String url = this.jyptURL
					+ "/api/platform/sysdict/SysDictDetailRest/findMaxOrderBy/v1/"
					+ dictId;

			ResponseMsg<String> responseMsg = RestClient.doGet(this.jyptAppId,
					url, new TypeReference() {
					});
			if ("200".equals(responseMsg.getRetCode())) {
				model.addObject("maxOrderBy", responseMsg.getResponseBody());
			}
		} catch (Exception e) {
			throw new AbaboonException("执行querySysDictMaxOrderBy异常：", e);
		}
		return model;
	}*/

	//@RequestMapping({ "/queryDetailCodeIsOk" })
	//@ResponseBody
	/*public DataMsg queryDetailCodeIsOk(HttpServletRequest request,
			SysDictDetailDTO dto, @ModelAttribute DataMsg dataMsg)
			throws AbaboonException {
		dataMsg = super.initDataMsg(dataMsg);
		try {
			String code = getParameterString("code");
			String dictId = getParameterString("dictId");
			if ((code != null) && (code.length() > 0) && (dictId != null)
					&& (dictId.length() > 0)) {
				QueryReqBean params = new QueryReqBean();
				Map<String, Object> searchParams = new HashMap();
				dto.setDictDetailName(code);
				dto.setDictId(Long.valueOf(Long.parseLong(dictId)));
				searchParams.put("dto", dto);
				params.setSearchParams(searchParams);
				String url = this.jyptURL
						+ "/api/platform/sysdict/SysDictDetailRest/queryDetailCodeIsOk/v1/";

				ResponseMsg<String> responseMsg = RestClient.doPost(
						this.jyptAppId, url, params, new TypeReference() {
						});
				if ("200".equals(responseMsg.getRetCode())) {
					dataMsg.setData(responseMsg.getResponseBody());
				} else {
					throw new AbaboonException("执行queryDetailCodeIsOk异常：");
				}
			} else {
				dataMsg.setData(Integer.valueOf(0));
			}
		} catch (Exception e) {
			throw new AbaboonException("执行queryDetailCodeIsOk异常：", e);
		}
		return dataMsg;
	}*/

	@RequestMapping({ "/insertSysDictDetail" })
	@ResponseBody
	public Map insertSysDictDetail(HttpServletRequest request,
			SysDictDetailDTO dto) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = false;
		String message = "";
		int i = 0;
		i = sysDictDetailService.insertSysDictDetail(dto);
		if (i > 0) {
			flag = true;
			message = "数据新增成功";
		} else {
			message = "数据新增失败";
		}
		map.put("message", message);
		map.put("flag", flag);
		return map;
	}

	@RequestMapping({ "/updateSysDictDetail" })
	@ResponseBody
	public Map updateSysDictDetail(HttpServletRequest request,
			SysDictDetailDTO dto) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = sysDictDetailService.updateSysDictDetail(dto);
		if(i>0){
			flag=true;
			message="数据更新成功";
		}else{
			message="数据更新失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}

	@RequestMapping({ "/deleteSysDictDetail" })
	@ResponseBody
	public Map deleteSysDictDetail(HttpServletRequest request) throws Exception {
		String id = (String) getParameter("id");
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag=false;
		String message="";
		int i = 0;
		i = sysDictDetailService.deleteSysDictDetailByPrimaryKey(null, id);
		if(i>0){
			flag=true;
			message="数据删除成功";
		}else{
			message="数据删除失败";
		}
		map.put("message",message);
		map.put("flag",flag);
		return map;
	}

	private ModelAndView queryOneDTO(String id) {
		ModelAndView model = new ModelAndView();
		try {

			SysDictDetailDTO dto = (SysDictDetailDTO) sysDictDetailService.querySysDictDetailByPrimaryKey(id);
			model.addObject("dto", dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
}
