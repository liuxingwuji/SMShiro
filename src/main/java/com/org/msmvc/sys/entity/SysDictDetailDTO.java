package com.org.msmvc.sys.entity;
/**
 * 数据字典明细表
 * @author zsh
 *
 */
public class SysDictDetailDTO extends BaseDTO{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long dictId;
	private String dictDetailName;
	private String dictDetailValue;
	private String orderBy;
	private String validateState;
	private Long version;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDictId() {
		return dictId;
	}
	public void setDictId(Long dictId) {
		this.dictId = dictId;
	}
	public String getDictDetailName() {
		return dictDetailName;
	}
	public void setDictDetailName(String dictDetailName) {
		this.dictDetailName = dictDetailName;
	}
	public String getDictDetailValue() {
		return dictDetailValue;
	}
	public void setDictDetailValue(String dictDetailValue) {
		this.dictDetailValue = dictDetailValue;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getValidateState() {
		return validateState;
	}
	public void setValidateState(String validateState) {
		this.validateState = validateState;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
}
