package com.org.msmvc.sys.entity;

public class SysDictDetailVo {
	private static final long serialVersionUID = 1L;
	  private String dictDetailName;
	  private String dictDetailValue;
	  private String orderBy;
	  
	  public String getDictDetailName()
	  {
	    return this.dictDetailName;
	  }
	  
	  public void setDictDetailName(String dictDetailName)
	  {
	    this.dictDetailName = dictDetailName;
	  }
	  
	  public String getDictDetailValue()
	  {
	    return this.dictDetailValue;
	  }
	  
	  public void setDictDetailValue(String dictDetailValue)
	  {
	    this.dictDetailValue = dictDetailValue;
	  }
	  
	  public String getOrderBy()
	  {
	    return this.orderBy;
	  }
	  
	  public void setOrderBy(String orderBy)
	  {
	    this.orderBy = orderBy;
	  }
}
