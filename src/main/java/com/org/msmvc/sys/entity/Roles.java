package com.org.msmvc.sys.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 角色
 *
 * @author zsh
 */
public class Roles {
    private Integer id;
    private String code;
    private String name;
    private String remark;
    private List<Menu> permissionList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Menu> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Menu> permissionList) {
        this.permissionList = permissionList;
    }
}
