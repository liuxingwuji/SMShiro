package com.org.msmvc.sys.entity;

import java.util.List;

public class SysDictVo {
	 private static final long serialVersionUID = 1L;
	  private String dictCode;
	  private String dictName;
	  private List sysDictDetail;
	  
	  public String getDictCode()
	  {
	    return this.dictCode;
	  }
	  
	  public void setDictCode(String dictCode)
	  {
	    this.dictCode = dictCode;
	  }
	  
	  public String getDictName()
	  {
	    return this.dictName;
	  }
	  
	  public void setDictName(String dictName)
	  {
	    this.dictName = dictName;
	  }
	  
	  public List getSysDictDetail()
	  {
	    return this.sysDictDetail;
	  }
	  
	  public void setSysDictDetail(List sysDictDetail)
	  {
	    this.sysDictDetail = sysDictDetail;
	  }
}
