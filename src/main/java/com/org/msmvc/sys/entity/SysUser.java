package com.org.msmvc.sys.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * 用户信息
 * @author zsh
 *
 */
public class SysUser {
	private Integer id;

	private String userName;
	
	private String name;

	private String  password;
	
	private String sex;
	
	private String email;
	
	private String status;

	private String descn;
	
	private Date createDate;
	
	private List<Roles> roleSet ;

	private String img;

	private String salt; //加密密码的盐

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescn() {
		return descn;
	}

	public void setDescn(String descn) {
		this.descn = descn;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<Roles> getRoleSet() {
		return roleSet;
	}

	public void setRoleSet(List<Roles> roleSet) {
		this.roleSet = roleSet;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getCredentialsSalt() {
		return userName + salt;
	}
}
