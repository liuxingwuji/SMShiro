package com.org.msmvc.sys.entity;
/**
 * 用户角色权限
 * @author zsh
 *
 */
public class UserRole {
	private int id;
	private String userName;
	private String userRoles;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String userRoles) {
		this.userRoles = userRoles;
	}

}
