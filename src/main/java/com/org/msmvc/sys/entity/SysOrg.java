package com.org.msmvc.sys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author zsh
 * @desc 部门管理
 * @DATE 2017年5月22日
 */
@Entity
@Table(name="sys_org")
public class SysOrg {
	@Id
	private int id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "code",unique = true,nullable = false)
	private String code;
	@Column(name = "parent_code", nullable = true)
	private String parentCode;
	@Column(name = "state", nullable = true)
	private int state=1;// 0无效 1有效
	@Column(name = "descs", nullable = true)
	private String descs;// 简介
	@Column(name = "creater", nullable = true)
	private String creater;
	@Column(name = "create_date", nullable = false)
	private Date createDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getDescs() {
		return descs;
	}
	public void setDescs(String descs) {
		this.descs = descs;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	

}
