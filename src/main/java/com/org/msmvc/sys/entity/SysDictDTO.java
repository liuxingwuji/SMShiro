package com.org.msmvc.sys.entity;
/**
 * 数据字典
 * @author zsh
 *
 */
public class SysDictDTO extends BaseDTO{
	private static final long serialVersionUID = 1L;
	private Long id;
	private String dictCode;
	private String dictName;
	private String dictType;
	private String validateState;
	private Long version;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDictCode() {
		return dictCode;
	}
	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}
	public String getDictName() {
		return dictName;
	}
	public void setDictName(String dictName) {
		this.dictName = dictName;
	}
	public String getDictType() {
		return dictType;
	}
	public void setDictType(String dictType) {
		this.dictType = dictType;
	}
	public String getValidateState() {
		return validateState;
	}
	public void setValidateState(String validateState) {
		this.validateState = validateState;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
}
