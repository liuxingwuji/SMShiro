package com.org.msmvc.sys.dao.common;

import java.util.List;
import java.util.Map;

/*
 * 通用的实体类处理
 * zsh
 */
public interface EntityMapper {
	/**
	 *  插入数据，新数据的id会保持在input.id
	 * @param input
	 * @return
	 */
	int insert(InputData input);

	/**
	 *  修改数据
	 * @param input
	 */
	int update(InputData input);

	/**
	 *  查询（因为缓存的问题，放到这里来。修改、查询放到一起，缓存才能及时更新）
	 * @param condition
	 * @return
	 */
	List<Map<String, Object>> list(SearchCondition condition);

	/**
	 *  取得表(tableName)中符合条件(condition)的数据总数
	 * @param condition
	 * @return
	 */
	int getTotal(SearchCondition condition);

	/**
	 *  删除指定表(tableName)及id的数据
	 * @param input
	 */
	void delete(InputData input);

	/**
	 *  清除缓存
	 */
	void clearCache();

	/**
	 *  批量更新
	 * @param input
	 */
	void updateSome(InputData input);

	/**
	 *  统计接口
	 * @param condition
	 * @return
	 */
	Double getSum(SearchCondition condition);
}
