package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.org.msmvc.sys.entity.SysUser;

/**
 * @author zsh
 * @desc 日志
 */
public interface LogsMapper {
	
	/**
	 * 日志列表
	 * 
	 * @return
	 */
	List<Map<String, Object>> logsList(Map<String, Object> map);

	/**
	 * 日志总数
	 * 
	 * @return
	 */
	public long logsCount(Map<String, Object> map);
}
