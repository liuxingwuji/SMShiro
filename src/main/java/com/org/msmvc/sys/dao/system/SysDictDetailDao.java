package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.SysDictDetailDTO;

public abstract interface SysDictDetailDao
{
  public abstract List<SysDictDetailDTO> searchSysDictDetailByPaging(Map<String, Object> paramMap);
  
  public abstract List<SysDictDetailDTO> searchSysDictDetail(Map<String, Object> paramMap);
  
  public abstract SysDictDetailDTO findSysDictDetailByPrimaryKey(String paramString);
  
  public abstract int insertSysDictDetail(Map<String, Object> paramMap);
  
  public abstract int updateSysDictDetail(Map<String, Object> paramMap);
  
  public abstract int deleteSysDictDetailByPrimaryKey(Map<String, Object> paramMap);
  
  public abstract int deleteSysDictDetailByDictKey(Map<String, Object> paramMap);
  
  public abstract String findSysDictMaxOrderBy(String paramString);
  
  public abstract String queryDetailCodeIsOk(Map<String, Object> paramMap);
  
  public abstract List<Map> queryDetailByDictCode(Map<String, String> paramMap);
  
  public abstract SysDictDetailDTO queryDetailByDictCodeAndDeatailValue(Map<String, Object> paramMap);
}



/* Location:           K:\platform\com-jy-platform-system\2.0.0\com-jy-platform-system-2.0.0.jar

 * Qualified Name:     com.jy.modules.platform.sysdict.dao.SysDictDetailDao

 * JD-Core Version:    0.7.0.1

 */