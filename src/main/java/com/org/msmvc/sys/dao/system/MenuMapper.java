package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.Menu;

/**
 * @author zsh
 * @desc 菜单
 */
public interface MenuMapper {
	//新增菜单
	public int insertMenu(Menu menu);
	//修改菜单
	public int updateMenu(Menu menu);
	//根据主键删除菜单
	public int deleteById(Integer id);
	//查询所有菜单
	public List<Menu> selectAll();
	//分页查询菜单
	public List pageMenuList(Map<String,Object> params);
	//菜单总数
	public long pageCounts(Map<String,Object> params);
	//菜单树
	public List menuTree(String treeId);
	//根据主键id获取菜单
	public Menu getById(Integer id);
	//根据菜单编码去删除菜单
	public int deleteByMId(String mId);
	//根据菜单编码查询菜单
	public Menu findByMenuId(String  menuId);
	//根据用户id获取菜单
	public List<Menu> getMenuByUserId(Map<String, Object> params);
}
