package com.org.msmvc.sys.dao.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用的数据对象，用于在mybatis的mapper中生成插入或者修改数据的sql
 * 
 * @author zsh
 *
 */
public class InputData {
	public InputData(String tableName) {
		this.tableName = tableName;
	}

	// 插入新的数据的id
	private int id = -1;
	// 表名
	private String tableName;
	// 属性表
	private List<String> fields = new ArrayList<String>();
	// 值表
	private List<Object> values = new ArrayList<Object>();
	// 批量更新条件
	private String condition = null;

	// 添加一条属性值
	public void add(String field, Object value) {
		fields.add(field);
		values.add(value);
	}

	// 清空属性值
	public void clear() {
		fields.clear();
		values.clear();
	}

	// 取得属性值数组，修改数据时需要用到
	public List<FieldValue> getFieldsValues() throws Exception {
		if (fields.size() == 0) {
			throw new Exception("请添加需要更新的数据");
		}
		List<FieldValue> fieldsValues = new ArrayList<FieldValue>();
		for (int i = 0; i < fields.size(); i++) {
			FieldValue fv = new FieldValue(fields.get(i), values.get(i));
			fieldsValues.add(fv);
		}
		return fieldsValues;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public List<Object> getValues() {
		return values;
	}

	public void setValues(List<Object> values) {
		this.values = values;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	private class FieldValue {
		private String field;
		private Object value;

		public FieldValue(String field, Object value) {
			this.field = field;
			this.value = value;
		}

		public String getField() {
			return field;
		}

		public Object getValue() {
			return value;
		}
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

}
