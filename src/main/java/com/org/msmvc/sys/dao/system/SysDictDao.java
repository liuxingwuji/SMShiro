package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.SysDictDTO;

public abstract interface SysDictDao
{
  public abstract List<SysDictDTO> searchSysDictByPaging(Map<String, Object> paramMap);
  
  public abstract List<SysDictDTO> searchSysDict(Map<String, Object> paramMap);
  
  public abstract SysDictDTO findSysDictByPrimaryKey(String paramString);
  
  public abstract int insertSysDict(Map<String, Object> paramMap);
  
  public abstract int updateSysDict(Map<String, Object> paramMap);
  
  public abstract int deleteSysDictByPrimaryKey(Map<String, Object> paramMap);
  
  public abstract String queryDictCodeIsOk(String paramString);
  
  public abstract Map<String, String> queryDictInfoByCode(String paramString);
}


