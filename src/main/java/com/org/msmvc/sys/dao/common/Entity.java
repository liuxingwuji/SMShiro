package com.org.msmvc.sys.dao.common;

/*
 * 实体类
 * 实现该接口后，作为被引用的属性对象时，保存的时候就只会保存对象的id
 * 
 */
public interface Entity {
	/**
	 * 取得数据id
	 * 
	 * @return
	 */
	int getId();

	/**
	 * 取得名称
	 * 
	 * @return
	 */
	String getName();

	void setName(String name);

	void setId(int id);
}
