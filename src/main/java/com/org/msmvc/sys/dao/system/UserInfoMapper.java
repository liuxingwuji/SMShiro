package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.org.msmvc.sys.entity.SysUser;

/**
 * @author zsh
 * @desc 用户
 */
public interface UserInfoMapper {
	public int deleteByPrimaryKey(Integer id);

	public int insert(SysUser record);

	public SysUser selectByPrimaryKey(Integer id);

	public int updateByPrimaryKey(SysUser record);

	public long pageCounts(Map<String, Object> params);

	public List<SysUser> selectAll(Map<String, Object> params);

	public int checkUserExits(String userName);

	public List exportUser(Map<String, Object> params);

	public SysUser getByUserName(String userName);

	public Set<String> findRoles(String username) throws Exception;// 根据用户名查找其角色

	public Set<String> findPermissions(String username) throws Exception; // 根据用户名查找其权限

	public SysUser findByUserName(String userName);
	
	public int updatePassword(Map<String, Object> params);// 修改用户密码

}
