package com.org.msmvc.sys.dao.common;

import java.util.ArrayList;
import java.util.List;

/*
 * 查询条件，用于在mybatis mapper中生成列表或者查询数据的sql * 
 */
public class SearchCondition {
	// 数据库表名
	private String tableName;
	// 显示多少行
	private int rows = 10;
	// 从第几页开始显示
	private int page = 1;

	// 从第几行开始显示
	public int getStart() {
		if (page > 0) {
			return (this.page - 1) * this.rows;
		} else {
			return -1;
		}
	}

	// 将需要显示的列转换为集合类型
	public List<String> getShowFields() {
		List<String> fieldsL = new ArrayList<String>();
		if (fields == null || fields.length() == 0) {
			fieldsL.add("*");
		} else {
			String[] showFields = fields.split("-");
			for (String f : showFields) {
				fieldsL.add(f);
			}
		}
		return fieldsL;
	}

	// 需要显示的列，列之间用-隔开（所以数据库定义列不能用-）
	private String fields;

	// 取得排序字符串
	public String getOrderby() {
		if (sort != null && sort.length() > 0) {
			return "order by " + sort + " " + order;
		} else {
			return "";
		}
	}

	// 接收easyui表格传来的升序、降序参数(asc或者desc)
	private String order = "";
	// 接收easyui表格传来的排序字段名
	private String sort = "";
	// 查询条件
	private String queryCondition;
	// 符合查询条件的数据总条数
	private int total;

	// get set
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getQueryCondition() {
		return queryCondition;
	}

	public void setQueryCondition(String queryCondition) {
		this.queryCondition = queryCondition;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFields() {
		return fields;
	}

	public String getOrder() {
		return order;
	}

	public String getSort() {
		return sort;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
