package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.SysOrg;

import tk.mybatis.mapper.common.Mapper;

/**
 * @author Administrator
 * @desc  组织管理
 * @DATE 2017年5月22日
 */
public interface SysOrgMapper  extends Mapper<SysOrg> {
	//组织集合
	public List<SysOrg> queryOrgList(Map<String, Object> params);

	//组织树
	public List orgTree(Map<String, Object> params);

	//组织列表查询
	public List getOrgList(Map<String, Object> params);
	//删除组织树
	public int deleteByCode(String code);

}
