package com.org.msmvc.sys.dao.system;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.org.msmvc.sys.entity.Roles;

/**
 * @author zsh
 * @desc 角色
 */
public interface RolesMapper {

	public int insert(Roles menu);

	public int update(Roles menu);

	public int deleteByCode(String code);

	public List selectAll();

	public List pageList(Map<String, Object> params);

	public long pageCounts(Map<String, Object> params);

	public Roles getById(Integer id);

	public int insertRoleAuthority(Map<String, Object> params);

	/**
	 * 删除用户角色信息
	 * 
	 * @param userId
	 * @return
	 */
	public int delUserRole(String userId);

	/**
	 * 删除角色中的用户权限
	 *
	 * @param code
	 * @return
	 */
	public int delUserRoleByRoleId(String code);

	/**
	 * 删除角色菜单信息
	 * 
	 * @param roleId
	 * @return
	 */
	public int delRoleAuthority(String roleId);

	/**
	 * 保存用户角色信息
	 * 
	 * @param params
	 * @return
	 */
	public int insertUserRole(Map<String, Object> params);

	/**
	 * 获取用户角色信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<String> getUserRole(String userId);

	public List<String> getRoleAuthority(String roleId);

	public List<String> getLoginRole(String userName);
}
