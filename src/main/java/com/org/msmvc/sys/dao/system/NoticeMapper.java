package com.org.msmvc.sys.dao.system;

import com.org.msmvc.sys.entity.Menu;

import java.util.List;
import java.util.Map;

/**
 * 新闻、banner展示
 * @author zsh
 * @desc 菜单
 */
public interface NoticeMapper {
	//新增通知
	public int insertNotice(Map<String, Object> params);
	//修改通知
	public int updateNotice(Map<String, Object> params);
	//根据主键删除通知
	public int deleteById(Integer id);
	//分页查询消息
	public List pageNoticeList(Map<String, Object> params);
	//消息总数
	public long pageCounts(Map<String, Object> params);
	//查看通知详细信息
	public Map<String, Object>  getNoticeDetail(Map<String, Object> params);

	//分页查询banner
	public List pageBannerList(Map<String, Object> params);
	//banner总数
	public long pageBannerCounts(Map<String, Object> params);
	//新增通知
	public int insertBanner(Map<String, Object> params);
	//修改通知
	public int updateBanner(Map<String, Object> params);
	//根据主键删除通知
	public int deleteBannerById(Integer id);
}
