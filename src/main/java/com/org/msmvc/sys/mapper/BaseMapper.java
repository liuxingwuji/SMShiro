package com.org.msmvc.sys.mapper;

import java.io.Serializable;

/**
 * @author Administrator
 * @desc
 * @DATE 2017年5月20日
 */
public interface BaseMapper<T, ID extends Serializable> {
	//根据主键删除
	int deleteByPrimaryKey(ID id);

	int insert(T record);//插入数据

	int insertSelective(T record);

	T selectByPrimaryKey(ID id);//根据主键查询

	int updateByPrimaryKeySelective(T record);

	int updateByPrimaryKeyWithBLOBs(T record);

	int updateByPrimaryKey(T record);
}
