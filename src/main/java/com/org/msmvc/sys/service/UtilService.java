package com.org.msmvc.sys.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * 通用公共接口
 * @author zsh
 *
 */
public interface UtilService {

	/**
	 * 导出excel
	 * @param dataList
	 * @param excelName
	 * @param XlsExportColumnNameIndexMappingOf_ContractChange
	 */
	public Workbook exportExcel(String xlsType,List<Map<String, Object>> dataList,String excelSheetName,Map<String,String> XlsExportColumnNameIndexMappingOf_ContractChange);
	public void testOnTime();
}
