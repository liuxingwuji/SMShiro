package com.org.msmvc.sys.service;/**
 * Created by Administrator on 2017/6/24.
 */

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author zsh
 * @create 2017-06-24 17:15
 **/
@WebService
public interface CxfService {
    @WebMethod
    public String sayHello(@WebParam(name="username") String username);
}
