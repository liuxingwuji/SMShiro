package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

public interface QuartzService {

	/**
	 * 每天给教练算薪资
	 * 
	 * @param reversion
	 * @throws Exception
	 */
	public void insertCoachEverySalary() throws Exception;

	/**
	 * 没有评论的默认好评，次月三号计算薪资
	 * 
	 * @throws Exception
	 */
	public void insertNoCommonSalary() throws Exception;

	/**
	 * 学员考试通过后给每个教练计算奖金
	 * 
	 * @throws Exception
	 */
	public void insertCoachSalaryExam() throws Exception;

}
