package com.org.msmvc.sys.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.service.UtilService;
@Service("utilService")
public class UtilServiceImpl implements UtilService{

	/**
	 * 导出excel
	 * @param dataList
	 * @param excelSheetName
	 * @param XlsExportColumnNameIndexMappingOf_ContractChange
	 */
	public Workbook exportExcel(String xlsType,List<Map<String, Object>> dataList, String excelSheetName,
			Map<String, String> XlsExportColumnNameIndexMappingOf_ContractChange) {
		Object[] columnModel = XlsExportColumnNameIndexMappingOf_ContractChange
                .keySet().toArray();
        Workbook wb = new HSSFWorkbook();
        if (xlsType.equals("2")) {
            wb = new XSSFWorkbook();
        }
        //XLS标题头样式 header style
        Font headFont = wb.createFont();
        headFont.setFontHeightInPoints((short) 12);// 大小
        headFont.setBoldweight(Font.BOLDWEIGHT_BOLD);// 粗体显示
        headFont.setFontName("黑体");

        CellStyle headStyle = wb.createCellStyle();

        headStyle.setFont(headFont);

        headStyle.setAlignment(CellStyle.ALIGN_CENTER);// 设置单元格水平方向对其方式
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 设置单元格垂直方向对其方式

        headStyle.setBorderTop(CellStyle.BORDER_THIN);
        headStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        headStyle.setBorderLeft(CellStyle.BORDER_THIN); // 左边边框
        headStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex()); // 左边边框颜色
        headStyle.setBorderRight(CellStyle.BORDER_THIN); // 右边边框
        headStyle.setRightBorderColor(IndexedColors.BLACK.getIndex()); // 右边边框颜色
        headStyle.setBorderBottom(CellStyle.BORDER_THIN); // 下边框
        headStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex()); // 下边框颜色

        headStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND); // 前景色

        headStyle.setWrapText(true);// 自动换行
      //XLS内容样式  body style
        CellStyle bodyStyle = wb.createCellStyle();
        bodyStyle.setAlignment(CellStyle.ALIGN_CENTER);
        bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        bodyStyle.setBorderLeft(CellStyle.BORDER_THIN); // 左边边框
        bodyStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex()); // 左边边框颜色
        bodyStyle.setBorderRight(CellStyle.BORDER_THIN); // 右边边框
        bodyStyle.setRightBorderColor(IndexedColors.BLACK.getIndex()); // 右边边框颜色
        bodyStyle.setBorderBottom(CellStyle.BORDER_THIN); // 下边框
        bodyStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex()); // 下边框颜色
        bodyStyle.setWrapText(true);// 自动换行
        
      //创建sheet
        Sheet sheet = wb.createSheet(excelSheetName);
        sheet.setDefaultColumnWidth(200);
      //生成XLS sheet头
        Row sheetHeadRow = sheet.createRow(0);// 头标题
        sheetHeadRow.setHeight((short) 400);// 高度
        for (int i = 0; i < columnModel.length; i++) {
            Cell cell = sheetHeadRow.createCell(i);
            cell.setCellValue(columnModel[i].toString());
            cell.setCellStyle(headStyle);
            // 设置列宽
            sheet.setColumnWidth(i, 5000);
        }
        //生成XLS sheet body 数据行
        for (int i = 0; i < dataList.size(); i++) {
            Row sheetRow = sheet.createRow(i + 1);// sheet页数据行
            Map<String, Object> dataRow = dataList.get(i);// 数据库数据行
            for (int j = 0; j < columnModel.length; j++) {
                Cell cell = sheetRow.createCell(j);
                String tempKey = columnModel[j].toString();
                Object tableName = XlsExportColumnNameIndexMappingOf_ContractChange
                        .get(tempKey);
                if (null != tableName) {
                    Object dataValue = dataRow.get(tableName.toString());
                    if (null != dataValue) {
                        cell.setCellValue(dataValue.toString());
                        cell.setCellStyle(bodyStyle);
                    }else{
                        //没有内容时也能生成XLS的边框格子美观些
                        cell.setCellValue("");
                        cell.setCellStyle(bodyStyle);
                    }
                }
            }
        }
        return wb;
     // Write the output to a file
        // FileOutputStream fileOut;
       /* try {
            // fileOut = new FileOutputStream("d:/workbook.xls");
            wb.write(outputStream);
            outputStream.close(); // 关闭流
            // fileOut.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
	}

	public void testOnTime() {
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^测试定时");
		
	}

}
