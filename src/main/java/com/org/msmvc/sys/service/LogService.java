package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

/**
 * 日志记录
 * @author zsh
 *
 */
public interface LogService {
	/**
	 * 增加操作日志
	 * @return
	 * @throws Exception
	 */
	public int addLog(Map<String, Object> map)throws Exception;
	/**
	 * 教练列表
	 * 
	 * @return
	 */
	List<Map<String, Object>> logsList(Map<String, Object> map);

	/**
	 * 教练总数
	 * 
	 * @return
	 */
	public long logsCount(Map<String, Object> map);
	
	/**
	 * 增加操作日志
	 * @return
	 * @throws Exception
	 */
	public int operaterLog(Map<String, Object> map)throws Exception;

}
