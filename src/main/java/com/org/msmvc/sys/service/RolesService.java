package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.org.msmvc.sys.entity.Menu;
import com.org.msmvc.sys.entity.Roles;

public interface RolesService {
	public int insert(Roles tree);

	public int update(Roles tree);

	/**
	 * 删除角色信息
	 * 
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int deleteById(Map<String, Object> params) throws Exception;

	public List selectAll();

	public List pageList(Map<String, Object> params);

	public long pageCounts(Map<String, Object> params);

	/**
	 * 根据角色id获取角色信息
	 * 
	 * @param id
	 * @return
	 */
	public Roles getById(Integer id);

	/**
	 * 插入角色菜单权限
	 * 
	 * @param roleId
	 * @param menuIdes
	 * @return
	 */
	public int insertRoleAuthority(String roleId, String menuIdes);

	/**
	 * 删除用户角色信息
	 * 
	 * @param userId
	 * @return
	 */
	public int delUserRole(String userId);

	/**
	 * 删除角色菜单信息
	 * 
	 * @param roleId
	 * @return
	 */
	public int delRoleAuthority(String roleId);

	/**
	 * 保存用户角色信息
	 * 
	 * @param userId
	 * @param roleIds
	 * @return
	 */
	public int insertUserRole(String userId, String roleIds);

	/**
	 * 获取用户角色信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<String> getUserRole(String userId);

	/**
	 * 获取角色菜单信息
	 * 
	 * @param roleId
	 * @return
	 */
	public List<String> getRoleAuthority(String roleId);

	/**
	 * 获取登录用户角色信息
	 * 
	 * @param userName
	 * @return
	 */
	public List<String> getLoginRole(String userName);

}
