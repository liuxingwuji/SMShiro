package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.Menu;

public interface MenuService {
	public int insertTree(Menu tree);

	public int updateTree(Menu tree);

	public int deleteById(Integer id);
	
	public int deleteByMId(String mId);

	public List<Menu> selectAll();

	public List pageTreeList(Map<String, Object> params);

	public long pageCounts(Map<String, Object> params);

	List menuTree(String treeId);
	
	public Menu getById(Integer id);
	
	public Menu findByMenuId(String  menuId);
	
	
	public List<Menu> getMenuByUserId(Map<String, Object> params);
}
