package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.SysDictDetailVo;

public abstract interface SysDictAPI
{
  public abstract List<Map> getDictByKey(String paramString);
  
  public abstract SysDictDetailVo queryDetailByDictCodeAndDeatailValue(String paramString1, String paramString2);
  
  public abstract Map<String, String> queryDictByDictCode(String paramString);
  
  public abstract String codeToName(String paramString1, String paramString2);
}
