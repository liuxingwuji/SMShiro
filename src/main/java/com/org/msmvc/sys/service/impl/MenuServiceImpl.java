package com.org.msmvc.sys.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.system.MenuMapper;
import com.org.msmvc.sys.entity.Menu;
import com.org.msmvc.sys.service.DefaultService;
import com.org.msmvc.sys.service.EntityService;
import com.org.msmvc.sys.service.MenuService;

@Service("menuService")
public class MenuServiceImpl implements MenuService{
	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private EntityService entityService;
	@Autowired
	@Qualifier("defaultService")
	private DefaultService defaultService;
	public List<Menu> selectAll() {
		// TODO Auto-generated method stub
		return menuMapper.selectAll();
	}

	public int insertTree(Menu tree) {
		tree.setMenuId(getMenuId(tree.getParentId(),tree.getType()));
		tree.setCreateDate(new Date());
		return menuMapper.insertMenu(tree);
	}

	public int updateTree(Menu tree) {
		// TODO Auto-generated method stub
		return menuMapper.updateMenu(tree);
	}

	public int deleteById(Integer id) {
		// TODO Auto-generated method stub
		return menuMapper.deleteById(id);
	}
	
	public List pageTreeList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return menuMapper.pageMenuList(params);
	}

	public long pageCounts(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return menuMapper.pageCounts(params);
	}

	public List menuTree(String treeId) {
		// TODO Auto-generated method stub
		return menuMapper.menuTree(treeId);
	}

	public Menu getById(Integer id) {
		// TODO Auto-generated method stub
		return menuMapper.getById(id);
	}

	public int deleteByMId(String mId) {
		// TODO Auto-generated method stub
		return menuMapper.deleteByMId(mId);
	}

	public Menu getById(String mId) {
		// TODO Auto-generated method stub
		return null;
	}

	public Menu findByMenuId(String menuId) {
		// TODO Auto-generated method stub
		return menuMapper.findByMenuId(menuId);
	}
	/**
	 * 设置菜单ID
	 * @param parentId
	 * @return
	 */
	private String getMenuId(String parentId,String type){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parentId", parentId);
		
		return type.toLowerCase()+parentId+(this.pageCounts(params)+1);
	}

	public List<Menu> getMenuByUserId(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return menuMapper.getMenuByUserId(params);
	}
}
