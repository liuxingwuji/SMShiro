package com.org.msmvc.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.org.msmvc.sys.dao.system.SysOrgMapper;
import com.org.msmvc.sys.entity.SysOrg;
import com.org.msmvc.sys.service.SysOrgService;
import tk.mybatis.mapper.entity.Example;

/**
 * @author Administrator
 * @desc 
 * @DATE 2017年5月22日
 */
@Service("sysOrgService")
public class SysOrgServiceImpl implements SysOrgService{
	@Autowired
    private SysOrgMapper mapper;
	public Map<String, Object> queryOrgList(Map<String, Object> params) {
		PageHelper.startPage(Integer.parseInt(params.get("page").toString()),Integer.parseInt(params.get("pageSize").toString()));
		/*PageHelper.orderBy("code desc");
		List<SysOrg> cls =  mapper.select(new SysOrg());
		 // 取分页信息
       PageInfo<SysOrg> pageInfo = new PageInfo<SysOrg>(cls);*/
		// 设置查询条件
		Example example = new Example(SysOrg.class);
		example.setOrderByClause("code asc"); // 设置排序条件
		List<SysOrg> orgList = this.mapper.selectByExample(example);
		PageInfo<SysOrg> pageInfo = new PageInfo<SysOrg>(orgList);
        params.put("Rows", pageInfo.getList());
		params.put("Total", pageInfo.getTotal());
		return params;
	}

	//@Cacheable(value="Org",key="'orgTree'")
	public List orgTree(Map<String, Object> params){
		//List<SysOrg> cls =  mapper.selectAll();
		return mapper.orgTree(params);
	}
	public int insertOrg(SysOrg sysOrg,Map<String, Object> map){
		SysOrg sysOrg0=new SysOrg();
		sysOrg0.setParentCode(sysOrg.getParentCode());
		int counts=mapper.getOrgList(map).size();
		sysOrg.setCode(sysOrg.getParentCode()+(counts+1));
		return mapper.insert(sysOrg);
	}
	/**
	 * 修改
	 * @param sysOrg
	 * @return
	 */
	public int updateOrg(SysOrg sysOrg){
		return mapper.updateByPrimaryKey(sysOrg);
	}
	public List getOrgList(Map<String, Object> params){
		return  mapper.getOrgList(params);
	}

	public int deleteByCode(String code){
		return mapper.deleteByCode(code);
	}
}
