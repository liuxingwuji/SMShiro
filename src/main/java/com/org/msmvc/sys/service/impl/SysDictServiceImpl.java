package com.org.msmvc.sys.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.service.SysDictDetailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.system.SysDictDao;
import com.org.msmvc.sys.entity.BaseDTO;
import com.org.msmvc.sys.entity.SysDictDTO;
import com.org.msmvc.sys.service.SysDictService;
 
 @Service("sysDictService")
 public class SysDictServiceImpl   implements SysDictService {


   @Autowired
   private SysDictDao dao;
   @Autowired
   private SysDictDetailService detailService;
   
   public List<SysDictDTO> searchSysDictByPaging(Map<String, Object> searchParams)
     throws Exception
   {
     List<SysDictDTO> dataList = this.dao.searchSysDictByPaging(searchParams);
     return dataList;
   }
   
   public List<SysDictDTO> sysDictByPagingNoCache(Map<String, Object> searchParams)
     throws Exception
   {
     List<SysDictDTO> dataList = this.dao.searchSysDictByPaging(searchParams);
     return dataList;
   }
   
   public List<SysDictDTO> searchSysDict(Map<String, Object> searchParams)
     throws Exception
   {
     List<SysDictDTO> dataList = this.dao.searchSysDict(searchParams);
     return dataList;
   }
   
   public SysDictDTO querySysDictByPrimaryKey(String id)
     throws Exception
   {
     SysDictDTO dto = this.dao.findSysDictByPrimaryKey(id);
     if (dto == null) {
       dto = new SysDictDTO();
     }
     return dto;
   }
   
   public int insertSysDict(SysDictDTO dto)
     throws Exception
   {
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", dto);
     
     int count = this.dao.insertSysDict(paramMap);
     
     /*SysDictDTO resultDto = (SysDictDTO)paramMap.get("dto");
     Long keyId = resultDto.getId();*/
     return count;
   }
   
   public int updateSysDict(SysDictDTO dto)
     throws Exception
   {
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", dto);
     
     return this.dao.updateSysDict(paramMap);
   }
   
   public int deleteSysDictByPrimaryKey(BaseDTO baseDto, String ids)
     throws Exception
   {
     if (StringUtils.isEmpty(ids)) {
       throw new Exception("删除失败！传入的参数主键为null");
     }
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", baseDto);
     paramMap.put("ids", ids);
     int i=0;
     this.detailService.deleteSysDictDetailByDictKey(baseDto, ids);
    i= this.dao.deleteSysDictByPrimaryKey(paramMap);

     return i;
   }
   
   public String queryDictCodeIsOk(String code)
     throws Exception
   {
     return this.dao.queryDictCodeIsOk(code);
   }
   
   public Map<String, String> queryDictInfoByCode(String code)
   {
     return this.dao.queryDictInfoByCode(code);
   }
 }