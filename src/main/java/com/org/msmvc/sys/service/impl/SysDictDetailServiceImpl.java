 package com.org.msmvc.sys.service.impl;
 
 import java.io.Serializable;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 import org.apache.commons.lang3.StringUtils;
 import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.org.msmvc.sys.dao.system.SysDictDetailDao;
import com.org.msmvc.sys.entity.BaseDTO;
import com.org.msmvc.sys.entity.SysDictDetailDTO;
import com.org.msmvc.sys.service.SysDictDetailService;
 
 @Service("sysDictDetailService")
 public class SysDictDetailServiceImpl implements SysDictDetailService{
   @Autowired
   private SysDictDetailDao dao;
   
   public List<SysDictDetailDTO> searchSysDictDetailByPaging(Map<String, Object> searchParams)
     throws Exception
   {
     List<SysDictDetailDTO> dataList = this.dao.searchSysDictDetailByPaging(searchParams);
     
     return dataList;
   }
   
   public String findSysDictMaxOrderBy(String dictId)
     throws Exception
   {
     return this.dao.findSysDictMaxOrderBy(dictId);
   }
   
   public String queryDetailCodeIsOk(Map<String, Object> searchParams)
     throws Exception
   {
     return this.dao.queryDetailCodeIsOk(searchParams);
   }
   
   public List<SysDictDetailDTO> searchSysDictDetail(Map<String, Object> searchParams)
     throws Exception
   {
     List<SysDictDetailDTO> dataList = this.dao.searchSysDictDetail(searchParams);
     return dataList;
   }
   
   public SysDictDetailDTO querySysDictDetailByPrimaryKey(String id)
     throws Exception
   {
     SysDictDetailDTO dto = this.dao.findSysDictDetailByPrimaryKey(id);
     if (dto == null) {
       dto = new SysDictDetailDTO();
     }
     return dto;
   }
   
   @Transactional
   public int insertSysDictDetail(SysDictDetailDTO dto)
     throws Exception
   {
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", dto);
     
     int count = this.dao.insertSysDictDetail(paramMap);
     
    /* SysDictDetailDTO resultDto = (SysDictDetailDTO)paramMap.get("dto");
     Long keyId = resultDto.getId();*/
     return count;
   }
   
   public int updateSysDictDetail(SysDictDetailDTO dto)
     throws Exception
   {
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", dto);
     
     return this.dao.updateSysDictDetail(paramMap);
   }
   
   public int deleteSysDictDetailByPrimaryKey(BaseDTO baseDto, String ids)
     throws Exception
   {
     if (StringUtils.isEmpty(ids)) {
       throw new Exception("删除失败！传入的参数主键为null");
     }
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", baseDto);
     paramMap.put("ids", ids);
     return this.dao.deleteSysDictDetailByPrimaryKey(paramMap);
   }
   
   public int deleteSysDictDetailByDictKey(BaseDTO baseDto, String ids)
     throws Exception
   {
     if (StringUtils.isEmpty(ids)) {
       throw new Exception("删除失败！传入的参数主键为null");
     }
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dto", baseDto);
     paramMap.put("ids", ids);
     return this.dao.deleteSysDictDetailByDictKey(paramMap);
   }
   
   public List<Map> queryDetailByDictCode(String code)
   {
     Map<String, String> temp = new HashMap();
     temp.put("code", code);
     return this.dao.queryDetailByDictCode(temp);
   }
   
   public SysDictDetailDTO queryDetailByDictCodeAndDeatailValue(String dict_code, String detail_value)
   {
     Map<String, Object> paramMap = new HashMap();
     paramMap.put("dict_code", dict_code);
     paramMap.put("detail_value", detail_value);
     return this.dao.queryDetailByDictCodeAndDeatailValue(paramMap);
   }
 }