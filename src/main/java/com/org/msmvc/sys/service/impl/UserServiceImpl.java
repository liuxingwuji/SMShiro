package com.org.msmvc.sys.service.impl;

import java.util.*;

import com.org.util.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.org.msmvc.sys.dao.common.EntityMapper;
import com.org.msmvc.sys.dao.common.InputData;
import com.org.msmvc.sys.dao.system.UserInfoMapper;
import com.org.msmvc.sys.entity.SysUser;
import com.org.msmvc.sys.service.UserService;
import com.org.util.Constant;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private EntityMapper entityMapper;


    public SysUser getUserById(int id) throws Exception {

        return userInfoMapper.selectByPrimaryKey(id);
    }

    public List getUsers(Map<String, Object> params) throws Exception {
        return userInfoMapper.selectAll(params);
    }

    @Transactional
    public int insert(SysUser userInfo) throws Exception {
        String salt= UUID.randomUUID().toString().replaceAll("-", "");
        userInfo.setSalt(salt);
        userInfo.setPassword(EncryptUtil.getShiroPassword(userInfo.getPassword(), userInfo.getCredentialsSalt()));
        userInfo.setStatus("1");//有效
        userInfo.setCreateDate(new Date());
        int result = userInfoMapper.insert(userInfo);

//		System.out.println(1/0);
        return result;
    }

    public List<SysUser> getPageList(Map<String, Object> params) throws Exception {
        //获取第1页，10条内容，默认查询总数count
        List<SysUser> list = userInfoMapper.selectAll(params);
        //PageInfo page = new PageInfo(list);
        return list;
    }

    public long getCounts(Map<String, Object> params) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.pageCounts(params);
    }

    public List<SysUser> getPageList() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Transactional
    public int deleteByPrimaryKey(Integer id) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.deleteByPrimaryKey(id);
    }

    public int checkUserExits(String userName) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.checkUserExits(userName);
    }

    @Transactional
    public int updateUser(SysUser userInfo) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.updateByPrimaryKey(userInfo);
    }

    public List exportUser(Map<String, Object> params) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.exportUser(params);
    }

    public SysUser getByUserName(String userName) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.getByUserName(userName);
    }

    public Set<String> findRoles(String username) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.findRoles(username);
    }

    public Set<String> findPermissions(String username) throws Exception {
        // TODO Auto-generated method stub
        return userInfoMapper.findPermissions(username);
    }

    public SysUser findByUserName(String userName) {
        // TODO Auto-generated method stub
        SysUser u = userInfoMapper.findByUserName(userName);
        return u;
    }

    @Transactional
    public int restPassword(String userId, String userName) {
        SysUser u=userInfoMapper.getByUserName(userName);
        InputData input = new InputData("sys_users");
        input.setId(Integer.parseInt(userId));
        input.add("password", EncryptUtil.getShiroPassword("123456", u.getCredentialsSalt()));
        return entityMapper.update(input);
    }

    @Transactional
    public int updatePassword(Map<String, Object> params) {
        SysUser u=userInfoMapper.getByUserName(params.get("username").toString());
        params.put("password", EncryptUtil.getShiroPassword(params.get("password").toString(), u.getCredentialsSalt()));
        return userInfoMapper.updatePassword(params);

    }

    @Transactional
    public int upUserImg(Map<String, Object> params) {
        InputData input = new InputData("sys_users");
        input.setId(Integer.parseInt(params.get("id").toString()));
        input.add("img", params.get("img").toString());
        return entityMapper.update(input);
    }


}
