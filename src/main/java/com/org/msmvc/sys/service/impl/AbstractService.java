package com.org.msmvc.sys.service.impl;

import java.io.Serializable;

import com.org.msmvc.sys.mapper.BaseMapper;
import com.org.msmvc.sys.service.BaseService;

/**
 * @author Administrator
 * @desc
 * @DATE 2017年5月20日
 */
public class AbstractService<T, ID extends Serializable> implements BaseService<T, ID> {
	private BaseMapper<T, ID> baseMapper;

	public void setBaseMapper(BaseMapper<T, ID> baseMapper) {
		this.baseMapper = baseMapper;
	}
	public void setBaseMapper() {
		// TODO Auto-generated method stub
		
	}


	public int deleteByPrimaryKey(ID id) {
		return baseMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(T record) {
		return baseMapper.insertSelective(record);
	}

	public T selectByPrimaryKey(ID id) {
		return baseMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(T record) {
		return baseMapper.updateByPrimaryKey(record);
	}

	public int updateByPrimaryKeyWithBLOBs(T record) {
		return baseMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	public int updateByPrimaryKey(T record) {
		return baseMapper.updateByPrimaryKey(record);
	}

	public int insert(T record) {
		return baseMapper.insert(record);
	}

	

}
