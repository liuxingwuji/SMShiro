package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.dao.common.Entity;


/*
 * 通用数据服务，单个表的数据查询、新建、修改、删除
 * 赵光 climber@pku.org.cn
 */
public interface EntityService {
	/*
	 * 查询数据，取得map列表
	 * Object entity 实体类,
	 * String q[] 查询字符串,可以传入多个参数,
	 * int page 显示第几页,
	 * int rows 每页显示多少行,
	 * String sort 根据那个属性排序,
	 * String order 升序asc，或者降序desc,
	 * String fields 显示哪些属性
	 * 得到的属性的值以Map集合的方式返回
	 */
	List<Map<String, Object>> list(Object entity,String queryCondition,int page,int rows,String sort,String order,String fields) throws Exception;
	//取得实体列表，输入参数同list，输出已经将map集合转换为对象了，便于在服务端使用。如果直接返回给客户端，建议使用list。
	List<Object> listBean(Object entity,String queryCondition,int page,int rows,String sort,String order,String fields) throws Exception;
	//取得符合条件的数据总数
	int getTotal(Object entity,String queryCondition) throws Exception;
	/*
	 * 批量插入或者修改
	 * Object entities[] 实体类列表(包含数据)
	 * List<String> fields[] 属性列表
	 */
	int[] update(Object entities[],List<String> fields[]) throws Exception;
	/*
	 * 插入或者修改
	 * Object entity 实体类(包含数据)
	 * List<String> fields 属性列表
	 * 返回刚刚插入的数据的id
	 */
	int update(Object entity,List<String> fields) throws Exception;
	/*
	 * 删除指定Id的数据
	 * Object entity 实体类,
	 * int id 欲删除的数据的Id
	 */
	void delete(Object entity,int id) throws Exception;	
	/*
	 * 根据id取得对象详情：取得指定Id的实体对象(主要用于直接输出为json返回客户端)
	 * Object entity 实体对象,
	 * int id 数据的Id
	 * String fields 需要取得哪些属性的值
	 * 得到的属性的值以Map集合的方式返回
	 */
	Map<String, Object> getEntity(Object entity,int id,String fields) throws Exception;
	/*
	 * 根据id取得对象详情：从数据库取得指定Id的实体对象属性的值，赋值到对象中(主要用于在服务端处理对象用)
	 * Object entity 实体对象,
	 * int id 数据的Id
	 * String fields 需要取得哪些属性的值
	 */
	void setEntityValue(Entity entity, int id,String fields) throws Exception;
	//清除缓存
    void clearCache();
}
