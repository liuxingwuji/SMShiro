package com.org.msmvc.sys.service.impl;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.org.msmvc.sys.entity.ResponseMsg;
public class BaseRest {
	 @ExceptionHandler({Exception.class})
	  public ResponseEntity<ResponseMsg<T>> handleInvalidRequestError(Exception ex)
	  {
	    String errorMsg = ex.getMessage();
	    if (errorMsg == null) {
	      errorMsg = "null";
	    }
	    errorMsg.replaceAll("[\\t\\n\\r]", "").replaceAll("[\\\\]", ".").replaceAll("\"", "");
	    ResponseMsg responseMsg = new ResponseMsg();
	    responseMsg.setErrorDesc(errorMsg);
	    responseMsg.setRetCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
	    ResponseEntity responseEntity = new ResponseEntity(responseMsg, HttpStatus.OK);
	    return responseEntity;
	  }
}
