package com.org.msmvc.sys.service;

import java.io.Serializable;

/**
 * @author Administrator
 * @desc
 * @DATE 2017年5月20日
 */
public interface BaseService<T,ID extends Serializable> {
	void setBaseMapper();

	int deleteByPrimaryKey(ID id);

	int insert(T record);

	int insertSelective(T record);

	T selectByPrimaryKey(ID id);

	int updateByPrimaryKeySelective(T record);

	int updateByPrimaryKeyWithBLOBs(T record);

	int updateByPrimaryKey(T record);
}
