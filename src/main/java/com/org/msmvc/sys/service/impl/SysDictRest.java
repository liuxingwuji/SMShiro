package com.org.msmvc.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.org.msmvc.sys.entity.ResponseMsg;
import com.org.msmvc.sys.entity.SysDictDTO;

public class SysDictRest extends BaseRest{
	  @Autowired
	  @Qualifier("com.org.msmvc.service.system.SysDictService")
	  private SysDictServiceImpl service;
	  @ResponseBody
	  @RequestMapping(value={"/create/v1"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	  public ResponseEntity<ResponseMsg<Void>> create(@RequestBody SysDictDTO obj)
	    throws Exception
	  {
	    ResponseMsg<Void> responseMsg = new ResponseMsg();
	    this.service.insertSysDict(obj);
	    ResponseEntity<ResponseMsg<Void>> responseEntity = new ResponseEntity(responseMsg, HttpStatus.OK);
	    return responseEntity;
	  }
}
