package com.org.msmvc.sys.service.impl;/**
 * Created by Administrator on 2017/6/24.
 */

import com.org.msmvc.sys.service.CxfService;

import javax.jws.WebService;

/**
 * @author zsh
 * @create 2017-06-24 17:16
 **/
@WebService(endpointInterface="com.org.msmvc.sys.service.CxfService")
public class CxfServiceImpl implements CxfService {
    @Override
    public String sayHello(String username) {
        return "Hello "+username;
    }
}
