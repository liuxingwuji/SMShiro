package com.org.msmvc.sys.service.impl;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.service.QuartzService;
import com.org.util.DateUtils;
@Service
@Component
public class QuartzServiceImpl implements QuartzService{

	public void insertCoachEverySalary() throws Exception {
		System.out.println("定时任务执行：" + DateUtils.getString_yyyy_MM_dd_HH_mm_ss_Form(new Date()));
		
	}
	@Scheduled(cron = "0 0 12 * * ? ") // 每天12点执行
	public void insertNoCommonSalary() throws Exception {
		System.out.println("注解定时任务执行：" + DateUtils.getString_yyyy_MM_dd_Form(new Date()));
		
	}

	public void insertCoachSalaryExam() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
