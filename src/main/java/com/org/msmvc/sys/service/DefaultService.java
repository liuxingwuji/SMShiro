package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

/**
 * 自定义通用实现接口
 * @author zsh
 *
 */
public interface DefaultService {
	/*
	 * 查询数据，取得map列表
	 * Object entity 实体类,
	 * String q[] 查询字符串,可以传入多个参数,
	 * int page 显示第几页,
	 * int rows 每页显示多少行,
	 * String sort 根据那个属性排序,
	 * String order 升序asc，或者降序desc,
	 * String fields 显示哪些属性
	 * int type 是否缓存  0代表不缓存 1代表缓存
	 * 得到的属性的值以Map集合的方式返回
	 */
	List<Map<String, Object>> list(String entity,String queryCondition,int page,int rows,String sort,String order,String fields,int type) throws Exception;
	/**
	 * 统计,根据条件统计
	 * @param entity   表名称
	 * @param q  统计条件
	 * @param fields  统计字段
	 * @return  
	 */
	Double getSum(Object entity,String q,String fields);
}
