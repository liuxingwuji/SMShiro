package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.org.msmvc.sys.entity.SysUser;

public interface UserService {
	public SysUser getUserById(int id) throws Exception;

	public List<SysUser> getUsers(Map<String, Object> params) throws Exception;

	public int insert(SysUser userInfo) throws Exception;

	public List<SysUser> getPageList(Map<String, Object> params) throws Exception;

	public long getCounts(Map<String, Object> params) throws Exception;

	public int deleteByPrimaryKey(Integer id) throws Exception;

	public int checkUserExits(String userName) throws Exception;

	public int updateUser(SysUser userInfo) throws Exception;

	public List exportUser(Map<String, Object> params) throws Exception;

	public SysUser getByUserName(String userName) throws Exception;

	public Set<String> findRoles(String username) throws Exception;// 根据用户名查找其角色

	public Set<String> findPermissions(String username) throws Exception; // 根据用户名查找其权限

	public SysUser findByUserName(String userName);

	public int restPassword(String userId,String userName);// 重置用户密码

	public int updatePassword(Map<String, Object> params);// 修改用户密码

	public int upUserImg(Map<String, Object> params);// 上传用户头像

}
