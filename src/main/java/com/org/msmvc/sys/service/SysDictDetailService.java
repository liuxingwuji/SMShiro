 package com.org.msmvc.sys.service;
 
 import java.io.Serializable;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 import org.apache.commons.lang3.StringUtils;
 import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.org.msmvc.sys.dao.system.SysDictDetailDao;
import com.org.msmvc.sys.entity.BaseDTO;
import com.org.msmvc.sys.entity.SysDictDetailDTO;
 /*数据字典详细*/
 public interface SysDictDetailService
  
 {
   
   public List<SysDictDetailDTO> searchSysDictDetailByPaging(Map<String, Object> searchParams)
     throws Exception;
   
   public String findSysDictMaxOrderBy(String dictId)
     throws Exception;
   
   public String queryDetailCodeIsOk(Map<String, Object> searchParams)
     throws Exception;
   
   public List<SysDictDetailDTO> searchSysDictDetail(Map<String, Object> searchParams)
     throws Exception;
   
   public SysDictDetailDTO querySysDictDetailByPrimaryKey(String id)
     throws Exception;
   
   public int insertSysDictDetail(SysDictDetailDTO dto)
     throws Exception;
   
   public int updateSysDictDetail(SysDictDetailDTO dto)
     throws Exception;
   
   public int deleteSysDictDetailByPrimaryKey(BaseDTO baseDto, String ids)
     throws Exception;
   
   public int deleteSysDictDetailByDictKey(BaseDTO baseDto, String ids)
     throws Exception;
   
   public List<Map> queryDetailByDictCode(String code);
   
   public SysDictDetailDTO queryDetailByDictCodeAndDeatailValue(String dict_code, String detail_value);
 }