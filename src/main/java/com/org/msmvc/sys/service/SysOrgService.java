package com.org.msmvc.sys.service;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.entity.SysOrg;

/**
 * @author Administrator
 * @desc 
 * @DATE 2017年5月22日
 */
public interface SysOrgService {
	/**
	 * 集合查询
	 * @param params
	 * @return
	 */
	public Map<String, Object> queryOrgList(Map<String, Object> params);
	/**
	 * 集合查询
	 * @param params
	 * @return
	 */
	public List getOrgList(Map<String, Object> params);

	/**
	 * 组织树集合查询
	 * @param params
	 * @return
	 */
	public List orgTree(Map<String, Object> params);

	/**
	 * 新增
	 * @param sysOrg
	 * @return
	 */
	public int insertOrg(SysOrg sysOrg,Map<String, Object> map);
	/**
	 * 修改
	 * @param sysOrg
	 * @return
	 */
	public int updateOrg(SysOrg sysOrg);
	/**
	 * 删除
	 * @param code
	 * @return
	 */
	public int deleteByCode(String code);
}
