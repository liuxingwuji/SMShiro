package com.org.msmvc.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.common.EntityMapper;
import com.org.msmvc.sys.dao.common.EntityUtil;
import com.org.msmvc.sys.dao.common.InputData;
import com.org.msmvc.sys.dao.system.LogsMapper;
import com.org.msmvc.sys.service.LogService;
import com.org.util.Constant;
@Service("logService")
public class LogServiceImpl implements LogService{
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private EntityMapper entityMapper;
	public int addLog(Map<String, Object> map) throws Exception {
		InputData input = new InputData("ccej_logs");
		input.add("requestip", map.get("ip"));
		input.add("method", map.get("method"));
		input.add("exceptionCode",  map.get("exceptionCode").toString());
		input.add("exceptionDetail",  map.get("exceptionDetail").toString()) ;
		input.add("description", map.get("description").toString() );
		input.add("params", map.get("p"));
		input.add("creater", map.get("creater"));
		return entityMapper.insert(input);
	}

	public List<Map<String, Object>> serarchLog(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public int operaterLog(Map<String, Object> map) throws Exception {
		InputData input = new InputData("ccej_logs");
		input.add("requestip", map.get("ip"));
		input.add("description", map.get("description").toString() );
		input.add("creater", map.get("operater"));
		return entityMapper.insert(input);
	}

	public List<Map<String, Object>> logsList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return logsMapper.logsList(map);
	}

	public long logsCount(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return logsMapper.logsCount(map);
	}

}
