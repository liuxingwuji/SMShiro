package com.org.msmvc.sys.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.system.SysDictDao;
import com.org.msmvc.sys.entity.BaseDTO;
import com.org.msmvc.sys.entity.SysDictDTO;
 /*数据字典*/
 public interface SysDictService    {
   
   public List<SysDictDTO> searchSysDictByPaging(Map<String, Object> searchParams)
     throws Exception;
   
   public List<SysDictDTO> sysDictByPagingNoCache(Map<String, Object> searchParams)
     throws Exception;
  
   
   public List<SysDictDTO> searchSysDict(Map<String, Object> searchParams)
     throws Exception;
   
   
   public SysDictDTO querySysDictByPrimaryKey(String id)
     throws Exception;
   
   public int insertSysDict(SysDictDTO dto)
     throws Exception;
   
   public int updateSysDict(SysDictDTO dto)
     throws Exception;
   
   public int deleteSysDictByPrimaryKey(BaseDTO baseDto, String ids)
     throws Exception;
   
   public String queryDictCodeIsOk(String code)
     throws Exception;
   
   public Map<String, String> queryDictInfoByCode(String code);
 }