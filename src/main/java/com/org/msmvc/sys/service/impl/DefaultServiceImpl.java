package com.org.msmvc.sys.service.impl;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.common.EntityMapper;
import com.org.msmvc.sys.dao.common.EntityUtil;
import com.org.msmvc.sys.dao.common.SearchCondition;
import com.org.msmvc.sys.service.DefaultService;

/**
 * 自定义通用接口实现
 * 
 * @author zsh
 */
@Service("defaultService")
public class DefaultServiceImpl implements DefaultService {
	private Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private EntityMapper entityMapper;

	@Autowired
	private EntityMapper EntityMapperNoCache;

	// 查询
	public List<Map<String, Object>> list(String entity, String queryCondition, int page, int rows, String sort,
			String order, String fields, int type) throws Exception {
		// 1、构造条件
		SearchCondition condition = new SearchCondition();
		condition.setTableName(entity);
		if (page > 0)
			condition.setPage(page);
		if (rows > 0)
			condition.setRows(rows);
		condition.setOrder(order);
		condition.setSort(sort);
		condition.setFields(fields);
		// 2、构造查询条件
		if (queryCondition != null && queryCondition.length() > 0) {
			condition.setQueryCondition(queryCondition);
		}
		// 3、执行 取得数据列表
		List<Map<String, Object>> data;
		if (type == 0) {
			data = EntityMapperNoCache.list(condition);
		} else {
			data = entityMapper.list(condition);
		}
		return data;
	}

	public Double getSum(Object entity, String q, String fields) {
		SearchCondition condition = new SearchCondition();
		condition.setTableName(EntityUtil.getTableName(entity));
		condition.setQueryCondition(q.toString());
		condition.setFields(fields);
		return entityMapper.getSum(condition) != null ? entityMapper.getSum(condition) : 0;
	}
}
