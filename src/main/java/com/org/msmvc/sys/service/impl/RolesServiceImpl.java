package com.org.msmvc.sys.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.dao.common.EntityMapper;
import com.org.msmvc.sys.dao.common.EntityUtil;
import com.org.msmvc.sys.dao.common.InputData;
import com.org.msmvc.sys.dao.system.RolesMapper;
import com.org.msmvc.sys.entity.Roles;
import com.org.msmvc.sys.service.DefaultService;
import com.org.msmvc.sys.service.EntityService;
import com.org.msmvc.sys.service.RolesService;

@Service("rolesService")
public class RolesServiceImpl implements RolesService {
	@Autowired
	private RolesMapper roleMapper;
	@Autowired
	private EntityMapper entityMapper;
	@Autowired
	@Qualifier("defaultService")
	private DefaultService defaultService;

	public List<Roles> selectAll() {
		// TODO Auto-generated method stub
		return roleMapper.selectAll();
	}

	public int insert(Roles role) {
		// TODO Auto-generated method stub
		// return roleMapper.insert(role);
		return this.roleMapper.insert(role);
	}

	public int update(Roles role) {
		// TODO Auto-generated method stub
		return roleMapper.update(role);
	}

	public int deleteById(Map<String, Object> params) throws Exception {
		delRoleAuthority(params.get("code").toString());
		roleMapper.delUserRoleByRoleId(params.get("code").toString());
		return roleMapper.deleteByCode(params.get("code").toString());
	}

	public List pageList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return roleMapper.pageList(params);
	}

	public long pageCounts(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return roleMapper.pageCounts(params);
	}

	public Roles getById(Integer id) {
		// TODO Auto-generated method stub
		return roleMapper.getById(id);
	}

	public int insertRoleAuthority(String roleId, String menuIdes) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("roleId", roleId);
		String[] menu = menuIdes.split(",");
		this.delRoleAuthority(roleId);// 删除原有角色菜单信息
		int n = 0;
		for (int i = 0; i < menu.length; i++) {
			params.put("menuId", menu[i]);
			n += roleMapper.insertRoleAuthority(params);
		}
		return n;
	}

	public List<String> getRoleAuthority(String roleId) {
		// TODO Auto-generated method stub
		return roleMapper.getRoleAuthority(roleId);
	}

	public int insertUserRole(String userId, String roleIds) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		String[] role = roleIds.split(",");
		this.delUserRole(userId);
		int n = 0;
		for (int i = 0; i < role.length; i++) {
			params.put("roleId", role[i]);
			n += roleMapper.insertUserRole(params);
		}
		return n;
	}

	public List<String> getLoginRole(String userName) {
		// TODO Auto-generated method stub
		return roleMapper.getLoginRole(userName);
	}

	public int delUserRole(String userId) {
		// TODO Auto-generated method stub
		return roleMapper.delUserRole(userId);
	}

	public List<String> getUserRole(String userId) {
		// TODO Auto-generated method stub
		return roleMapper.getUserRole(userId);
	}

	public int delRoleAuthority(String code) {
		// TODO Auto-generated method stub
		return roleMapper.delRoleAuthority(code);
	}
}
