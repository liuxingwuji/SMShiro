package com.org.msmvc.sys.service.impl;

import com.org.msmvc.sys.dao.system.NoticeMapper;

import com.org.msmvc.sys.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("noticeService")
public class NoticeServiceImpl implements NoticeService {
	@Autowired
	private NoticeMapper noticeMapper;
	//新增通知
	public int insertNotice(Map<String, Object> params){
		return noticeMapper.insertNotice(params);
	};
	//修改通知
	public int updateNotice(Map<String, Object> params){
		return noticeMapper.updateNotice(params);
	};
	//根据主键删除通知
	public int deleteById(Integer id){
		return noticeMapper.deleteById(id);
	};
	//分页查询菜单
	public List pageNoticeList(Map<String, Object> params){
		return noticeMapper.pageNoticeList(params);
	};
	//菜单总数
	public long pageCounts(Map<String, Object> params){
		return noticeMapper.pageCounts(params);
	};
	//查看通知详细信息
	public Map<String, Object>  getNoticeDetail(Map<String, Object> params){
		return noticeMapper.getNoticeDetail(params);
	}

	@Override
	public List pageBannerList(Map<String, Object> params) {
		return noticeMapper.pageBannerList(params);
	}

	@Override
	public long pageBannerCounts(Map<String, Object> params) {
		return noticeMapper.pageBannerCounts(params);
	}

	@Override
	public int insertBanner(Map<String, Object> params) {
		return noticeMapper.insertBanner(params);
	}

	@Override
	public int updateBanner(Map<String, Object> params) {
		return noticeMapper.updateBanner(params);
	}

	@Override
	public int deleteBannerById(Integer id) {
		return noticeMapper.deleteBannerById(id);
	}

	;



}
