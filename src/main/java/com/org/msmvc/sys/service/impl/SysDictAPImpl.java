package com.org.msmvc.sys.service.impl;

import java.util.List;
import java.util.Map;

import com.org.msmvc.sys.service.SysDictDetailService;
import com.org.msmvc.sys.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.msmvc.sys.entity.SysDictDetailDTO;
import com.org.msmvc.sys.entity.SysDictDetailVo;
import com.org.msmvc.sys.service.SysDictAPI;

@Service("sysDictAPI")
public class SysDictAPImpl implements SysDictAPI {
	@Autowired
	private SysDictDetailService sysDictDetailsrv;
	@Autowired
	private SysDictService sysServce;

	public List<Map> getDictByKey(String key) {
		List<Map> detailList = this.sysDictDetailsrv.queryDetailByDictCode(key);
		return detailList;
	}

	public SysDictDetailVo queryDetailByDictCodeAndDeatailValue(
			String dict_code, String detail_value) {
		SysDictDetailDTO dto = this.sysDictDetailsrv
				.queryDetailByDictCodeAndDeatailValue(dict_code, detail_value);
		if (dto != null) {
			SysDictDetailVo vo = new SysDictDetailVo();
			vo.setDictDetailName(dto.getDictDetailName());
			vo.setDictDetailValue(dto.getDictDetailValue());
			vo.setOrderBy(dto.getOrderBy());
			return vo;
		}
		return null;
	}

	public Map<String, String> queryDictByDictCode(String code) {
		return this.sysServce.queryDictInfoByCode(code);
	}

	public String codeToName(String type_, String code_) {
		List<Map> list = getDictByKey(type_);
		if ((list != null) && (list.size() > 0)) {
			for (int i = 0; i < list.size(); i++) {
				if ((code_ != null)
						&& (("," + code_ + ",").indexOf(","
								+ ((Map) list.get(i)).get("DICVALUE") + ",") != -1)) {
					return "" + ((Map) list.get(i)).get("DICNAME") + " ";
				}
			}
		}
		return "";
	}
}
