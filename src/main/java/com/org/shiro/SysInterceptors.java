package com.org.shiro;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
/*拦截器*/
public class SysInterceptors  implements HandlerInterceptor {
	private static final String LOGIN_URL="/index01.jsp";
	/** 
     * 在渲染视图之后被调用； 
     * 可以用来释放资源 
     */   
    public void afterCompletion(HttpServletRequest arg0,  
            HttpServletResponse arg1, Object arg2, Exception arg3)  
            throws Exception {  
        // TODO Auto-generated method stub  
        System.out.println("SysInterceptors afterCompletion");
    }  
    /** 
     * 该方法在目标方法调用之后，渲染视图之前被调用； 
     * 可以对请求域中的属性或视图做出修改 
     *  
     */  
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,  
            Object arg2, ModelAndView arg3) throws Exception {  
        // TODO Auto-generated method stub  
        System.out.println("SysInterceptors postHandle");
    }  
  
    /** 
     * 可以考虑作权限，日志，事务等等 
     * 该方法在目标方法调用之前被调用； 
     * 若返回TURE,则继续调用后续的拦截器和目标方法 
     * 若返回FALSE,则不会调用后续的拦截器和目标方法 
     *  
     */  
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,  
            Object arg2) throws Exception {  
        // TODO Auto-generated method stub  
        // 获得请求路径的uri
        String uri = request.getRequestURI();
        // 进入登录页面，判断session中是否有key，有的话重定向到首页，否则进入登录界面
        if(uri.contains("login")) {
            if(request.getSession().getAttribute("user") != null) {
                response.sendRedirect(request.getContextPath());//默认跟路径为首页
            } else {
                return true;//继续登陆请求
            }
        }
        System.out.println("**********Incetptor"+SecurityUtils.getSubject().getPrincipal());
        // 其他情况判断session中是否有key，有的话继续用户的操作
        if(request.getSession().getAttribute("currentuser") != null) {
            return true;
        }

       /* if (!SecurityUtils.getSubject().isAuthenticated()) {
            //判断session里是否有用户信息
            if (request.getHeader("x-requested-with") != null
                    && request.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest")) {
                //如果是ajax请求响应头会有，x-requested-with
               // response.setHeader("session-status", "timeout");//在响应头设置session状态
                response.sendRedirect(request.getSession().getServletContext().getContextPath()+LOGIN_URL);
                return false;
            }
        }*/
        response.sendRedirect(request.getSession().getServletContext().getContextPath()+LOGIN_URL);
        return false;
    }

    /**
     * 判断ajax请求
     * @param request
     * @return
     */
    boolean isAjax(HttpServletRequest request){
        return  (request.getHeader("X-Requested-With") != null  && "XMLHttpRequest".equals( request.getHeader("X-Requested-With").toString())   ) ;
    }
}
