package com.org.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.org.msmvc.sys.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 过滤器管理
 * @author zsh
 *
 */
public class SysUserFilter extends PathMatchingFilter {

	@Autowired
	private UserService userService;

	@Override
	protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

		String username = (String) SecurityUtils.getSubject().getPrincipal();
		request.setAttribute("user", userService.getByUserName(username));
		return true;
	}
	public static void main(String[] args) {
		String algorithmName = "md5";
		String username = "admin";
		String password = "123123";
		String salt1 = username;
		String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
		int hashIterations = 3;
		/*SimpleHash hash = new SimpleHash(algorithmName, password,
				salt1 + salt2, hashIterations);*/
		SimpleHash hash = new SimpleHash(algorithmName, password,
				salt1, hashIterations);
		String encodedPassword = hash.toHex();
		System.out.println("password:"+encodedPassword);
		System.out.println("salt2:"+salt2);
	}
}
