package com.org.shiro;

import com.org.util.Constant;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 会话管理
 * Created by zsh on 2018/1/25.
 */
@Controller
@RequestMapping("sessions")
public class SessionController {
    @Autowired
    private SessionDAO sessionDAO;
    @RequestMapping(value = "/query/{operate}")
    public ModelAndView execute(@PathVariable("operate") String operate, HttpServletRequest request,
                                HttpServletResponse response) throws NumberFormatException, Exception {
        ModelAndView model = new ModelAndView();
        if (operate.equals("toSession")) {//列表显示
            model.setViewName("/sessions/listSessions");
        }
        return model;
    }

    /**
     * 获取系统用户登录信息
     * @param model
     * @return
     */
    @RequestMapping("/sessionList")
    @ResponseBody
    public Map<String,Object> list(Model model) {
        Collection<Session> sessions = sessionDAO.getActiveSessions();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("Rows", sessions);
        map.put("Total", sessions.size());
        return map;
        //return "sessions/list";
    }

    /**
     * 强制用户退出系统
     * @param sessionId
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value="/forceLogout")
    @ResponseBody
    public Map forceLogout(@RequestParam("sessionId") String  sessionId, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            Session session = sessionDAO.readSession(sessionId);
            if(session != null) {
                session.setAttribute("session.force.logout", Boolean.TRUE);
            }
        } catch (Exception e) {/*ignore*/}
        map.put("msg", "强制退出成功！");
        return map;
    }

    /**
     * 是否强制退出
     * @param session
     * @return
     */
    public static boolean isForceLogout(Session session) {
        return session.getAttribute(Constant.SESSION_FORCE_LOGOUT_KEY) != null;
    }

    /**
     * 获取当前登录用户名
     * @param session
     * @return
     */
    public static String principal(Session session) {
        PrincipalCollection principalCollection =
                (PrincipalCollection) session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);

        return (String)principalCollection.getPrimaryPrincipal();
    }
}
