package com.org.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.org.msmvc.sys.service.SysDictAPI;

public class TagSupport extends javax.servlet.jsp.tagext.TagSupport
{
	  private static final long serialVersionUID = 1L;
	  private String id;
	  private String name;
	  private String defaultValue;
	  private String onchange;
	  private String className;
	  private boolean hasBlank = true;
	  private String language;
	  private String type = "select";
	  private String extendProperty;
	  private String emptyText = "请选择";
	  private String codeType = "";
	  private String used = "";
	  
	  public int doStartTag()
	    throws JspException
	  {
	    StringBuffer select = new StringBuffer();
	    try
	    {
	      List<Map> allList = null;
	      List<Map> list = null;
	      if ((this.codeType != null) && (!this.codeType.equals("")))
	      {
	        WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
	        ServletContext servletContext = webApplicationContext.getServletContext();
	        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
	        SysDictAPI sysDictAPI = (SysDictAPI)context.getBean("sysDictAPI");
	        if ((this.used != null) && (this.used.length() > 0))
	        {
	          allList = sysDictAPI.getDictByKey(this.codeType);
	          if ((allList != null) && (allList.size() > 0))
	          {
	            list = new ArrayList();
	            for (int j = 0; j < allList.size(); j++) {
	              if (("," + this.used + ",").indexOf("," + ((Map)allList.get(j)).get("DICVALUE") + ",") != -1) {
	                list.add(allList.get(j));
	              }
	            }
	          }
	        }
	        else
	        {
	          list = sysDictAPI.getDictByKey(this.codeType);
	        }
	      }
	      if ((list != null) && (list.size() > 0))
	      {
	        if (this.type.equals("select"))
	        {
	          select.append("<select ");
	          select = getAllProp(select);
	          select.append(">");
	          if (this.hasBlank)
	          {
	            select.append("<option value=''>");
	            select.append(this.emptyText);
	            select.append("</option>");
	          }
	          for (int i = 0; i < list.size(); i++)
	          {
	            Map sysCode = (Map)list.get(i);
	            select.append("<option value='");
	            select.append(sysCode.get("DICVALUE"));
	            select.append("'");
	            if (sysCode.get("DICVALUE").equals(this.defaultValue)) {
	              select.append(" selected='selected'");
	            }
	            select.append(">");
	            select.append(sysCode.get("DICNAME"));
	            select.append("</option>");
	          }
	          select.append("</select>");
	        }
	        else if (this.type.equals("json"))
	        {
	          select.append("[");
	          if ((list != null) && (list.size() > 0))
	          {
	            if (this.hasBlank)
	            {
	              select.append("{value:'',text:'--");
	              select.append(this.emptyText);
	              select.append("--'},");
	            }
	            for (int i = 0; i < list.size(); i++)
	            {
	              select.append("{value:'");
	              select.append(((Map)list.get(i)).get("DICVALUE"));
	              select.append("',text:'");
	              select.append(((Map)list.get(i)).get("DICNAME"));
	              if (i == list.size() - 1) {
	                select.append("'}");
	              } else {
	                select.append("'},");
	              }
	            }
	          }
	          else
	          {
	            select.append("{}");
	          }
	          select.append("]");
	        }
	        else if (this.type.equals("radio"))
	        {
	          if ((list != null) && (list.size() > 0)) {
	            for (int i = 0; i < list.size(); i++)
	            {
	              Map sysCode = (Map)list.get(i);
	              
	              select.append("<input type='radio' id='" + this.name + i + "' name='");
	              select.append(this.name);
	              select.append("'");
	              if (sysCode.get("DICVALUE").equals(this.defaultValue)) {
	                select.append(" checked");
	              }
	              select.append(" value='");
	              select.append(sysCode.get("DICVALUE"));
	              select.append("'>");
	              select.append("<label for='" + this.name + i + "'>" + sysCode.get("DICNAME") + "</label>");
	              
	              select.append("&nbsp;&nbsp;");
	            }
	          }
	        }
	        else if (this.type.equals("checkbox"))
	        {
	          if ((list != null) && (list.size() > 0)) {
	            for (int i = 0; i < list.size(); i++)
	            {
	              Map sysCode = (Map)list.get(i);
	              
	              select.append("<input type='checkbox' id='" + this.name + i + "' name='");
	              select.append(this.name);
	              select.append("'");
	              if ((this.defaultValue != null) && (("," + this.defaultValue + ",").indexOf("," + sysCode.get("DICVALUE") + ",") != -1)) {
	                select.append(" checked");
	              }
	              select.append(" value='");
	              select.append(sysCode.get("DICVALUE"));
	              select.append("'>");
	              select.append("<label for='" + this.name + i + "'>" + sysCode.get("DICNAME") + "</label>");
	              
	              select.append("&nbsp;&nbsp;");
	            }
	          }
	        }
	        else if (this.type.equals("text"))
	        {
	          for (int i = 0; i < list.size(); i++)
	          {
	            Map sysCode = (Map)list.get(i);
	            if ((this.defaultValue != null) && (("," + this.defaultValue + ",").indexOf("," + sysCode.get("DICVALUE") + ",") != -1)) {
	              select.append(sysCode.get("DICNAME") + "&nbsp;&nbsp;");
	            }
	          }
	        }
	      }
	      else if (this.type.equals("json")) {
	        select.append("{}");
	      }
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    try
	    {
	      this.pageContext.getOut().write(select.toString());
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
	    return 0;
	  }
	  
	  private StringBuffer getAllProp(StringBuffer results)
	    throws JspException
	  {
	    prepareAttribute(results, "id", getId());
	    prepareAttribute(results, "name", getName());
	    prepareAttribute(results, "onchange", getOnchange());
	    prepareAttribute(results, "className", getClassName());
	    
	    results.append(" " + getExtendProperty());
	    return results;
	  }
	  
	  protected void prepareAttribute(StringBuffer handlers, String name, Object value)
	  {
	    if (value != null)
	    {
	      handlers.append(" ");
	      handlers.append(name);
	      handlers.append("=\"");
	      handlers.append(value);
	      handlers.append("\"");
	    }
	  }
	  
	  public String getId()
	  {
	    return this.id;
	  }
	  
	  public void setId(String id)
	  {
	    this.id = id;
	  }
	  
	  public String getName()
	  {
	    return this.name;
	  }
	  
	  public void setName(String name)
	  {
	    this.name = name;
	  }
	  
	  public String getDefaultValue()
	  {
	    return this.defaultValue;
	  }
	  
	  public void setDefaultValue(String defaultValue)
	  {
	    this.defaultValue = defaultValue;
	  }
	  
	  public String getOnchange()
	  {
	    return this.onchange;
	  }
	  
	  public void setOnchange(String onchange)
	  {
	    this.onchange = onchange;
	  }
	  
	  public boolean isHasBlank()
	  {
	    return this.hasBlank;
	  }
	  
	  public void setHasBlank(boolean hasBlank)
	  {
	    this.hasBlank = hasBlank;
	  }
	  
	  public String getLanguage()
	  {
	    return this.language;
	  }
	  
	  public void setLanguage(String language)
	  {
	    this.language = language;
	  }
	  
	  public String getClassName()
	  {
	    return this.className;
	  }
	  
	  public void setClassName(String className)
	  {
	    this.className = className;
	  }
	  
	  public String getType()
	  {
	    return this.type;
	  }
	  
	  public void setType(String type)
	  {
	    if (null == type) {
	      this.type = "select";
	    } else {
	      this.type = type;
	    }
	  }
	  
	  public String getExtendProperty()
	  {
	    return this.extendProperty;
	  }
	  
	  public void setExtendProperty(String extendProperty)
	  {
	    this.extendProperty = extendProperty;
	  }
	  
	  public String getEmptyText()
	  {
	    return this.emptyText;
	  }
	  
	  public void setEmptyText(String emptyText)
	  {
	    if (null == emptyText) {
	      this.emptyText = "请选择";
	    } else {
	      this.emptyText = emptyText;
	    }
	  }
	  
	  public String getCodeType()
	  {
	    return this.codeType;
	  }
	  
	  public void setCodeType(String codeType)
	  {
	    this.codeType = codeType;
	  }
	  
	  public String getUsed()
	  {
	    return this.used;
	  }
	  
	  public void setUsed(String used)
	  {
	    this.used = used;
	  }
	}
