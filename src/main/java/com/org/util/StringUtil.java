package com.org.util;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 字符串验证工具类
 * 格式/是否为空
 * @author 尹坤
 *
 */
public final class StringUtil {
	/**
	 * 判断 对象 不为 NULL
	 * @param obj 任意对象
	 * @return 如果不为NULL，返回true
	 */
    public static boolean notNull(Object obj){
        return obj!=null;
    }
    /**
	 * 判断 对象 为 NULL
	 * @param obj 任意对象
	 * @return 如果为NULL，返回true
	 */
    public static boolean isNull(Object obj){
        return !notNull(obj);
    }
    /**
	 * 判断 字符串 不为 空（字符串的Empty）
	 * @param str 任意字符串
	 * @return 如果字符串不为NULL,平且不为EMPTY，返回true
	 */
    public static boolean notEmpty(String str){
        return notNull(str)&&(str.trim().length()>0);
    }
    /**
	 * 判断 字符串 为 空（字符串的Empty）
	 * @param str 任意字符串
	 * @return 如果字符串为NULL,或者为EMPTY，返回true
	 */
    public static boolean isEmpty(String str){
        return !notEmpty(str);
    }
    /**
	 * 判断 字符串 不为 NULL或EMPTY（字符串的Empty）
	 * @param str 任意字符串
	 * @return 如果字符串不为NULL,并且不为EMPTY，返回true
	 */
    public static boolean notNullOrEmpty(Object str){
        return notNull(str)&&notEmpty(str+"");
    }
    /**
	 * 判断 字符串 为 空（字符串的Empty）
	 * @param str 任意字符串
	 * @return 如果字符串为NULL,并且为EMPTY，返回true
	 */
    public static boolean isNullOrEmpty(String str){
        return !notNullOrEmpty(str);
    }
    /**
     * 判断字符串是否是邮箱格式
     * @param str  任意字符串(不为null)
     * @return   如果字符串满足邮箱格式返回true
     */
    public static boolean isEmail(String str){
    	String reg = "^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9]){2,5}.)([a-zA-Z0-0]{2,4})$";
    	return str.matches(reg)==true;
    }
    /**
     * 判断字符串是否是手机号码格式
     * @param str  任意字符串(不为null)
     * @return   如果字符串满足邮箱格式返回true
     */
    public static boolean isPhone(String str){
    	String reg="^1[3|4|5|7|8][0-9]\\d{8}$";
    	return str.matches(reg)==true;
    }
    /**
     * 判断是否是身份证号
     * @param str  任意字符串(不为null)
     * @return   如果字符串满足邮箱格式返回true
     */
    public static boolean isIdentity(String str){
    	String reg="^\\d{6}19\\d{2}((1[0-2])|0\\d)([0-2]\\d|30|31)\\d{3}[\\d|X|x]$";
    	return str.matches(reg)==true;
    }
    
    /**
     * 格式化日期(从数据库中查出的日期)
     * @param date   日期对象或字符串
     * @param fmt    格式
     * @return
     * @throws ParseException 
     */
    public static String fmtDate(Object date,String fmt) throws ParseException{
    	if(date == null){
    		return "";
    	}
    	String time = date.toString();
    	if(time.length()>10){
    		return time.substring(0,10).replaceAll("-", ".");
    	}
    	return "";
    }
    /**
     * 格式化账号，中间加*
     * @param str
     * @return
     */
    public static String fmtString(String str){
    	if(str.length()<=3){
    		return str;
    	}else if(str.length()<=7){
    		return str.substring(0, 2)+"****"+str.substring(str.length()-2,str.length());
    	}else{
    		return str.substring(0, 3)+"****"+str.substring(str.length()-3,str.length());
    	}
    }
    /**
     * 身份证给格式化*
     * @param str
     * @return
     */
    public static String fmtIdcard(String str){
    	if(str.length()<=3){
    		return str;
    	}else if(str.length()>=18){
    		return str.substring(0, 4)+"**********"+str.substring(str.length()-4,str.length());
    	}else{
    		return str;
    	}
    }
    /**
     * 格式化money
     * @param money
     * @return
     */
    public static String fmtMoney(Object money){
    	java.text.DecimalFormat   df   =new   java.text.DecimalFormat("######0.00");
		return df.format(money);  
    }
    /**
     * 时间格式化，获取星期几
     * @param date
     * @return
     */
    public static String fmtDaeWeek(Date date){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	int week=cal.get(Calendar.DAY_OF_WEEK);
    	String str ="";
    	switch (week) {
		case 1:
			str="周日";
			break;
		case 2:
			str="周一";
			break;
		case 3:
			str="周二";
			break;
		case 4:
			str="周三";
			break;
		case 5:
			str="周四";
			break;
		case 6:
			str="周五";
			break;
		case 7:
			str="周六";
			break;
		}
		return str;
    }
    /**
     * 判断是否是同一天
     * @param date1
     * @param date2
     * @return
     */
    public static int istoday(Date date1,Date date2){
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	if(format.format(date1).equals(format.format(date2))){
    		return 1;
    	}else{
    		return 0;
    	}
    }
    /**
     * 判断是否是七天之内
     * @param date
     * @return
     */
    public static boolean inSevenDay(Date date){
    	Date day = new Date();
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	try {
			if(date.getTime()<format.parse(format.format(day)).getTime()){
				return false;
			}
			Calendar cal = Calendar.getInstance();
	    	cal.setTime(day);
	    	cal.add(cal.DATE, 7);
	    	if(date.getTime()>cal.getTime().getTime()){
	    		return false;
	    	}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return true;
    }
    /**
     * 时间比大小
     * @param t1
     * @param t2
     * @return t1比t2打  返回1，小 返回-1  相等 0
     */
    public static int timeCompare(String t1,String t2){  
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");  
        Calendar c1=Calendar.getInstance();  
        Calendar c2=Calendar.getInstance();  
        try {  
            c1.setTime(formatter.parse(t1));  
            c2.setTime(formatter.parse(t2));  
        } catch (ParseException e) {  
            e.printStackTrace();  
        }  
        int result=c1.compareTo(c2);  
        return result;  
    }  
    /**
     * double  转换 int
     * @param dou
     * @return
     */
    public static int fmtDouble(Double dou){
    	 DecimalFormat df = new DecimalFormat("###");
         String value = df.format(dou);
		return Integer.parseInt(value);
    }
    /**
     * 格式化double  保留一位小数
     * @param 
     * @return
     */
    public static String fmtDouble1(Object dou){
    	java.text.DecimalFormat   df   =new   java.text.DecimalFormat("######0.0");
		return df.format(Math.rint((Double) dou));  
    }
    /**
     * 时间正则
     * @param date
     * @return
     */
    public static boolean isDate(String date)
    {
      /**
       * 判断日期格式和范围
       */
      String rexp = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";
      
      Pattern pat = Pattern.compile(rexp);  
      
      Matcher mat = pat.matcher(date);  
      
      boolean dateType = mat.matches();

      return dateType;
    }
    /**
     * 计算两个时间差几天
     * @param smdate  
     * @param bdate
     * @return
     * @throws ParseException
     */
    public static int daysBetween(Date smdate,Date bdate){   
    	try{
	        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
	        smdate=sdf.parse(sdf.format(smdate));  
	        bdate=sdf.parse(sdf.format(bdate));  
	        Calendar cal = Calendar.getInstance();    
	        cal.setTime(smdate);    
	        long time1 = cal.getTimeInMillis();                 
	        cal.setTime(bdate);    
	        long time2 = cal.getTimeInMillis();   
	        if(time2<time1){
	        	return 0;
	        }
	        long between_days=(time2-time1)/(1000*3600*24);  
	       return Integer.parseInt(String.valueOf(between_days));  
    	}catch (Exception e) {
    		return 0;
		}
    }    
}
