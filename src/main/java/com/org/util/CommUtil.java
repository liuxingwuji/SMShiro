package com.org.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author zhoushenghui
 * 
 */
public class CommUtil {
	static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String args[]) throws ParseException {
		String s="\n";
	}

	/**
	 * 取得当月的某天
	 * 
	 * @param day
	 * @return
	 */
	public static String getNowDay(int day) {
		Calendar calendar = Calendar.getInstance(); // 得到当前时间
		calendar.set(Calendar.DAY_OF_MONTH, day);// 设置为1号,当前日期既为本月第16天
		return sf.format(calendar.getTime());
	}

	/**
	 * 取得下月的某天
	 * 
	 * @param day
	 * @return
	 */
	public static String getNextDay(int day) {
		Calendar calendar = Calendar.getInstance(); // 得到当前时间
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);// 设置为1号,当前日期既为本月第16天
		return sf.format(calendar.getTime());
	}

	/**
	 * 取得上月的某天
	 * 
	 * @param day
	 * @return
	 */
	public static String getBeforetDay(int day) {
		Calendar calendar = Calendar.getInstance(); // 得到当前时间
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, day);// 设置为1号,当前日期既为本月第16天
		return sf.format(calendar.getTime());
	}

	// date类型转换为String类型
	// formatType格式为yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
	// data Date类型的时间
	public static String dateToString(Date data, String formatType) {
		return new SimpleDateFormat(formatType).format(data);
	}

	// long类型转换为String类型
	// currentTime要转换的long类型的时间
	// formatType要转换的string类型的时间格式
	public static String longToString(long currentTime, String formatType)
			throws ParseException, java.text.ParseException {
		Date date = longToDate(currentTime, formatType); // long类型转成Date类型
		String strTime = dateToString(date, formatType); // date类型转成String
		return strTime;
	}

	// string类型转换为date类型
	// strTime要转换的string类型的时间，formatType要转换的格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日
	// HH时mm分ss秒，
	// strTime的时间格式必须要与formatType的时间格式相同
	public static Date stringToDate(String strTime, String formatType)
			throws ParseException, java.text.ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(formatType);
		Date date = null;
		date = formatter.parse(strTime);
		return date;
	}

	// long转换为Date类型
	// currentTime要转换的long类型的时间
	// formatType要转换的时间格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
	public static Date longToDate(long currentTime, String formatType)
			throws ParseException, java.text.ParseException {
		Date dateOld = new Date(currentTime); // 根据long类型的毫秒数生命一个date类型的时间
		String sDateTime = dateToString(dateOld, formatType); // 把date类型的时间转换为string
		Date date = stringToDate(sDateTime, formatType); // 把String类型转换为Date类型
		return date;
	}

	// string类型转换为long类型
	// strTime要转换的String类型的时间
	// formatType时间格式
	// strTime的时间格式和formatType的时间格式必须相同
	public static long stringToLong(String strTime, String formatType)
			throws ParseException, java.text.ParseException {
		Date date = stringToDate(strTime, formatType); // String类型转成date类型
		if (date == null) {
			return 0;
		} else {
			long currentTime = dateToLong(date); // date类型转成long类型
			return currentTime;
		}
	}

	// date类型转换为long类型
	// date要转换的date类型的时间
	public static long dateToLong(Date date) {
		return date.getTime();
	}

	public static String numToDate(int number, String formatType) {
		Date date = new Date(number);
		SimpleDateFormat sdf = new SimpleDateFormat(formatType);
		return sdf.format(date);
	}
	
	/** 
	 * @Title: getIpAddr  
	 * @author kaka  www.zuidaima.com 
	 * @Description: 获取客户端IP地址   
	 * @param @return     
	 * @return String    
	 * @throws 
	 */  
	public static String getIp(HttpServletRequest request) {
		             String ip = request.getHeader("X-Forwarded-For");
		             if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
		                 //多次反向代理后会有多个ip值，第一个ip才是真实ip
		                 int index = ip.indexOf(",");
		                 if(index != -1){
		                     return ip.substring(0,index);
		                 }else{
		                     return ip;
		                }
		            }
		           ip = request.getHeader("X-Real-IP");
		            if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
		                return ip;
		            }
		            return request.getRemoteAddr();
		        }

}
