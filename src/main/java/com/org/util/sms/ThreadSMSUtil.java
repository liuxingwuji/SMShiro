package com.org.util.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 发送短信线
 * @author zsh
 *
 */
public class ThreadSMSUtil implements Runnable{
	  //private Log log = LogFactory.getLog(this.getClass());
	  private String phoneNumber;
	  private int type;
	  private String count;
	  
	  public ThreadSMSUtil(String phoneNumber, int type,String count){
		  this.phoneNumber=phoneNumber;
		  this.type =type;
		  this.count=count;
		  Thread t=new Thread(this);
		  t.start();
	  }
	   public void run() { 
			try {
				// 创建StringBuffer对象用来操作字符串
				//StringBuffer sb = new StringBuffer("http://sms.1xinxi.cn/asmx/smsservice.aspx?");
				StringBuffer sb = new StringBuffer("http://web.1xinxi.cn/asmx/smsservice.aspx?");
				String sign="长城e驾";
				// 向StringBuffer追加用户名
				sb.append("name=ccejia@126.com");

				// 向StringBuffer追加密码（登陆网页版，在管理中心--基本资料--接口密码，是28位的）
				sb.append("&pwd=6116E915B09186943846BECE73A1");

				// 向StringBuffer追加手机号码
				sb.append("&mobile="+this.phoneNumber);

				// 向StringBuffer追加消息内容转URL标准码
				sb.append("&content="+URLEncoder.encode(this.count));
				
				//追加发送时间，可为空，为空为及时发送
				sb.append("&stime=");
				
				//加签名
				sb.append("&sign="+URLEncoder.encode(sign));
				
				//type为固定值pt  extno为扩展码，必须为数字 可为空
				sb.append("&type=pt&extno=");
				// 创建url对象
				URL url = new URL(sb.toString());

				// 打开url连接
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();

				// 设置url请求方式 ‘get’ 或者 ‘post’
				connection.setRequestMethod("POST");

				// 发送
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(),"utf-8"));

				// 返回发送结果
				String inputline = in.readLine();
				String[] msg = inputline.split(",");
				
				//如果失败写入日志文件
				if(!msg[0].equals("0")){
					//log.error("【"+phoneNumber+"】发送失败,"+msg[1].toString());
					System.out.println("失败");
				}
				System.out.println("【"+phoneNumber+"】发送成功,"+count);
			} catch (Exception e) {
				//log.error("异常："+url);
				//log.error(e);
			}
	    }
}
