package com.org.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.org.util.email.MailSenderInfo;
import com.org.util.email.SimpleMailSender;

/**
 * 程序异常处理
 * 算术异常类：ArithmeticExecption

 空指针异常类：NullPointerException

 类型强制转换异常：ClassCastException

 数组负下标异常：NegativeArrayException

 数组下标越界异常：ArrayIndexOutOfBoundsException

 违背安全原则异常：SecturityException

 文件已结束异常：EOFException

 文件未找到异常：FileNotFoundException

 字符串转换为数字异常：NumberFormatException

 操作数据库异常：SQLException

 输入输出异常：IOException

 方法未找到异常：NoSuchMethodException
 */
public class OwnerException extends SimpleMappingExceptionResolver {
	public static final Log log = LogFactory.getLog(OwnerException.class);

	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		/*String accept = request.getHeader("accept");  
        if (accept != null && !(accept.indexOf("application/json") > -1   
                || (request.getHeader("X-Requested-With") != null   
                && request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {  
           // mv = super.doResolveException(request, response, handler, ex);  
        } else {  
            try {   
                // json 请求返回  
                PrintWriter writer = response.getWriter();    
                writer.write(Return.failure((ex.getMessage())));  
                writer.flush();  
            } catch (IOException e) {  
                if (log.isInfoEnabled()) {  
                    //log.info(StringUtil.getTrace(e));  
                }  
            }  
        } */ 
		doLog((HandlerMethod) handler, ex,request.getRemoteAddr());
		return new ModelAndView("outException");
	}
	
	/** 
     * 记录异常日志 
     *  
     * @param handler 
     * @param excetpion 
     */  
    private void doLog(HandlerMethod handler, Exception excetpion,String ip) {
    	log.error(excetpion.getMessage());
    	MailSenderInfo mailInfo = new MailSenderInfo();
		
		mailInfo.setToAddress("526550856@qq.com");// 收件人地址
		mailInfo.setSubject("SMShiro系统异常");// 标题
		String sendContent="";
		sendContent = "您好！"
				+ "<br>"
				+ "\r\n"
				+ "服务器："+ip+ "<br>"
				+ "以下是程序报的错误，"
				+ "，如有问题，可联系技术部"
				+ "<br><br>"
				+ "\r\n"
				+ excetpion
				+ "<br>"
				+ "-------------------------------------------------------------------------"
				+ "<br>"
				+ "\r\n"
				+ "信息技术部门"
				+ "<br>"
				+ "\r\n"
				+ "Information Technology Services IT Department "
				+ "<br>"
				+ "\r\n"
				+ "ADD: XXXXXXXXXX"
				+ "<br>"
				+ "\r\n"
				//+ "10/F TowerA YinHe SOHO Chaoyangmen Nei Street, Dongcheng District, Beijing, China."
				+ "<br>" + "\r\n" + "TEL: 010-xxxx" + "<br>"
				+ "\r\n" + "Http:" + "xxxx" + "\r\n"
				+ "<br>" + "众志成城";
		mailInfo.setContent(sendContent);// 邮件内容
		SimpleMailSender.sendQQEmail(mailInfo);
    }     

	
	public static void main(String[] args) {
		/*try {
			//sendErrorToEmail("dddd");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}
