package com.org.util;

/**
 * 常量
 * 
 * @author Administrator
 *
 */
public class Constant {
	public static final String CURRENT_USER = "user";//当前用户
	public static final String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";//强制退出
	//菜单类型
	public static String MENU_TYPE_URL="url";
	public static String MENU_TYPE_BUTTON="button";

}
