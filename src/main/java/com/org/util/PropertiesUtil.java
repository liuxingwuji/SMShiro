package com.org.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Properties;

import com.org.util.email.MailSenderInfo;

/**
 * 配置文件读取
 * 
 * @author zhoushenghui
 *
 */
public class PropertiesUtil {
//	 private static String fileName=File.separator+"/conf/email.properties";//这里是指放在classes下，如果有包的话，前面加包名即可。例：/com/web/db.properties  
	 private static String fileNamed=File.separator+"/email.properties";
	public static void main(String[] args) {
		PropertiesUtil pu=new PropertiesUtil();
		/*String readfile = "d:" + File.separator
				+ "sendMails//readfile.properties";
		String writefile = "d:" + File.separator + "writefile.properties";
		String readxmlfile = "d:" + File.separator + "readxmlfile.xml";
		String writexmlfile = "d:" + File.separator + "writexmlfile.xml";
		String readtxtfile = "d:" + File.separator + "readtxtfile.txt";
		String writetxtfile = "d:" + File.separator + "writetxtfile.txt";*/
		MailSenderInfo ms=new MailSenderInfo();
		pu.readPropertiesFile(fileNamed,ms); // 读取properties文件
//		pu.getPropertiesFile(fileNamed, "filePath",path);

	}

	/**
	 * 读取资源文件,并处理中文乱码
	 * 几种读取.properties文件的方式:
	 * InputStream in = new BufferedInputStream(new FileInputStream(name)); 
	 * InputStream in = 类名.class.getResourceAsStream(name);  
	 * InputStream in = 类名.class.getClassLoader().getResourceAsStream(name); 
	 * InputStream in = ClassLoader.getSystemResourceAsStream(name);   
	 * 
	 * Servlet中可以使用javax.servlet.ServletContext的getResourceAsStream()方法 示例： 
	 * InputStream in = context.getResourceAsStream(path);   
	 */
	public static  void readPropertiesFile(String filename,
			MailSenderInfo mailInfo) {
		Properties properties = new Properties();
		try {
//			PropertiesUtil.class.getClassLoader().getResourceAsStream("jdbc.properties");
			InputStream inputStream =PropertiesUtil.class.getResourceAsStream("/"+filename);   
			properties.load(inputStream);
			inputStream.close(); // 关闭流
		} catch (IOException e) {
			e.printStackTrace();
		}
		String serverHost = properties.getProperty("serverHost");
		String serverPort = properties.getProperty("serverPort");
		String senderEmail = properties.getProperty("senderEmail");
		String password = properties.getProperty("password");
		mailInfo.setMailServerHost(serverHost);
		mailInfo.setMailServerPort(serverPort);
		mailInfo.setValidate(true);
		mailInfo.setUserName(senderEmail.trim());
		mailInfo.setPassword(password.trim());// 您的邮箱密码
		mailInfo.setFromAddress(senderEmail.trim());
		// chinese = new String(chinese.getBytes("ISO-8859-1"), "GBK"); //
		// 处理中文乱码
	}
	/**
	 * 获取文件Properties中的某个值
	 * @param filename
	 * @param mailInfo
	 * @return
	 */
	public static String   getPropertiesFile(String filename,
			String txtName) {
		Properties properties = new Properties();
		try {
			InputStream inputStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(File.separator+filename);
			properties.load(inputStream);
			inputStream.close(); // 关闭流
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.getProperty(txtName);
	}

	/**
	 * 读取XML文件,并处理中文乱码
	 * 
	 * @param filename
	 */
	public static void readPropertiesFileFromXML(String filename) {
		Properties properties = new Properties();
		try {
			InputStream inputStream = new FileInputStream(filename);
			properties.loadFromXML(inputStream);
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String username = properties.getProperty("username");
		String passsword = properties.getProperty("password");
		String chinese = properties.getProperty("chinese"); // XML中的中文不用处理乱码，正常显示
		
	}

	/**
	 * 写资源文件，含中文
	 * 
	 * @param filename
	 */
	public static void writePropertiesFile(String filename) {
		Properties properties = new Properties();
		try {
			OutputStream outputStream = new FileOutputStream(filename);
			properties.setProperty("username", "myname");
			properties.setProperty("password", "mypassword");
			properties.setProperty("chinese", "中文");
			properties.store(outputStream, "author: shixing_11@sina.com");
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 写资源文件到XML文件，含中文
	 * 
	 * @param filename
	 */
	public static void writePropertiesFileToXML(String filename) {
		Properties properties = new Properties();
		try {
			OutputStream outputStream = new FileOutputStream(filename);
			properties.setProperty("username", "myname");
			properties.setProperty("password", "mypassword");
			properties.setProperty("chinese", "中文");
			properties.storeToXML(outputStream, "author: shixing_11@sina.com");
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
