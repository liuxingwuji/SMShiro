package com.org.util;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import net.sourceforge.pinyin4j.PinyinHelper;
/**
 * 取的汉字的拼音首字母，在字符串中替换所有的目标文本
 *
 */

public class FirstLetterUtil {
	// 判断是否是数字开头
	private static boolean beginNumber(String str) {
		boolean leaf = false;
		if (str != null && str.trim().length() >= 0) {
			str = str.substring(0, 1);
			for (int i = 0; i < 10; i++) {
				if (n_FirstLetter[i].equals(str)) {
					leaf = true;
					break;
				}
			}
		}
		return leaf;
	}

	// 判断是否是字母开头
	private static boolean beginLetter(String str) {
		boolean leaf = false;
		if (str != null && str.trim().length() >= 0) {
			str = str.substring(0, 1);
			for (int i = 0; i < 23; i++) {
				if (lc_FirstLetter[i].equalsIgnoreCase(str)) {
					leaf = true;
					break;
				}
			}
		}
		return leaf;
	}

	/**
	 * 取得给定汉字串的首字母串,即声母串
	 */
	public static synchronized String getFirstLetter(String str) {
		if (str == null || str.trim().length() == 0) {
			return "";
		}
		String _str = "";
		for (int i = 0; i < str.length(); i++) {
			_str = _str + getFirstLetter1(str.substring(i, i + 1));
		}
		return _str;
	}

	// 取得给定汉字串的首字母串,即声母串
	private static String getFirstLetter1(String inputStr) {
		if (inputStr == null || inputStr.trim().length() == 0) {
			return "";
		}
		if (beginNumber(inputStr) || beginLetter(inputStr)) {
			return inputStr;
		} else {
			String chinese = conversionStr(inputStr, "GB2312", "ISO8859-1");
			// 判断是不是汉字
			if (chinese.length() > 1) {
				// 汉字区码
				int li_SectorCode = (int) chinese.charAt(0);
				// 汉字位码
				int li_PositionCode = (int) chinese.charAt(1);
				li_SectorCode = li_SectorCode - 160;
				li_PositionCode = li_PositionCode - 160;
				// 汉字区位码
				int li_SecPosCode = li_SectorCode * 100 + li_PositionCode;
				if (li_SecPosCode > 1600 && li_SecPosCode < 5590) {
					for (int i = 0; i < 23; i++) {
						if (li_SecPosCode >= li_SecPosValue[i]
								&& li_SecPosCode < li_SecPosValue[i + 1]) {
							chinese = lc_FirstLetter[i];
							break;
						}
					}
				} else {
					int li_offset = (li_SectorCode - 56) * 94 + li_PositionCode
							- 1;
					// 二区汉字
					if (li_offset >= 0 && li_offset <= 3007) {
						chinese = ls_SecondSecTable.substring(li_offset,
								li_offset + 1);
					}
					// 非汉字字符,如图形符号或ASCII码
					else {
						chinese = inputStr;
					}
				}
			} else {
				//尝试用pinyin4j
				String[] pinyinArray =PinyinHelper.toHanyuPinyinStringArray(inputStr.charAt(0));
				if(pinyinArray!=null && pinyinArray.length>0){
					chinese = pinyinArray[pinyinArray.length-1];
					chinese=chinese.substring(0,1);
				}else{
					chinese = inputStr;
				}
			}
			return chinese;
		}
	}

	/**
	 * 字符串编码转换
	 */
	private static String conversionStr(String str, String charsetName,
			String toCharsetName) {
		try {
			str = new String(str.getBytes(charsetName), toCharsetName);
		} catch (java.io.UnsupportedEncodingException ex) {
			System.out.println("字符串编码转换异常：" + ex.getMessage());
		}
		return str;
	}

	private final static int[] li_SecPosValue = { 1601, 1637, 1833, 2078, 2274,
			2302, 2433, 2594, 2787, 3106, 3212, 3472, 3635, 3722, 3730, 3858,
			4027, 4086, 4390, 4558, 4684, 4925, 5249, 5590 };

	private final static String[] lc_FirstLetter = { "a", "b", "c", "d", "e",
			"f", "g", "h", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
			"t", "w", "x", "y", "z" };

	private final static String[] n_FirstLetter = { "0", "1", "2", "3", "4",
			"5", "6", "7", "8", "9" };
	private final static String ls_SecondSecTable = "cjwgnspgcgne[y[btyyzdxykygt[jnnjqmbsgzscyjsyy[pgkbzgy[ywjkgkljywkpjqhy[w[dzlsgmrypywwcckznkyygttnjjnykkzytcjnmcylqlypyqfqrpzslwbtgkjfyxjwzltbncxjjjjtxdttsqzycdxxhgck[phffss[ybgxlppbyll[hlxs[zm[jhsojnghdzqyklgjhsgqzhxqgkezzwyscscjxyeyxadzpmdssmzjzqjyzc[j[wqjbyzpxgznzcpwhkxhqkmwfbpbydtjzzkqhylygxfptyjyyzpszlfchmqshgmxxsxj[[dcsbbqbefsjyhxwgzkpylqbgldlcctnmayddkssngycsgxlyzaybnptsdkdylhgymylcxpy[jndqjwxqxfyyfjlejpzrxccqwqqsbnkymgplbmjrqcflnymyqmsqyrbcjthztqfrxqhxmjjcjlxqgjmshzkbswyemyltxfsydswlycjqxsjnqbsctyhbftdcyzdjwyghqfrxwckqkxebptlpxjzsrmebwhjlbjslyysmdxlclqkxlhxjrzjmfqhxhwywsbhtrxxglhqhfnm[ykldyxzpylgg[mtcfpajjzyljtyanjgbjplqgdzyqyaxbkysecjsznslyzhsxlzcghpxzhznytdsbcjkdlzayfmydlebbgqyzkxgldndnyskjshdlyxbcghxypkdjmmzngmmclgwzszxzjfznmlzzthcsydbdllscddnlkjykjsycjlkwhqasdknhcsganhdaashtcplcpqybsdmpjlpzjoqlcdhjjysprchn[nnlhlyyqyhwzptczgwwmzffjqqqqyxaclbhkdjxdgmmydjxzllsygxgkjrywzwyclzmssjzldbyd[fcxyhlxchyzjq[[qagmnyxpfrkssbjlyxysyglnscmhzwwmnzjjlxxhchsy[[ttxrycyxbyhcsmxjsznpwgpxxtaybgajcxly[dccwzocwkccsbnhcpdyznfcyytyckxkybsqkkytqqxfcwchcykelzqbsqyjqcclmthsywhmktlkjlycxwheqqhtqh[pq[qscfymndmgbwhwlgsllysdlmlxpthmjhwljzyhzjxhtxjlhxrswlwzjcbxmhzqxsdzpmgfcsglsxymjshxpjxwmyqksmyplrthbxftpmhyxlchlhlzylxgsssstclsldclrpbhzhxyyfhb[gdmycnqqwlqhjj[ywjzyejjdhpblqxtqkwhlchqxagtlxljxmsl[htzkzjecxjcjnmfby[sfywybjzgnysdzsqyrsljpclpwxsdwejbjcbcnaytwgmpapclyqpclzxsbnmsggfnzjjbzsfzyndxhplqkzczwalsbccjx[yzgwkypsgxfzfcdkhjgxdlqfsgdslqwzkxtmhsbgzmjzrglyjbpmlmsxlzjqqhzyjczydjwbmyklddpmjegxyhylxhlqyqhkycwcjmyyxnatjhyccxzpcqlbzwwytwbqcmlpmyrjcccxfpznzzljplxxyztzlgdldcklyrzzgqtgjhhgjljaxfgfjzslcfdqzlclgjdjcsnzlljpjqdcclcjxmyzftsxgcgsbrzxjqqctzhgyqtjqqlzxjylylbcyamcstylpdjbyregklzyzhlyszqlznwczcllwjqjjjkdgjzolbbzppglghtgzxyghzmycnqsycyhbhgxkamtxyxnbskyzzgjzlqjdfcjxdygjqjjpmgwgjjjpkqsbgbmmcjssclpqpdxcdyyky[cjddyygywrhjrtgznyqldkljszzgzqzjgdykshpzmtlcpwnjafyzdjcnmwescyglbtzcgmssllyxqsxsbsjsbbsgghfjlypmzjnlyywdqshzxtyywhmzyhywdbxbtlmsyyyfsxjc[dxxlhjhf[sxzqhfzmzcztqcxzxrttdjhnnyzqqmnqdmmg[ydxmjgdhcdyzbffallztdltfxmxqzdngwqdbdczjdxbzgsqqddjcmbkzffxmkdmdsyyszcmljdsynsbrskmkmpcklgdbqtfzswtfgglyplljzhgj[gypzltcsmcnbtjbqfkthbyzgkpbbymtdssxtbnpdkleycjnyddykzddhqhsdzsctarlltkzlgecllkjlqjaqnbdkkghpjtzqksecshalqfmmgjnlyjbbtmlyzxdcjpldlpcqdhzycbzsczbzmsljflkrzjsnfrgjhxpdhyjybzgdlqcsezgxlblgyxtwmabchecmwyjyzlljjyhlg[djlslygkdzpzxjyyzlwcxszfgwyydlyhcljscmbjhblyzlycblydpdqysxqzbytdkyxjy[cnrjmpdjgklcljbctbjddbblblczqrppxjcjlzcshltoljnmdddlngkaqhqhjgykheznmshrp[qqjchgmfprxhjgdychghlyrzqlcyqjnzsqtkqjymszswlcfqqqxyfggyptqwlmcrnfkkfsyylqbmqammmyxctpshcptxxzzsmphpshmclmldqfyqxszyydyjzzhqpdszglstjbckbxyqzjsgpsxqzqzrqtbdkyxzkhhgflbcsmdldgdzdblzyycxnncsybzbfglzzxswmsccmqnjqsbdqsjtxxmbltxzclzshzcxrqjgjylxzfjphymzqqydfqjjlzznzjcdgzygctxmzysctlkphtxhtlbjxjlxscdqxcbbtjfqzfsltjbtkqbxxjjljchczdbzjdczjdcprnpqcjpfczlclzxzdmxmphjsgzgszzqlylwtjpfsyasmcjbtzkycwmytcsjjljcqlwzmalbxyfbpnlsfhtgjwejjxxglljstgshjqlzfkcgnnnszfdeqfhbsaqtgylbxmmygszldydqmjjrgbjtkgdhgkblqkbdmbylxwcxyttybkmrtjzxqjbhlmhmjjzmqasldcyxyqdlqcafywyxqhz";
	public static void main(String[] args) {
	    	String firstLetter = FirstLetterUtil.getFirstLetter("huang马祎煇芃鋆");
	    	System.out.println(firstLetter);
	    }
}
