package com.org.util;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class HttpUtil {
	/**
	 * 向指定URL发送GET方法的请求
	 * 
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
//			for (String key : map.keySet()) {
//				System.out.println(key + "--->" + map.get(key));
//			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	
	/**
     * Post Request
     * @return
     * @throws Exception
     */
    public static String httpURL(String url, String parameterData) throws Exception {
        //String parameterData = "username=nickhuang&blog=http://www.cnblogs.com/nick-huang/";
        
        URL localURL = new URL(url);
        URLConnection connection = localURL.openConnection();
        HttpURLConnection httpURLConnection = (HttpURLConnection)connection;
        
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Accept-Charset", "utf-8");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(parameterData.length()));
        OutputStream outputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();
        String tempLine = null;
        
        try {
            outputStream = httpURLConnection.getOutputStream();
            outputStreamWriter = new OutputStreamWriter(outputStream,"UTF-8");//传输时字符格式设置
            
            outputStreamWriter.write(parameterData.toString());
            outputStreamWriter.flush();
            
            if (httpURLConnection.getResponseCode() >= 300) {
                throw new Exception("HTTP Request is not success, Response code is " + httpURLConnection.getResponseCode());
            }
            
            inputStream = httpURLConnection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
            reader = new BufferedReader(inputStreamReader);
            
            while ((tempLine = reader.readLine()) != null) {
                resultBuffer.append(tempLine);
            }
            
        } finally {
            
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
            
            if (outputStream != null) {
                outputStream.close();
            }
            
            if (reader != null) {
                reader.close();
            }
            
            if (inputStreamReader != null) {
                inputStreamReader.close();
            }
            
            if (inputStream != null) {
                inputStream.close();
            }
            
        }

        return resultBuffer.toString();
    }
	public static void main(String[] args) {
		String json = HttpUtil.sendPost("http://apis.haoservice.com/weizhang/citys","key=a0b0470edb50451e8ca8d37493288e21&paybyvas=false");


		JSONObject jsonObject = JSONObject.fromObject(json);
		String result = jsonObject.optString("result");
		JSONArray listsArray = jsonObject.getJSONArray("result");
		//jsonObject = JSONObject.fromObject(result);
		//String citys = jsonObject.optString("citys");
		//JSONArray listsArray = jsonObject.getJSONArray("result");
		//System.out.println(listsArray.size()+"--------"+result);
		for(int i=0,l=listsArray.size();i<l;i++)
		{
            /*通过列表获得出第一条记录的列表,包含了信息的id,发送者...
            其中该列表中嵌套着另一个JSON串*/
			JSONObject object = listsArray.getJSONObject(i);
			//获取嵌套的JSON串
			String users = object.optString("citys");
			//将获取的嵌套的JSON串,再解释一次, 得到可使用的对象.
			//JSONObject user = new JSONObject(users);
			//System.out.println(object.getString("id")+ ":" + user.getString("birthday"));
			System.out.println("----"+users);
		}
		// 存放违章信息列表
		//List<CarCity> list = new ArrayList<CarCity>();
		// 封装数据到列表
		/*for(int i=0;i<listsArray.size();i++){
			HashMap<String, String> map = new HashMap<String, String>();
			CarCity carCity = new CarCity();

			JSONObject futurnObject = JSONObject.fromObject(listsArray.getJSONObject(i));
			Iterator iterator = futurnObject.keys();
			while (iterator.hasNext()) {
				String key = String.valueOf(iterator.next());
				String value = (String) futurnObject.get(key);
				map.put(key, value);
			}

			carCity.setCity_name(map.get("city_name")); // 违章时间
			carCity.setCity_code(map.get("city_code")); // 违章区域
			carCity.setAbbr(map.get("abbr"));   // 违章行为
			carCity.setEngine(Integer.parseInt(map.get("engine")));   // 违章扣分
			carCity.setEngineno(Integer.parseInt(map.get("engineno"))); // 违章罚款
			carCity.setClassa(Integer.parseInt(map.get("classa"))); // 是否处理

			carCity.setClassno(Integer.parseInt(map.get("classno"))); // 是否处理
			carCity.setRegist(Integer.parseInt(map.get("regist"))); // 是否处理
			carCity.setRegistno(Integer.parseInt(map.get("registno"))); // 是否处理


			list.add(carCity);
		}*/
		//System.out.println(list.toString());
	}
}

