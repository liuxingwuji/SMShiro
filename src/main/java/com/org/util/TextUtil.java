package com.org.util;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * 文本操作
 * 
 * @author zhoushenghui
 *
 */
public class TextUtil {
	/**
	 * 功能：Java读取txt文件的内容 步骤： 1：先获得文件句柄 2：获得文件句柄当做是输入一个字节码流，需要对这个输入流进行读取
	 * 3：读取到输入流后，需要读取生成字节流 4：一行一行的输出。readline()。 备注：需要考虑的是异常情况
	 * 
	 * @param filePath
	 */
	public static void readTxtFile(String filePath) {
		try {
			String encoding = "utf-8";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
				}
				read.close();
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 写txt文件
	 */
	private static void writerTxt() {
		BufferedWriter fw = null;
		try {
			File file = new File("e://test.txt");
			fw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file, true), "UTF-8"));
			// 指定编码格式，以免读取时中文字符异常
			fw.append("我写入的内容");
			fw.newLine();
			fw.append("我又写入的内容0");
			fw.flush(); // 全部写入缓存中的内容
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static InputStream getInputStream(String path) throws Exception{
		URL url = new URL(path);// 构建一个URL对象
		HttpURLConnection con = (HttpURLConnection) url.openConnection();// 打开连接
		con.setConnectTimeout(5000);// 设置超时时间
		con.setRequestMethod("GET");// 设置请求方式
		System.out.println("-----:"+con.getResponseCode());
		if (con.getResponseCode() == 200) {// 判断是否请求成功，状态码为200
			return con.getInputStream();
			
		}
		return null;
	}
	
	public static void main(String[] args) {
		try {
			InputStream s=getInputStream("http://192.168.1.199/school/html/version.xml");
			ParseXmlService service = new ParseXmlService();
			System.out.println("---------"+service.parseXml(s));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
