package com.org.util.email;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
/**
 * 验证发送者邮箱
 * @author zhoushenghui
 *
 */
public class MyAuthenticator {
	String userName=null;
	String password=null;
	 
	public MyAuthenticator(){
	}
	public MyAuthenticator(String username, String password) { 
		this.userName = username; 
		this.password = password; 
	} 
	protected PasswordAuthentication getPasswordAuthentication(){
		return new PasswordAuthentication(userName, password);
	}
}
