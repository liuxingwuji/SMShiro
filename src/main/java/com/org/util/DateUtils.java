package com.org.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	private static SimpleDateFormat yyyyMMdd_Form = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat yyyy_MM_dd_Form = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat yyyy_MM_dd_HH_mm_ss_Form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat yyyyMMddHHmmssSSS_Form = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	private static SimpleDateFormat yyyyMMddHHmmss_Form = new SimpleDateFormat("yyyyMMddHHmmss");
	private static SimpleDateFormat yyyy_MM_dd_hh_mm_ss_Form1 = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
	private static SimpleDateFormat HHmmsss_Form = new SimpleDateFormat("HHmmss");
	private static SimpleDateFormat HHmmssSSS_Form = new SimpleDateFormat("HHmmssSSS");

	private static String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";

	
	public static synchronized String getStringHmsDate(Date date) {
		return HHmmsss_Form.format(date);
	}
	/***
	 * 把Date转化成String类型的日期格式
	 *
	 * @param date
	 * @param pattern
	 *            (返回的时间格式)
	 * @return
	 */
	public static synchronized String getStringDateFromDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	/***
	 * 把字符串格式的日期转换为date型
	 * 
	 * @param dateString
	 * @return
	 */
	public static synchronized Date getDate_yyyyMMdd_Form(String dateString) {
		try {
			if (dateString.length() == 8) {
				return yyyyMMdd_Form.parse(dateString);
			} else if (dateString.length() == 10) {
				return yyyy_MM_dd_Form.parse(dateString);
			} else if (dateString.length() == 19) {
				return yyyy_MM_dd_Form.parse(dateString);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage() + "日期转换异常！");
		}
		return null;
	}

	/***
	 * 把Date转化成String类型的日期格式yyyy-MM-dd
	 *
	 * @param date
	 * @return
	 */
	public static synchronized String getString_yyyy_MM_dd_Form(Date date) {
		return yyyy_MM_dd_Form.format(date);
	}





	/***
	 * 把Date转化成String类型的日期格式 yyyyMMdd
	 *
	 * @param date
	 * @return
	 */
	public static synchronized String getString_yyyyMMdd_Form(Date date) {
		return yyyyMMdd_Form.format(date);
	}

	/***
	 * 把Date转化成String类型的日期格式 yyyyMMddhhmmssSSS
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String getString_yyyyMMddHHmmssSSS_Form(Date date) {
		return yyyyMMddHHmmssSSS_Form.format(date);
	}

	/***
	 * 把Date转化成String类型的日期格式 yyyyMMddhhmmss
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String getString_yyyyMMddHHmmss_Form(Date date) {
		return yyyyMMddHHmmss_Form.format(date);
	}




	/**
	 * 获取格式化后的日期字符串 yyyy_MM_dd_HH_mm_ss
	 * @param date
	 * @return
	 */
	public static synchronized String getString_yyyy_MM_dd_HH_mm_ss_Form(Date date) {
		return yyyy_MM_dd_HH_mm_ss_Form.format(date);
	}
	/**
	 * 获取格式化后的日期 yyyy_MM_dd_HH_mm_ss
	 * @param date
	 * @return
	 */
	public static synchronized Date getDate_yyyy_MM_dd_HH_mm_ss_Form(Date date) throws ParseException {
		return yyyy_MM_dd_HH_mm_ss_Form.parse(yyyy_MM_dd_HH_mm_ss_Form.format(date));
	}

	/**
	 * 当前时间前一个月
	 *
	 * @return
	 */
	public static synchronized String getPreDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		return yyyy_MM_dd_Form.format(calendar.getTime());
	}

	/**
	 * 获取当前日期时间的开始
	 *
	 * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
	 */
	public static Date getNowBeginDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		Date start = calendar.getTime();
		return start;
	}

	/**
	 * 获取当前日期时间的结束
	 *
	 * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
	 */
	public static Date getNowEndDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.SECOND, -1);
		Date end = calendar.getTime();
		return end;
	}

	/***
	 * 当前时间到天
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static synchronized Date getPreDay(Date date) throws ParseException {
		return yyyyMMdd_Form.parse(yyyyMMdd_Form.format(date));
	}

	/***
	 * 当前时间到天
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static synchronized Date getPreDay_1(Date date) throws ParseException {
		return yyyy_MM_dd_Form.parse(yyyy_MM_dd_Form.format(date));
	}

	/**
	 * 两个日期相差天数
	 *
	 * @param bigTime
	 * @param smallTime
	 * @return
	 */
	public static long daysOfTwo(String bigTime, String smallTime) {
		long quot = 0;
		try {
			Date date1 = yyyy_MM_dd_Form.parse(bigTime);
			Date date2 = yyyy_MM_dd_Form.parse(smallTime);
			if (date1.after(date2)) {
				quot = date1.getTime() - date2.getTime();
			} else {
				quot = date2.getTime() - date1.getTime();
			}
			quot = quot / 1000 / 60 / 60 / 24;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quot;
	}

	/**
	 * 根据某人的出生年月日，计算年龄
	 * 
	 * @param birthDay
	 * @return
	 * @throws Exception
	 */
	public static int getAge(Date birthDay) throws Exception {
		Calendar cal = Calendar.getInstance();

		if (cal.before(birthDay)) {
			throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
		}

		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH) + 1;
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);

		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;

		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth
				age--;
			}
		}

		return age;
	}

	/***
	 * 当前时间的年和月份
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public static synchronized String getCurrMonth() throws ParseException {
		String currDate = getString_yyyyMMdd_Form(new Date());
		return currDate.substring(0, 8);
	}

	/***
	 * 计算两个日期相差的小时数
	 * 
	 * @param startTime
	 * @param endTime
	 * @param str
	 * @return
	 */
	public static Long dateDiff(String startTime, String endTime, String str) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(yyyy_MM_dd_HH_mm_ss);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long ns = 1000;// 一秒钟的毫秒数
		long diff;
		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		// 获得两个时间的毫秒时间差异
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			day = diff / nd;// 计算差多少天
			hour = diff % nd / nh + day * 24;// 计算差多少小时
			min = diff % nd % nh / nm + day * 24 * 60;// 计算差多少分钟
			sec = diff % nd % nh % nm / ns;// 计算差多少秒
			// 输出结果
			if (str.equalsIgnoreCase("h")) {
				return hour;
			} else {
				return min;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return day;
	}

	/***
	 * 获取某月第一天
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static synchronized String getMonthFirstDay( Date date,int month) throws ParseException {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		if (date != null && !"".equals(date)) {
			
			cal_1.setTime(date);
			if(month!=0){
				cal_1.add(Calendar.MONTH, month);
			}
			
			cal_1.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
			return yyyy_MM_dd_Form.format(cal_1.getTime());
		}else{
			return null;
		}
		
	}

	/***
	 * 获取某月最后一天
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static synchronized String getMonthLastDay(Date date,int month) throws ParseException {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		if (date != null && !"".equals(date)) {
			cal_1.setTime(date);
			cal_1.add(Calendar.MONTH, -1);
			cal_1.set(Calendar.DAY_OF_MONTH, cal_1.getActualMaximum(Calendar.DAY_OF_MONTH));
			return yyyy_MM_dd_Form.format(cal_1.getTime());
		}else{
			return null;
		}
		
	}

	/***
	 * 获取某天的几个小时一个时间段
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static SimpleDateFormat hh = new SimpleDateFormat("HH");

	/**
	 * 获取某天的几个小时一个时间段
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            几个小时之后
	 * @return
	 * @throws ParseException
	 */
	public static synchronized String getHH(Date date, int n) {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		cal_1.setTime(date);
		// cal_1.add(Calendar.HOUR, 2);
		String startHH = hh.format(cal_1.getTime());
		cal_1.add(Calendar.HOUR, n);
		String endHH = hh.format(cal_1.getTime());
		return startHH + ":00-" + endHH + ":00";
	}

	private static SimpleDateFormat ss = new SimpleDateFormat("HH");

	/**
	 * 计算某个时间在一个时间范围内
	 * 
	 * @param date
	 * @return
	 */
	public static boolean betweenHH(Date date) {
		SimpleDateFormat ss = new SimpleDateFormat("HH");
		String hh = ss.format(new Date());
		if (Integer.parseInt(hh) >= 18 || Integer.parseInt(hh) <= 8) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取当前日期所在的周一、周日日期
	 * 
	 * @param time
	 * @return
	 */
	public static String getMondayAndWeekday(Date time, String type) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(time);

		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了

		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天

		if (1 == dayWeek) {

			cal.add(Calendar.DAY_OF_MONTH, -1);

		}

		System.out.println("要计算日期为:" + yyyy_MM_dd_Form.format(cal.getTime())); // 输出要计算日期

		cal.setFirstDayOfWeek(Calendar.MONDAY);// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一

		int day = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天

		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值

		String imptimeBegin = yyyy_MM_dd_Form.format(cal.getTime());

		if (type.equals("MONADY")) {
			return imptimeBegin;
		} else {
			cal.add(Calendar.DATE, 6);
			String imptimeEnd = yyyy_MM_dd_Form.format(cal.getTime());
			return imptimeEnd;
		}

	}

	/**
	 * 获取当前日期是星期几
	 * 
	 */
	public static String getWday(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int weekDay = c.get(Calendar.DAY_OF_WEEK);
		if (Calendar.MONDAY == weekDay) {
			return "星期一";
		}
		if (Calendar.TUESDAY == weekDay) {
			return "星期二";
		}
		if (Calendar.WEDNESDAY == weekDay) {
			return "星期三";
		}
		if (Calendar.THURSDAY == weekDay) {
			return "星期四";
		}
		if (Calendar.FRIDAY == weekDay) {
			return "星期五";
		}
		if (Calendar.SATURDAY == weekDay) {
			return "星期六";
		}
		if (Calendar.SUNDAY == weekDay) {
			return "星期日";
		}
		return null;
	}
	/**
	 * 获取某天的前一天
	 * @param date
	 * @return
	 */
	public static Date getYesterday(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, -1);
		return c.getTime();
	}
	/***
	 * 每个时间按时、分、秒、毫秒的顺序组合，如20:15:25.362则为201525362
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String getStringHMSS(Date date) {
		return HHmmssSSS_Form.format(date);
	}
	public static void main(String[] args) throws ParseException {// dateFormat
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -1);
		
		System.out.println(yyyy_MM_dd_Form.format(calendar.getTime()));
	}     
}
