package com.baidu.ueditor.upload;

import com.baidu.ueditor.define.State;

import java.net.URISyntaxException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public class Uploader {
	private HttpServletRequest request = null;
	private Map<String, Object> conf = null;

	public Uploader(HttpServletRequest request, Map<String, Object> conf) {
		this.request = request;
		this.conf = conf;
	}

	public final State doExec() {
		String filedName = (String) this.conf.get("fieldName");
		State state = null;
		try {
		if ("true".equals(this.conf.get("isBase64"))) {
			state = Base64Uploader.save(this.request.getParameter(filedName),
					this.conf);
		} else {
			String path=this.getClass().getResource("/").toURI().getPath();
			 path = path.substring(0, path.length() - 17);
			 path = path.substring(0,path.lastIndexOf("/")+1) ;
			 this.conf.put("rootPath", path);
			state = BinaryUploader.save(this.request, this.conf);
		}
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return state;
	}
}
