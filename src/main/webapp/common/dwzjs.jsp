<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>commonJS</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	 	<link href="<%=path %>/js/dwz/themes/azure/style.css" rel="stylesheet" type="text/css" media="screen">
		<link href="<%=path %>/js/dwz/themes/css/core.css" rel="stylesheet" type="text/css" media="screen">
		<link href="<%=path %>/js/dwz/themes/css/print.css" rel="stylesheet" type="text/css" media="print">
		<link href="<%=path %>/js/dwz/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen">
		
		<script src="<%=path %>/js/dwz/js/jquery-2.1.4.min.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/jquery.cookie.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/jquery.bgiframe.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/xheditor/xheditor-1.2.2.min.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/uploadify/scripts/jquery.uploadify.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/raphael.js"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/g.raphael.js"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/g.bar.js"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/g.line.js"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/g.pie.js"></script>
		<script type="text/javascript" src="<%=path %>/js/dwz/chart/g.dot.js"></script>
		
		<script src="<%=path %>/js/dwz/js/dwz.core.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.util.date.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.validate.method.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.barDrag.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.drag.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.tree.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.accordion.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.ui.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.theme.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.switchEnv.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.alertMsg.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.contextmenu.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.navTab.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.tab.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.resize.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.dialog.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.dialogDrag.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.sortDrag.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.cssTable.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.stable.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.taskBar.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.ajax.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.pagination.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.database.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.datepicker.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.effects.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.panel.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.checkbox.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.history.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.combox.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.file.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.print.js" type="text/javascript"></script>
		<script src="<%=path %>/js/dwz/js/dwz.regional.zh.js" type="text/javascript"></script>
  </head>
  
  
</html>
