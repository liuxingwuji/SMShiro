<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>主页</title>
 <link href="<%=path %>/js/assets/css/bootstrap.css" rel="stylesheet" />
 <!-- FONTAWESOME STYLES-->
 <link href="<%=path %>/js/assets/css/font-awesome.css" rel="stylesheet" />

 <link href="<%=path %>/js/assets/css/basic.css" rel="stylesheet" />
 <!--CUSTOM MAIN STYLES-->
 <link href="<%=path %>/js/assets/css/custom.css" rel="stylesheet" />
 <script src="<%=path %>/js/assets/js/jquery-1.10.2.js"></script>
 <!-- BOOTSTRAP SCRIPTS -->
 <script src="<%=path %>/js/assets/js/bootstrap.js"></script>
 <!-- METISMENU SCRIPTS -->

 <!-- CUSTOM SCRIPTS -->
 <script src="<%=path %>/js/assets/js/custom.js"></script>
</head>
<body>
<!-- /. ROW  -->

<div class="row">
 <div class="col-md-8">
  <div class="row">
   <div class="col-md-12">

    <div id="reviews" class="carousel slide" data-ride="carousel">

     <div class="carousel-inner">
      <div class="item active">

       <div class="col-md-10 col-md-offset-1">

        <h4><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit onec molestie non sem vel condimentum. <i class="fa fa-quote-right"></i></h4>
        <div class="user-img pull-right">
         <img src="<%=path %>/js/assets/img/user.gif" alt="" class="img-u image-responsive" />
        </div>
        <h5 class="pull-right"><strong class="c-black">Lorem Dolor</strong></h5>
       </div>
      </div>
      <div class="item">
       <div class="col-md-10 col-md-offset-1">

        <h4><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit onec molestie non sem vel condimentum. <i class="fa fa-quote-right"></i></h4>
        <div class="user-img pull-right">
         <img src="<%=path %>/js/assets/img/user.png" alt="" class="img-u image-responsive" />
        </div>
        <h5 class="pull-right"><strong class="c-black">Lorem Dolor</strong></h5>
       </div>

      </div>
      <div class="item">
       <div class="col-md-10 col-md-offset-1">

        <h4><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipiscing  Lorem ipsum dolor sit amet, consectetur adipiscing elit onec molestie non sem vel condimentum. <i class="fa fa-quote-right"></i></h4>
        <div class="user-img pull-right">
         <img src="<%=path %>/js/assets/img/user.gif" alt="" class="img-u image-responsive" />
        </div>
        <h5 class="pull-right"><strong class="c-black">Lorem Dolor</strong></h5>
       </div>
      </div>
     </div>
     <!--INDICATORS-->
     <ol class="carousel-indicators">
      <li data-target="#reviews" data-slide-to="0" class="active"></li>
      <li data-target="#reviews" data-slide-to="1"></li>
      <li data-target="#reviews" data-slide-to="2"></li>
     </ol>
     <!--PREVIUS-NEXT BUTTONS-->

    </div>

   </div>

  </div>
  <!-- /. ROW  -->
  <hr />
  <div class="copyrights">Collect from <a href="http://www.cssmoban.com/"  title="网站模板">网站模板</a></div>

  <div class="panel panel-default">

   <div id="carousel-example" class="carousel slide" data-ride="carousel" style="border: 5px solid #000;">

    <div class="carousel-inner">
     <div class="item active">

      <img src="<%=path %>/js/assets/img/slideshow/1.jpg" alt="" />

     </div>
     <div class="item">
      <img src="<%=path %>/js/assets/img/slideshow/2.jpg" alt="" />

     </div>
     <div class="item">
      <img src="<%=path %>/js/assets/img/slideshow/3.jpg" alt="" />

     </div>
    </div>
    <!--INDICATORS-->
    <ol class="carousel-indicators">
     <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
     <li data-target="#carousel-example" data-slide-to="1"></li>
     <li data-target="#carousel-example" data-slide-to="2"></li>
    </ol>
    <!--PREVIUS-NEXT BUTTONS-->
    <a class="left carousel-control" href="#carousel-example" data-slide="prev">
     <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example" data-slide="next">
     <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
   </div>
  </div>

 </div>

</div>
</body>
</html>