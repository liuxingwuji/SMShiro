<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>信息综合管理系统</title>
    <link rel="shortcut icon" href="<%=path %>/images/head.ico" type="image/x-icon"/>

    <link href="<%=path %>/js/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<%=path %>/js/assets/css/font-awesome.css" rel="stylesheet" />

    <script src="<%=path %>/js/jquery/jquery-2.0.3.min.js"></script>
    <style type="text/css">
        footer{position:absolute;bottom:0;width:100%;height:60px;margin-top:-100px;clear:both; text-align:center;}
    </style>
    <script type="text/javascript">
        $(function () {
            if (window != top){
                top.location.href = location.href;

            }
        });

        function keyLogin(e)
        {
            var targ
            if (!e) var e = window.event
            //  if (e.target) targ = e.target
            // else if (e.srcElement) targ = e.srcElement
            //if (targ.nodeType == 3) // defeat Safari bug
            // targ = targ.parentNode
            // var tname
            // tname=targ.tagName
            // alert("You clicked on a " + tname + " element."+e.keyCode)
            if (e.keyCode==13)  //回车键的键值为13
                document.getElementById("sub").click(); //调用登录按钮的登录事件
        }
    </script>
</head>
<body style="background-color: #E2E2E2;" onkeydown="keyLogin(event)">
<div class="container">
    <div class="row text-center " style="padding-top:100px;">
        <div class="col-md-12">
            <h2>信息综合管理系统</h2>
        </div>
    </div>
    <div class="row ">

        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">

            <div class="panel-body">
                <%--<form role="form" id="logins"  method="post" action="<%=path %>/logins/login.do">--%>
                <form action="<%=path %>/logins/login.do" method="post">
                    <hr />
                    <h5>请输入帐号与密码</h5>

                    <br />
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                        <input type="text" name="username" class="form-control" placeholder="帐号 " value="<shiro:principal/>"/>
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                        <input type="password" name="password" class="form-control"  placeholder="密码" />
                    </div>
                    <div id="message" style="text-align: center">
                        <font color="red">
                            ${message_login }
                            <c:if test="${not empty param.kickout}">您被踢出登录。</c:if>
                            ${error}
                        </font></div>
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="rememberMe" value="true"> 记住帐号
                        </label>
                        <span class="pull-right">
                                                   <a href="index.html" >忘记密码 ? </a>
                                            </span>
                    </div>
                    <input type="submit" value="登录" class="btn btn-primary ">
                    <hr />
                    没有注册 ? <a href="index.html" >请点击这里 </a> 或者返回 <a href="index.html">登录页面</a>
                </form>
            </div>

        </div>


    </div>

</div>
<footer id="footer" class="footer">
    <div class="footer-inner" >

        <nav class="footerNav">
            <a href="#" target="_blank">首页</a>

            <a href="#" target="_blank" style="margin-right:10px">您的满意是我们最大的动力</a>|<span class="copyright">ZSH工作室版权所有&copy;2010-2018</span>
        </nav>
    </div>
</footer>
</body>
</html>
