﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
  <%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>日志信息</title>
<%@ include file="/common/ligerUIJS.jsp"%>
    <script language="javascript" type="text/javascript" src="<%=path %>/js/My97DatePicker/WdatePicker.js"></script>  
    <script type="text/javascript">
       var grid = null;
       var win=null;
        $(function ()
        {
        	$("#grade").ligerComboBox({
                url: '<%=path %>/school/getSchoolGrade.do?schoolId=83', 
                ajaxType: 'get'
            }); 
           /*  $("#toptoolbar").ligerToolBar({ items: [  
                                                    {text: '导出Excel',id:'excel',icon:'print',click:itemclick},  
                                                    {text: '导出Word' ,id:'word',icon:'print',click:itemclick}                                                     
                                               ]  
                                      });  */
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '操作信息', name: 'description', align: 'left', width: 500} ,
              /*     { display: '类型', name: 'state', width: 100 ,render: function (record, rowindex, value, column) {
           	       //this     这里指向grid
           	       //record   行数据
           	       //rowindex 行索引
           	       //value    当前的值，对应record[column.name]
           	       //column   列信息
           	       var v;
           	      if(value=='1'){
           	    	 v='支付成功' 
           	      } if(value=='2'){
           	    	 v='分期未签约' 
           	      } 
           	   if(value=='7'){
         	    	 v='分期已签约' 
         	      } 
           	      if(value=='4'){
           	    	  v='<font color="red">退学</font>';
           	      }
           	       return v;
           	 			}
             		}, */
             	
                  { display: '操作员', name: 'name', width: 150 },
                  { display: '操作日期', name: 'createdate', width: 150, type:'date',render: function (item)
                      {
                      return item.createdate==null?'':new Date(item.createdate+8*3600*1000).toISOString().replace(/T/g,' ').replace(/\.[\d]{3}Z/,'');
                  } }
                  
                ], 
                url:"<%=path %>/sysUser/logsList.do",
                root :'Rows',                       //数据源字段名
                record:'Total',
                rownumbers:true,
                pageSize: 10, 
                pageSizeOptions :[10, 20, 30, 40],
                width: '100%', 
                height: '99%'
                
            }); 
        });
        
        function f_search(val)
        {	
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'description', value: $("#description").val()},
                { name: 'name', value: $("#name").val()}
              /*   { name: 'beginTime', value: $("#beginTime").val()},
                { name: 'endTime', value: $("#endTime").val()} */
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
        
      
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}
    	
    	
    	
    	
  
    	
    	
    	
    </script>
</head>
<body >
	<p align="center">
	<div id="toptoolbar"></div> 
	<div id="searchbar" style="background-color: #C6E2FF;height: auto;width: 100%;padding: 10px">
		     操作信息：<input id="description"  class="fx_k" name="description" type="text"  class="fx_k" style="width: 180px;"/>&nbsp;&nbsp;
		  &nbsp;&nbsp;
		    操作员：<input id="name"  class="fx_k" name="name" type="text"  class="fx_k"/>&nbsp;&nbsp;
		    <input id="btnOK" type="button" value="查询" name="search" onclick="f_search(this.name)" />&nbsp;&nbsp;
		    
</div> 
<div id="maingrid" style="margin: 20; padding: 0;scrolling="no"">

</div> 
	</p>
 
</body>
</html>