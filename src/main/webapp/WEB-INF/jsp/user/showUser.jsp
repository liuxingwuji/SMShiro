<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		div{ 

　　float:cneter; 

　　min-height:200px;　　/*当div内容不超过200px高度时，div高度为200px*/ 

　　height:auto;　　/*当div内容超过200px高度时，div自动增高*/ 

　　min-width:100px;　　/*当div内容不超过100px宽度时，div宽度为100px*/ 

　　width:auto;　　/*当div内容超过100px宽度时，div自动增宽*/ 
	overflow:hidden;   

　　}

        *{ margin:0px; padding:0px;}
        .kuang{width:600px; min-height:320px; margin:0 auto; border:1px solid #999;}
        .kuang_l{width:330px; height:auto; float:left; line-height:28px; color:#000; font-size:14px; padding:20px 20px 20px 50px;;}
        .kuang_l ul{margin:0; padding:0;}
        .kuang_l ul li{width:400px; height:auto; float:left; list-style:none;}

        .kuang_r{width:200px; height:auto; float:right; padding:20px 0;}

    </style>
  </head>
  
  <body>
    <%--<div>
    	 <table cellpadding="0" cellspacing="2" class="l-table-edit" align="center" style="padding-top: 10px">
            <tr>
                <td align="right" class="l-table-edit-td">帐号：</td>
                <td align="left" class="l-table-edit-td" style="width:160px">${user.userName }
					</td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">名字：</td>
                <td align="left" class="l-table-edit-td" style="width:160px">${user.name }</td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td" valign="top">性别：</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                	<c:if test="${user.sex=='1'}">
                		男
                	</c:if>
                	<c:if test="${user.sex=='2'}">
                		女
                	</c:if>
                </td><td align="left"></td>
            </tr>   
             <tr>
                <td align="right" class="l-table-edit-td">Email：</td>
                <td align="left" class="l-table-edit-td" style="width:160px">${user.email }</td>
                <td align="left"></td>
            </tr>
            
        
            <tr>
                <td align="right" class="l-table-edit-td">个人简介：</td>
                <td align="left" class="l-table-edit-td" colspan="2"> 
                	${user.descn}
                </td> <td align="left"></td>
            </tr>
             <tr>
                 <td align="right" class="l-table-edit-td">个人简介：</td>
                 <td align="left" class="l-table-edit-td" colspan="2">

                     <img src="${user.img}" alt="相片" width="344" height="481"/>
                 </td> <td align="left"></td>
             </tr>
        </table>
    </div>--%>
    <div class="kuang">
        <div class="kuang_l">
            <ul>
                <li>账号：${user.userName }</li>
                <li>名字：${user.name }</li>
                <li>姓别：<c:if test="${user.sex=='1'}">
                    男
                </c:if>
                    <c:if test="${user.sex=='2'}">
                        女
                    </c:if></li>
                <li>Email：00001</li>
                <li>个人简介：${user.descn}</li>
            </ul>
        </div>
        <div class="kuang_r"><img src="${user.img}" width="200" height="200"/></div>
    </div>
  </body>
</html>
