<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" /> 
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
      var eee;
        $(function () {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                //调试状态，不会提交数据的
                debug: true,
                errorPlacement: function (lable, element) {

                    if (element.hasClass("l-textarea")) {
                        element.addClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().addClass("l-text-invalid");
                    }

                    var nextCell = element.parents("td:first").next("td");
                    nextCell.find("div.l-exclamation").remove(); 
                    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
                },
                success: function (lable) {
                    var element = $("#" + lable.attr("for"));
                    var nextCell = element.parents("td:first").next("td");
                    if (element.hasClass("l-textarea")) {
                        element.removeClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().removeClass("l-text-invalid");
                    }
                    nextCell.find("div.l-exclamation").remove();
                },
                submitHandler: function (form) {
                    //form.submit();
                    //parent.closeWin();
                    $.ajax({
                    	type:"post",
                    	url:"<%=path %>/sysUser/save.do",
                    	data:$("#form1").serialize(),
                    	success : function(result) {//返回数据根据结果进行相应的处理  
                        if ( result.flag ) {  
                            $.ligerDialog.question(result.message,function() {
	                            parent.closeWin();
	                        });  
                        } else {  
                             
                        }  
                    }
                    	
                    });
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function () {
                alert(v.element($("#txtName")));
            });
        });  
		function test(){
			parent.test1()
		}
    </script>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style >
</head>
<body>
<form name="form1" method="post"  id="form1"  >
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
            <tr>
                <td align="right" class="l-table-edit-td">帐号:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="userName" type="text" id="txtUserName" ltype="text" validate="{required:true,minlength:3,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">密码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="password" type="password" id="password" ltype="text" validate="{required:true,minlength:1,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">名字:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="name" type="text" id="name" ltype="text" validate="{required:true,minlength:2,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td" valign="top">性别:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                    <input id="rbtnl_0" type="radio" name="sex" value="1" checked="checked" /><label for="rbtnl_0">男</label> <input id="rbtnl_1" type="radio" name="sex" value="2" /><label for="rbtnl_1">女</label>
                </td><td align="left"></td>
            </tr>   
             <tr>
                <td align="right" class="l-table-edit-td">Email:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="email" type="text" id="email" ltype="text" validate="{required:true,email:true}" /></td>
                <td align="left"></td>
            </tr>
            
        
            <tr>
                <td align="right" class="l-table-edit-td">个人简介:</td>
                <td align="left" class="l-table-edit-td" colspan="2"> 
                <textarea cols="100" rows="4" class="l-textarea" name="descn" id="descn" style="width:400px" validate="{required:true}" ></textarea>
                </td> <td align="left"></td>
            </tr>
        </table>
 <br />
<input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /> 
<input type="reset" value="重置" class="l-button"/>
    </form>
    <div style="display:none">
    <!--  数据统计代码 --></div>
</body>
</html>