<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="syscode" uri="/syscode" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%@ include file="/common/ligerUIJS.jsp"%>
		<link href="<%=path %>/js/dwz/themes/azure/style.css" rel="stylesheet" type="text/css" media="screen">
		<link href="<%=path %>/js/dwz/themes/css/core.css" rel="stylesheet" type="text/css" media="screen">
		<link href="<%=path %>/js/dwz/themes/css/print.css" rel="stylesheet" type="text/css" media="print">
		
		
		
    
       <script type="text/javascript">
       var grid = null;
       var win=null;
        $(function ()
        {
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '帐号', name: 'userName', align: 'cneter', width: 150,
                    render: function (item) {
                        return "<a href='javascript:show("+item.id+")'>"+item.userName+"</a>";
                    } } ,
                  { display: '姓名', name: 'name', width: 150, align: 'cneter' },
                  { display: '性别', name: 'sex', width: 150 ,
                  	render: function (item)
                     {
                         if (item.sex == '1') return '男';
                         return '女';
                     }
                  	
                  },
                  { display: '电子邮箱', name: 'email', width: 150, align: 'cneter' },
                  { display: '描述', name: 'descn', width: 150, align: 'cneter' },
                  { display: '是否启用', name: 'status', width: 150 ,
                    	render: function (item)
                       {
                           if (item.status == '1') return '启用';
                           return '禁用';
                       }
                    	
                    },
                   { display: '注册时间', name: 'createDate', width: 150, align: 'cneter', type:'date',format: 'yyyy年MM月dd',render: function (item)
                     {
                         return new Date(item.createDate).toLocaleDateString();
                     }} 
                  
                ], 
                url:"<%=path %>/sysUser/list.do",
                rownumbers:true,
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 10, 
                pageSizeOptions :[10, 20, 30, 50],
                width: '99.8%', 
                height: '99%'
            }); 
        });
        
        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'userName', value: $("#userName").val()},
                { name: 'name', value: $("#name").val()},
                { name: 'phone', value: $("#phone").val()},
                { name: 'sex', value: $("#sex").val()},
                { name: 'beginTime', value: $("#beginTime").val()},
                { name: 'endTime', value: $("#endTime").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
         function itemclick(item)//测试
        {
           var del=grid.getSelected();
            alert(item.userName);
        }
       
        //新增
	    function create(){
	        win= $.ligerDialog.open({ 
	         title:'新增',
	         url: '<%=path %>/sysUser/query/add.do', 
	         height: 450, 
	         width: 700, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
	    }
	    //修改用户
    	function editUser(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要修改的数据！');
    			return;
    		}
    		win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/sysUser/query/edit.do?id='+del.id, 
	         height: 400, 
	         width: 500, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
    	}
    	//删除用户
    	function delUser(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要删除的数据！');
    			return;
    		}
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysUser/userDel.do',//这里是接收数据的PHP程序
	                data:{id:del.id,username:del.userName,name:del.name},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            f_search();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    	
    	function show(id){
    		 win= $.ligerDialog.open({ 
	         title:'详细',
	         url: '<%=path %>/sysUser/query/show.do?id='+id, 
	         height: 600,
	         width: 1000,
             isResize: true
            }); 
    	}
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}
    	
    	//用户赋予角色
    	function userAuthority(){
	    	var at=grid.getSelected();
	    		if(at==null){
	    			$.ligerDialog.question('请选中用户！');
	    			return;
	    		}
				win= $.ligerDialog.open({ 
		         title:'用户 角色',
		         url: '<%=path %>/sysUser/query/userAuthority.do?userName='+at.userName, 
		         height: 440, 
		         width: 300, 
	             isResize: true
	            });
    	}
    	//调出用户信息
    	function exportExcle(){
    		$.ajax({
    			type:"post",
    			url:"<%=path %>/sysUser/exportUser.do",
    			data:{userName:$("#userName").val()},
    			success : function(result) {//返回数据根据结果进行相应的处理  
                        
                    } 
    		});
    	}
    	//重置用户密码
    	function restPassword(){
    		var re=grid.getSelected();
    		if(re==null){
    			$.ligerDialog.question('请选中用户！');
    			return;
    		}
    		if(confirm("请确认是否重置用户 "+re.name+" 密码")){
    			$.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysUser/restPassword.do',//这里是接收数据的PHP程序
	                data:{id:re.id,userName:re.userName},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message);
	                },
	                error:function(){
	                    $.ligerDialog.question('密码重置失败！');
	                }
	            }) ;
    		}
    	}
    	function exportOrder() {
    		
    		var url="<%=path %>/exports/exportUser.do";
			//window.open(url);
			window.location.href=url;
    		<%-- if(b!=null && e!=null && b.length>0 && e.length>0){
    	
    			var url="<%=path %>/exports/exportOrder.do?beginTime="+b
				+"&endTime="+e+"&intrphone="+$("#intrphone").val()+"&no="+$("#no").val()+"&paytype="+$("#paytype").val();
    			
    		}else{
    			$.ligerDialog.question("导出时请选择报名日期",function() {
                    
                });
    		} --%>
    		
		}
    	
    
	function upImg(){
		var up=grid.getSelected();
		if(up==null){
			$.ligerDialog.question('请选中一条数据！');
			return;
		}
		win= $.ligerDialog.open({ 
         title:'头像上传',
         url: '<%=path %>/sysUser/query/toUpUserImg.do?userIDS='+up.id, 
         height: 350, 
         width: 300, 
   
         isResize: true
        });
	}

	function  exportTemplate() {
        window.location.href='<%=path %>/upOrDown/download.do?fileName=教练绩效模版.xls';
    }
    </script>
  <style type="text/css">
	.btn16
	{
		width:100px;
		height:25px;
		font-family: "Arial","Tahoma","微软雅黑","雅黑";
		color:#000;
		/* border-radius:6px; -moz-border-radius:6px; -webkit-border-radius:6px;  */
	}
	.l-text-wrapper {
	    position: relative;
	    width: 130px;
	}
  </style>
</head>
<body >
<div class="pageHeader">
	<div class="searchBar">
		
		<table class="searchContent">
			<tr>
				<td>
					帐号：<input type="text" name="userName"  id="userName" />
				</td>
				<td>
					姓名：<input type="text" name="name"  id="name" />
				</td>
				<td>
					电话：<input type="text" name="phone"  id="phone" />
				</td>
				<td>
					性别：
					<%--<select  name="sex" id="sex">
						<option value="">-选择性别-</option>
						<option value="1">男</option>
						<option value="2">女</option>
						
					</select>--%>
					<syscode:dictionary id="sex" name="sex" extendProperty="checkFun='checkNotNull'" defaultValue="1" codeType="sys_sex" type="select" used="1,0">
					</syscode:dictionary>
				</td>
				<td>
					入职日期：
					<input id="beginTime"  class="Wdate" name="beginTime" type="text" onClick="WdatePicker({startDate:'%y/%M/%d',dateFmt:'yyyy/MM/dd'})" />
		  ~<input id="endTime"  class="Wdate" name="endTime" type="text" onClick="WdatePicker({startDate:'%y/%M/%d',dateFmt:'yyyy/MM/dd'})" /> 
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit"  onclick="f_search()">查&nbsp;&nbsp;询</button></div></div></li>
			</ul>
		</div>
	</div>
	<!-- </form> -->

</div>
<div class="panelBar">
		<ul class="toolBar">
			<shiro:hasPermission name="user:add"><li><a class="add" href="javascript:create();" ><span>添加</span></a></li></shiro:hasPermission>
			<shiro:hasPermission name="user:update"><li><a class="edit" href="javascript:editUser();" ><span>修改</span></a></li></shiro:hasPermission>
			<shiro:hasPermission name="user:del"><li><a class="delete" href="javascript:delUser();"  ><span>删除</span></a></li></shiro:hasPermission>

			<li><a class="delete" href="javascript:userAuthority();"  ><span>权限</span></a></li>
			<li><a class="delete" href="javascript:restPassword();"  ><span>重置密码</span></a></li>
			<li><a class="add" href="javascript:upImg();"  ><span>上传头像</span></a></li>
			
			
			<li><a class="icon" href="javascript:exportOrder();"  title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>

			<li><a class="icon" href="javascript:exportTemplate();"  title="实要导出这些记录吗?"><span>导出模版测试</span></a></li>
		</ul>
	</div>
<div id="maingrid" style="margin: 0; padding: 0;scrolling:no"></div>
</body>
</html>