<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文件上传</title>
<link rel="stylesheet" href="<%=path %>/js/uploadify/uploadify.css" type="text/css"></link> 
<script src="<%=path %>/js/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="<%=path %>/js/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>

 <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
  <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
  <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerWindow.js" type="text/javascript"></script>
<script type="text/javascript">
	var count = 1;
	
    $(function() {
    	var pathArray="";
        $('#uploadify').uploadify({
        	'langFile':"<%=path %>/js/uploadify/errorMessage.js",
            'swf'      : '<%=path %>/js/uploadify/uploadify.swf',//swf的路径
            'uploader' : '<%=path %>/upOrDown/uploadify.do?folders=users',//服务器端脚本文件路径
            'height': 25,
            'whith' :120,
            'auto'  : false,//表示在选择文件后是否自动上传
            'multi' : true,//是否支持多文件上传
            'method':'post',
            //'debug':true,
            'fileObjName':'file',
            'buttonText' : '选择图片...',
            'fileTypeDesc' : 'jpg、png、gif、bmp',//允许上传的文件类型的描述，在弹出的文件选择框里会显示  
            'fileTypeExts' : '*.jpg;*.png;*.gif;*.bmp;*.doc;*.txt',//指定文件格式 
            'fileSizeLimit' : '50MB',//上传文件大小限制，默认单位是KB，若需要限制大小在100KB以内，可设置该属性为：'100KB'  
            'progressData' : 'all', // 'percentage''speed''all'//队列中显示文件上传进度的方式：all-上传速度+百分比，percentage-百分比，speed-上传速度  
            'preventCaching' : true,//若设置为true，一个随机数将被加载swf文件URL的后面，防止浏览器缓存。默认值为true 
            'uploadLimit':2,//最多允许上传文件
            'queueSizeLimit':2,//最多允许上传文件
            'simUploadLimit' :3, //并发上传数据
            'timeoutuploadLimit' : 2,//能同时上传的文件数目  
            'removeCompleted' : true,//默认是True，即上传完成后就看不到上传文件进度条了。  
            'removeTimeout' : 3,//上传完成后多久删除队列中的进度条，默认为3，即3秒。  
            'requeueErrors' : true,//若设置为True，那么在上传过程中因为出错导致上传失败的文件将被重新加入队列。 
            'overrideEvents' : ['onDialogClose','onSelectError' ,'onUploadError'],
            'onSelect' : uploadify_onSelect, 
            'onSelectError' : uploadify_onSelectError,  
            'onUploadError' : uploadify_onUploadError,
            'onUploadSuccess' : function(file, data, response) {//单个文件上传成功触发 
            	//将complete改为中文的'上传完毕'
            	 
            	 pathArray=data;
            	 //alert( pathArray);
                },
            'onUploadError' : function(file, errorCode, errorMsg, errorString) {
                alert('The file ' + file.name + ' could not be uploaded:1 ' + errorString);
               },
            'onQueueComplete' : function(){//所有文件上传完成    
                  // alert("文件上传成功!"+pathArray);  
                   //alert(pathArray);  
                   if(pathArray.length>0){
                	   $.ajax({
                		   type:'post',
                		   url:'<%=path %>/sysUser/upCImg.do',
                		   data:{id:$("#userIDS").val(),img:pathArray},
                		   dataType:'json',
                		   success:function(data){
              	            	if ( data.flag ) {  
              	            		alert(data.message);
              	            		 parent.closeWin();
                                   
                                } else {  
                                	alert(data.message);   
                                }           
               	            }   ,
               	            error: function(){
               	                //请求出错处理
               	            } 
                	   });
                   }else{
                	   alert("文件上传失败");
                   }
               }  

        });
    });
    
    var uploadify_onSelectError = function(file, errorCode, errorMsg) {  
        var msgText = "上传失败\n";  
        alert(errorCode);
        switch (errorCode) {  
            case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:  
                //this.queueData.errorMsg = "每次最多上传 " + this.settings.queueSizeLimit + "个文件";  
                msgText += "每次最多上传 " + this.settings.queueSizeLimit + "个文件";  
                break;  
            case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:  
                msgText += "文件大小超过限制( " + this.settings.fileSizeLimit + " )";  
                break;  
            case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:  
                msgText += "文件大小为0";  
                break;  
            case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:  
                msgText += "文件格式不正确，仅限 " + this.settings.fileTypeExts;  
                break;  
            default:  
                msgText += "错误代码：" + errorCode + "\n" + errorMsg;  
        }  
        alert(msgText);  
    }; 
    
    var uploadify_onUploadError = function(file, errorCode, errorMsg, errorString) {
        // 手工取消不弹出提示
        if (errorCode == SWFUpload.UPLOAD_ERROR.FILE_CANCELLED
                || errorCode == SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED) {
            return;
        }
        var msgText = "上传失败\n";
        switch (errorCode) {
            case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
                msgText += "HTTP 错误\n" + errorMsg;
                break;
            case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
                msgText += "上传文件丢失，请重新上传";
                break;
            case SWFUpload.UPLOAD_ERROR.IO_ERROR:
                msgText += "IO错误";
                break;
            case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
                msgText += "安全性错误\n" + errorMsg;
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
                msgText += "每次最多上传 " + this.settings.uploadLimit + "个";
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
                msgText += errorMsg;
                break;
            case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
                msgText += "找不到指定文件，请重新操作";
                break;
            case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
                msgText += "参数错误";
                break;
            default:
                msgText += "文件:" + file.name + "\n错误码:" + errorCode + "\n"
                        + errorMsg + "\n" + errorString;
        }
        alert(msgText);
    }
    var uploadify_onSelect = function(){  
    	   
    };
</script>
<style>
a:link {
 text-decoration: none;
}
.btn16
	{
		width:100px;
		height:25px;
		font-family: "Arial","Tahoma","微软雅黑","雅黑";
		color:#000;
		/* border-radius:6px; -moz-border-radius:6px; -webkit-border-radius:6px;  */
	}

</style>
</head>
<body>
	<div style="margin: auto;">
	  <input id="uploadify" type="file" name="uploadify"/><br>
	  <input  type="hidden" name="folders" value="users"/><br>
	  <input  type="hidden" name="userIDS" id="userIDS" value="${userIDS}"/><br>
       <div id="preview" align="center"></div>    
	  <a href="javascript:$('#uploadify').uploadify('upload', '*')" >上传文件</a>&nbsp;&nbsp; 
      <img style="display: none" id="upload_org_code_img" src="" width="150" height="150">
     </div>
</body>
</html>