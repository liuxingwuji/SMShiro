<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>密码修改</title>
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" /> 
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        var eee;
        $(function ()
        {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                debug: true,
                errorPlacement: function (lable, element)
                {
                    if (element.hasClass("l-textarea"))
                    {
                        element.ligerTip({ content: lable.html(), target: element[0] }); 
                    }
                    else if (element.hasClass("l-text-field"))
                    {
                        element.parent().ligerTip({ content: lable.html(), target: element[0] });
                    }
                    else
                    {
                        lable.appendTo(element.parents("td:first").next("td"));
                    }
                },
                success: function (lable)
                {
                    lable.ligerHideTip();
                    lable.remove();
                },
                submitHandler: function ()
                {
                	var passwd = document.getElementById("password").value;
                	var repasswd = document.getElementById("surePassword").value;
                	if(passwd == repasswd){
                		 $("form .l-text,.l-textarea").ligerHideTip();
                         $.ajax({
                         	type:"post",
                         	url:"<%=path %>/sysUser/updatePass.do",
                         	data:$("#form1").serialize(),
                         	success : function(result) {//返回数据根据结果进行相应的处理  
                             if ( result.flag ) {  
                                 $.ligerDialog.question(result.message,function() {
     	                            parent.autoQuit();
     	                        });  
                             } else {  
                                  
                             }  
                         }
                         	
                         });
                	}else{
                	window.alert("两次输入密码不一致,请重新输入");
                	return false;
                	}
                   
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function ()
            {
                alert(v.element($("#txtName")));
            });
        });  
        
       
    </script>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style> 
</head>
<body>
<form name="form1" method="post"  id="form1"  >
		<input type="hidden" name="username" value="${username}">
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
            <tr>
                <td align="right" class="l-table-edit-td">帐号:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                ${username}
                </td>
                <td align="left"></td>
            </tr>
           <!--   <tr>
                <td align="right" class="l-table-edit-td">旧密码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="oldPassword" type="password" id="oldPassword" ltype="text" validate="{required:true,minlength:1,maxlength:10}" /></td>
                <td align="left"></td>
            </tr> -->
            <tr>
                <td align="right" class="l-table-edit-td">新密码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="password" type="password" id="password" ltype="text" validate="{required:true,minlength:6,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
             <tr>
                <td align="right" class="l-table-edit-td">确认密码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="surePassword" type="password" id="surePassword" ltype="text" validate="{required:true,minlength:6,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
        </table>
 <br />
<input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /> 
<input type="reset" value="重置" class="l-button"/>
    </form>
    <div style="display:none">
    <!--  数据统计代码 --></div>
</body>
</html>