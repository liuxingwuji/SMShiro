<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<meta http-equiv="description" content="This is my page">
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
   <%-- <link href="<%=path %>/js/ligerUI/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> --%>

	  <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style >
  </head>
  
  <body>
    <div>
    	<table cellpadding="0" cellspacing="0" class="l-table-edit" align="center">
        <tr>
                <td align="right" class="l-table-edit-td" >菜单编码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                	${menu.menuId }
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">菜单名称:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                		${menu.menuName }
                	</td>
                <td align="left"></td>
            </tr>
             <tr>
                <td align="right" class="l-table-edit-td">url:</td>
                <td align="left" class="l-table-edit-td" style="width:200px">
                		${menu.url }
                	</td>
                <td align="left"></td>
            </tr>
          <!--   <tr>
                <td align="right" class="l-table-edit-td">父级菜单:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                	<input name="parentId" type="hidden" id="parentId" ltype="textParentId"   />
               		<input name="menuName" type="text" id="menuName" ltype="textMenuName" onclick="showMenu()" readonly value="" style="width:120px;" validate="{required:true}" />
                </td>
                <td align="left"></td>
            </tr> -->
        
        </table>
    </div>
  </body>
</html>
