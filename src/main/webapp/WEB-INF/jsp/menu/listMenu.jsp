 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 
    <%@ include file="/common/ligerUIJS.jsp"%>
       <script type="text/javascript">
       var grid = null;
       var win=null;
        $(function ()
        {
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '菜单名称', name: 'menuName', align: 'cneter', width: 150 ,
                    render: function (item) {
                        return "<a href='javascript:show("+item.id+")'>"+item.menuName+"</a>";
                    } } ,
                  { display: '菜单编码', name: 'menuId', width: 150, align: 'left' },
                  { display: 'url', name: 'url', width: 200, align: 'left' },
                  { display: '父级菜单', name: 'parentId', width: 150, align: 'cneter' }
                  
                ], 
                url:"<%=path %>/menu/list.do",
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 5, 
                pageSizeOptions :[5,10, 20, 30, 40],
                width: '99.8%', 
                height: '99%',
                //dateFormat:'yyyy-MM-dd', 
                toolbar:{items:[
                	{text:'修改',click:edit,icon:'modify'},
                	{ line: true },
                	{text:'删除',click:delUser,img:'<%=path %>/js/ligerUI/ligerUI/skins/icons/delete.gif'}
                ]}
            }); 
        });
        
        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'menuName', value: $("#menuName").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
         function itemclick(item)//测试
        {
           var del=grid.getSelected();
            alert(item.userName);
        }
       
        //新增
	    function create(){
	        win= $.ligerDialog.open({ 
	         title:'新增',
	         url: '<%=path %>/menu/query/add.do', 
	         height: 400, 
	         width: 500, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
	    }
	    //修改用户
    	function edit(){
    		var del=grid.getSelected();
    		   win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/menu/query/edit.do?id='+del.id, 
	         height: 440, 
	         width: 500, 
             isResize: true
            });
    	}
    	//根据主键ID删除用户
    	function delUser(){
    		var del=grid.getSelected();
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/menu/deleteByID.do',//这里是接收数据的PHP程序
	                data:{id:del.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            f_search();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    	
    	function show(val){
    		win= $.ligerDialog.open({ 
	         title:'详细',
	         url: '<%=path %>/menu/query/show.do?id='+val, 
	         height: 400, 
	         width: 500, 
             isResize: true
            });
    	}
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}
    </script>
</head>
<body >
 <div id="searchbar" style="background-color: #C6E2FF;height: 30px;width: 100%">
    菜单：<input id="menuName" type="text" />
    <input id="btnOK" type="button" value="查询" onclick="f_search()" />
</div> 
<div id="maingrid" style="margin: 0; padding: 0;scrolling="no""></div> 
</body>
</html>