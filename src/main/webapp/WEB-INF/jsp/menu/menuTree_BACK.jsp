<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%@ include file="/common/ligerUIJS.jsp"%>
   <%-- <link rel="stylesheet" href="<%=path %>/js/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=path %>/js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.exedit-3.5.js"></script>--%>
	<link rel="stylesheet" href="<%=path %>/js/plugins/ztreeV3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=path %>/js/plugins/ztreeV3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.exedit.js"></script>
    <style type="text/css">
    	body{  font-size:20px;font-family:"微软雅黑","方正兰亭黑简体";}
    </style>
    <SCRIPT type="text/javascript">
		
		var setting = {
			view: {
				addHoverDom: addHoverDom,
				removeHoverDom: removeHoverDom,
				selectedMulti: false
			},
			edit: {
				enable: true,
				editNameSelectAll: true,
				showRemoveBtn: showRemoveBtn,
				showRenameBtn: showRenameBtn
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeEditName: beforeEditName,
				beforeRemove: beforeRemove,
				beforeRename: beforeRename,
				onRemove: onRemove,
				onRename: onRename
			}
		};
		
		var treeNodes;  
  		function initialize(){
  			$.ajax({  
		        async : false,  
		        cache:false,  
		        type: 'POST',  
		        //dataType : "json",  
		        url: "<%=path %>/menu/menuTree.do",//请求的action路径  
		        error: function () {//请求失败处理函数  
		            alert('请求失败');  
		        },  
		        success:function(data){ //请求成功后处理函数。
					alert(data);
		            treeNodes = data;   //把后台封装好的简单Json格式赋给treeNodes  
		        }  
		    });  
		  
		    //zTree = $("#treeDemo").zTree(setting, treeNodes); 
		    $.fn.zTree.init($("#treeDemo"), setting, treeNodes); 
  		}
		$(function(){  
		    initialize();
		});  

		

		var log, className = "dark";
		function beforeDrag(treeId, treeNodes) {
			return false;
		}
		//进入修改页面
		function beforeEditName(treeId, treeNode) {
			className = (className === "dark" ? "":"dark");
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.selectNode(treeNode);
			win=$.ligerDialog.open({ 
		         title:'修改',
		         url: '<%=path %>/menu/query/edit.do?menuID='+treeNode.id, 
		         height: 400, 
		         width: 500, 
	             isResize: true
	            });
			
			//return confirm("进入节点 -- " + treeNode.name +"----"+treeNode.id+ " 的编辑状态吗？");
		}
		
		//删除数据 begin--
		function beforeRemove(treeId, treeNode) {
			className = (className === "dark" ? "":"dark");
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.selectNode(treeNode);
			return confirm("请确认是否删除 -- " + treeNode.name + " ！");
		}
		//删除菜单节点
		function onRemove(e, treeId, treeNode) {//根据节点编码删除数据
			//showLog("[ "+getTime()+" onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/menu/deleteByMID.do',//这里是接收数据的PHP程序
	                data:{mId:treeNode.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            initialize();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
		}
		//修改后保存
		function beforeRename(treeId, treeNode, newName, isCancel) {
			className = (className === "dark" ? "":"dark");
			showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" beforeRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
			if (newName.length == 0) {
				alert("节点名称不能为空.");
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				setTimeout(function(){zTree.editName(treeNode)}, 10);
				return false;
			}
			return true;
		}
		function onRename(e, treeId, treeNode, isCancel) {
			showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
		}
		function showRemoveBtn(treeId, treeNode) {
			//return !treeNode.isFirstNode;
			return true;
		}
		function showRenameBtn(treeId, treeNode) {
			//return !treeNode.isLastNode;
			return true;
		}
		function showLog(str) {
			if (!log) log = $("#log");
			log.append("<li class='"+className+"'>"+str+"</li>");
			if(log.children("li").length > 8) {
				log.get(0).removeChild(log.children("li")[0]);
			}
		}
		function getTime() {
			var now= new Date(),
			h=now.getHours(),
			m=now.getMinutes(),
			s=now.getSeconds(),
			ms=now.getMilliseconds();
			return (h+":"+m+":"+s+ " " +ms);
		}

		var newCount = 1;
		//新增菜单
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add node' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var btn = $("#addBtn_"+treeNode.tId);
			/* if (btn) btn.bind("click", function(){
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node1" + (newCount++)});
				return false;
			}); */
			 btn.bind("click", function(){
				 win=$.ligerDialog.open({ 
		         title:'新增',
		         url: '<%=path %>/menu/query/add.do?parentId='+treeNode.id, 
		         height: 400, 
		         width: 500, 
	             isResize: true
	            });
			});
		};
		
		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
		};
		
		
		var win=null;
		//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		initialize();
    	}
		
	</SCRIPT>
 <style type="text/css">
    	body {font-size:25px;}
    	.ztree * {padding:0; margin:0; font-size:18px; font-family: Verdana, Arial, Helvetica, AppleGothic, sans-serif}
    	.ztree li span {line-height:18px; margin-right:2px}
		.ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
	</style>
</head>
<body>

	<div style="margin: auto;width:750px;height:auto;">
		<div class="zTreeDemoBackground left">
			<ul id="treeDemo" class="ztree" style="background-color: white;border: 0;overflow-y: auto;width:600px;height:auto;"></ul>
		</div>
		<div class="right"></div>
	</div>

</body>
</html>