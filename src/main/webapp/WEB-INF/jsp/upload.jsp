<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>servlet上传</title>
<style>
* {
	font-family: "宋体";
	font-size: 14px
}
</style>
<script type="text/javascript">
/** 
* 从 file 域获取 本地图片 url 
*/ 
function getFileUrl(sourceId) { 
var url; 
if (navigator.userAgent.indexOf("MSIE")>=1) { // IE 
url = document.getElementById(sourceId).value; 
} else if(navigator.userAgent.indexOf("Firefox")>0) { // Firefox 
url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0)); 
} else if(navigator.userAgent.indexOf("Chrome")>0) { // Chrome 
url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0)); 
} 
return url; 
} 

/** 
* 将本地图片 显示到浏览器上 
*/ 
function preImg(sourceId, targetId) { 
var url = getFileUrl(sourceId); 
var imgPre = document.getElementById(targetId); 
imgPre.src = url; 
} 

	
	/**
	* 显示大图片
	*/
	function showDetails(ev){
		 var target = ev.srcElement || ev.target;
		  var details = document.getElementById("details");
		  details.style.display='';
		 
		  if (navigator.userAgent.indexOf("MSIE")>=1) { // IE 
			  details.style.left = window.event.clientX;
			  details.style.top = window.event.clientY;
			} else if(navigator.userAgent.indexOf("Firefox")>0) { // Firefox 
				 details.style.left = target.pageX;
				  details.style.top = target.pageY;
			} else if(navigator.userAgent.indexOf("Chrome")>0) { // Chrome 
				 details.style.left = window.event.clientX;
				  details.style.top = window.event.clientY;
			} 
		  
		 }


	var count = 0;
	function additem(id) {
		var row, cell, str;
		row = document.getElementById(id).insertRow();
		if (row != null) {
			cell = row.insertCell();
			cell.innerHTML = "<input id=\"St"+count+"\" type=\"text\" name=\"St"+count+"\" value= \"St"+count+"\"><input type=\"button\" value=\"删除\" onclick=\'deleteitem(this);\'>";
			count++;
		}
	}
	function deleteitem(obj) {
		var curRow = obj.parentNode.parentNode;
		tb.deleteRow(curRow.rowIndex);
	}

	function getsub() {
		var re = "";
		for (var i = 0; i < count; i++) {
			re += document.getElementsByName("St" + i)[0].value;

		}
		document.getElementById("Hidden1").value = re;
	}
</script>
</head>
<body>
	<p align="center">请您选择需要上传的文件</p>
	<form id="form1" name="form1" method="post"
		action="servlet/fileServlet" enctype="multipart/form-data">
		<table border="0" align="center">
			<tr>
			    <td></td>
			    <td>
					<img id="imgPre" src="" width="300px" height="300px"  /> 

			      </td>
			</tr>
			<tr>
				<td>上传文件：</td>
				<td><input name="file" type="file" size="20"
					id="imgOne" onchange="preImg(this.id,'imgPre');" ></td>
				
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" value="提交"> <input
					type="reset" name="reset" value="重置"></td>
			</tr>
		</table>
	</form>


</body>
</html>