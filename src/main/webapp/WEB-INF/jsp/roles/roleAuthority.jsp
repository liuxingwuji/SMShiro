<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" /> 
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <link rel="stylesheet" href="<%=path %>/js/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=path %>/js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.exhide-3.5.js"></script>
    
    <script type="text/javascript">
      var eee;
        $(function () {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                //调试状态，不会提交数据的
                debug: true,
                errorPlacement: function (lable, element) {

                    if (element.hasClass("l-textarea")) {
                        element.addClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().addClass("l-text-invalid");
                    }

                    var nextCell = element.parents("td:first").next("td");
                    nextCell.find("div.l-exclamation").remove(); 
                    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
                },
                success: function (lable) {
                    var element = $("#" + lable.attr("for"));
                    var nextCell = element.parents("td:first").next("td");
                    if (element.hasClass("l-textarea")) {
                        element.removeClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().removeClass("l-text-invalid");
                    }
                    nextCell.find("div.l-exclamation").remove();
                },
                submitHandler: function (form) {
                    //form.submit();
                    //parent.closeWin();
                    $.ajax({
                    	type:"post",
                    	url:"<%=path %>/mroles/saveAuthority.do",
                    	data:$("#form1").serialize(),
                    	success : function(result) {//返回数据根据结果进行相应的处理  
                        if ( result.flag ) {  
                            $.ligerDialog.question(result.message,function() {
	                            parent.closeWin();
	                        });  
                        } else {  
                             
                        }  
                    }
                    	
                    });
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function () {
                alert(v.element($("#txtName")));
            });
        });  
		
    </script>
    
    <SCRIPT type="text/javascript">
		
		var setting = {
			check: {
				chkboxType :{"Y" : "p", "N" : "ps"},
				enable: true
			},
			view: {
				dblClickExpand: false,
				expandSpeed: "slow"
			},
			data: {
				simpleData: {
				open:true,
					enable: true
				}
			},
			callback: {
				beforeClick: beforeClick,
				onCheck: onCheck
			}
		};
		
		var treeNodes;  
  
		$(function(){  
		    $.ajax({  
		        async : false,  
		        cache:false,  
		        type: 'POST',  
		        //dataType : "json",  
		        url: "<%=path %>/menu/menuTree.do",//请求的action路径  
		        error: function () {//请求失败处理函数  
		            alert('请求失败');  
		        },  
		        success:function(data){ //请求成功后处理函数。    
		            treeNodes = data;   //把后台封装好的简单Json格式赋给treeNodes  
		        }  
		    });  
		  
		    //zTree = $("#treeDemo").zTree(setting, treeNodes); 
		    $.fn.zTree.init($("#treeDemo"), setting, treeNodes); 
		    AssignCheck();
		});  

		

		function beforeClick(treeId, treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.checkNode(treeNode, !treeNode.checked, null, true);
			return false;
		}
		
		function onCheck(e, treeId, treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getCheckedNodes(true),
			v = "";
			c = "";
			for (var i=0, l=nodes.length; i<l; i++) {
				v += nodes[i].name + ",";
				c += nodes[i].id + ",";
			}
			if (v.length > 0 ) v = v.substring(0, v.length-1);
			if (c.length > 0 ) c = c.substring(0, c.length-1);
			var cityObj = $("#menuName");
			var authority = $("#authority");
			cityObj.attr("value", v);
			authority.attr("value", c);
		}
			

		function showMenu() {
			var cityObj = $("#menuName");
			var cityOffset = $("#menuName").offset();
			$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

			$("body").bind("mousedown", onBodyDown);
		}
		function hideMenu() {
			$("#menuContent").fadeOut("fast");
			$("body").unbind("mousedown", onBodyDown);
		}
		function onBodyDown(event) {
			if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
				hideMenu();
			}
		}

		
		//选中指定的节点
    function AssignCheck() {
    	var getAuthority=$("#authority").val();
    	var array=getAuthority.split(',');
        var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
        var i;
		for(i=0;i<array.length;i++){
			treeObj.checkNode(treeObj.getNodeByParam("id", array[i], null), true, true);
		}
    }
	</SCRIPT>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:right; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
        div {  
	        line-height:25px;  
	        overflow:hidden;  
 		}   
    </style> 
</head>
<body>
<div id="menuContent" class="menuContent"  style="position: absolute;overflow:hidden;" >
	<form name="form1" method="post"  id="form1"  >
		<input name="roleId" type="hidden" id="roleId" ltype="text" value="${role.code }" readonly="readonly" />
		<input name="authority" type="hidden" id="authority" ltype="textParentId"   value="${RA }"/>
		<input name="menuNames" type="hidden" id="menuName" ltype="textMenuName" onclick="showMenu()" readonly   />
		
		<!-- <ul id="treeDemo" class="ztree" style="margin-top:0; width:210px;background-color: white;border: 0;overflow-y:auto;"></ul> -->
		<ul id="treeDemo" class="ztree" style="margin-top:0; width:210px; height: 300px;background-color: white;border: 0;"></ul>
		 <br/>
		<input type="submit" value="保 存" id="Button1" class="l-button l-button-submit" /> 
	 </form>
 </div>  
   
</body>
</html>