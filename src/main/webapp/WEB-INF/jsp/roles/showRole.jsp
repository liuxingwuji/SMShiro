<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<meta http-equiv="description" content="This is my page">
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
   <%-- <link href="<%=path %>/js/ligerUI/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> --%>

	  <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style> 
  </head>
  
  <body>
    <div>
    	<table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <tr>
                <td align="right" class="l-table-edit-td" >角色编码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                	${role.code }
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">角色名称:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                		${role.name }
                	</td>
                <td align="left"></td>
            </tr>
             
        
            <tr>
                <td align="right" class="l-table-edit-td">描述:</td>
                <td align="left" class="l-table-edit-td" colspan="2"> 
                <textarea cols="80" rows="4" class="l-textarea" name="describes" id="describesId" style="width:400px" validate="{required:true}" readonly="readonly" >${role.describes }</textarea>
                </td> <td align="left"></td>
            </tr>
        </table>
    </div>
  </body>
</html>
