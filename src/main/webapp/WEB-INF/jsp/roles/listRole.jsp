 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 
    <%@ include file="/common/ligerUIJS.jsp"%>
       <script type="text/javascript">
       var grid = null;
       var win=null;
        $(function ()
        {
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '角色名称', name: 'name', align: 'cneter', width: 150 } ,
                  { display: '角色编码', name: 'code', width: 150, align: 'cneter' },
                  { display: '描述', name: 'remark', width: 150, align: 'cneter' }
                ], 
                url:"<%=path %>/mroles/list.do",
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 5, 
                pageSizeOptions :[5,10, 20, 30, 40],
                width: '99.8%', 
                height: '99%',
                //dateFormat:'yyyy-MM-dd', 
                toolbar:{items:[
                	{text:'增加',click:create,icon:'add'},
                	{ line: true },
                	{text:'修改',click:edit,icon:'modify'},
                	{ line: true },
                	{text:'删除',click:delRole,img:'<%=path %>/js/ligerUI/ligerUI/skins/icons/delete.gif'},
                	{ line: true },
                	{text:'角色权限',click:authority,img:'<%=path %>/js/ligerUI/ligerUI/skins/Aqua/images/common/exclamation.gif'}
                	
                ]}
            }); 
        });
        
        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'menuName', value: $("#name").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
         function itemclick(item)//测试
        {
           var del=grid.getSelected();
            alert(item.userName);
        }
       
        //新增
	    function create(){
	        win= $.ligerDialog.open({ 
	         title:'新增',
	         url: '<%=path %>/mroles/query/add.do', 
	         height: 400, 
	         width: 500, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
	    }
	    //修改用户
    	function edit(){
    		var del=grid.getSelected();
    		   win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/mroles/query/edit.do?id='+del.id, 
	         height: 440, 
	         width: 500, 
             isResize: true
            });
    	}
    	//删除角色信息
    	function delRole(){
    		var del=grid.getSelected();
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/mroles/deletes.do',//这里是接收数据的PHP程序
	                data:{id:del.id,code:del.code},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            f_search();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    	
    	function show(){
    	var del=grid.getSelected();
    		win= $.ligerDialog.open({ 
	         title:'详细',
	         url: '<%=path %>/mroles/query/show.do?id='+del.id, 
	         height: 400, 
	         width: 500, 
             isResize: true
            });
    	}
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}
    	//角色赋权限
    	function authority(){
    		var at=grid.getSelected();
    		if(at==null){
    			$.ligerDialog.question('请选中角色！');
    			return;
    		}
			win= $.ligerDialog.open({ 
	         title:at.name+'--角色权限',
	         url: '<%=path %>/mroles/query/authority.do?id='+at.id, 
	         height: 450, 
	         width: 300, 
             isResize: true
            });
    	}
    	
    </script>
</head>
<body >
 <div id="searchbar" style="background-color: #C6E2FF;height: 30px;width: 100%">
    角色名称：<input id="name" type="text" />
    <input id="btnOK" type="button" value="查询" onclick="f_search()" />
</div> 
<div id="maingrid" style="margin: 0; padding: 0;scrolling="no""></div> 
</body>
</html>