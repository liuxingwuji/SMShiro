<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" /> 
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <link rel="stylesheet" href="<%=path %>/js/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=path %>/js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.core-3.5.js"></script>
    
    <script type="text/javascript">
      var eee;
        $(function () {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                //调试状态，不会提交数据的
                debug: true,
                errorPlacement: function (lable, element) {

                    if (element.hasClass("l-textarea")) {
                        element.addClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().addClass("l-text-invalid");
                    }

                    var nextCell = element.parents("td:first").next("td");
                    nextCell.find("div.l-exclamation").remove(); 
                    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
                },
                success: function (lable) {
                    var element = $("#" + lable.attr("for"));
                    var nextCell = element.parents("td:first").next("td");
                    if (element.hasClass("l-textarea")) {
                        element.removeClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().removeClass("l-text-invalid");
                    }
                    nextCell.find("div.l-exclamation").remove();
                },
                submitHandler: function (form) {
                    //form.submit();
                    //parent.closeWin();
                    $.ajax({
                    	type:"post",
                    	url:"<%=path %>/mroles/save.do",
                    	data:$("#form1").serialize(),
                    	success : function(result) {//返回数据根据结果进行相应的处理  
                        if ( result.flag ) {  
                            $.ligerDialog.question(result.message,function() {
	                            parent.closeWin();
	                        });  
                        } else {  
                             
                        }  
                    }
                    	
                    });
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function () {
                alert(v.element($("#txtName")));
            });
        });  
		
    </script>
    
    <SCRIPT type="text/javascript">
		
		var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
		};
		
		var zTree;  
		var treeNodes;  
  
		$(function(){  
		    $.ajax({  
		        async : false,  
		        cache:false,  
		        type: 'POST',  
		        //dataType : "json",  
		        url: "<%=path %>/menu/menuTree.do",//请求的action路径  
		        error: function () {//请求失败处理函数  
		            alert('请求失败');  
		        },  
		        success:function(data){ //请求成功后处理函数。    
		            treeNodes = data;   //把后台封装好的简单Json格式赋给treeNodes  
		        }  
		    });  
		  
		    //zTree = $("#treeDemo").zTree(setting, treeNodes); 
		    $.fn.zTree.init($("#treeDemo"), setting, treeNodes); 
		});  

		

		function beforeClick(treeId, treeNode) {
			/* var check = (treeNode && !treeNode.isParent);
			if (!check) alert("只能选择城市..."); */
			return true;
		}
		
		function onClick(e, treeId, treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			v = "";
			n = "";
			nodes.sort(function compare(a,b){return a.id-b.id;});
			for (var i=0, l=nodes.length; i<l; i++) {
			   // v += nodes[i].id + ",";
				//n += nodes[i].name + ",";
				v += nodes[i].id ;
				n += nodes[i].name ;
			}
			//if (v.length > 0 ) v = v.substring(0, v.length-1);
			var cityObj = $("#parentId");
			var nameObj = $("#menuName");
			cityObj.attr("value", v);
			nameObj.attr("value", n);
			hideMenu();
		}

		function showMenu() {
			var cityObj = $("#menuName");
			var cityOffset = $("#menuName").offset();
			$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

			$("body").bind("mousedown", onBodyDown);
		}
		function hideMenu() {
			$("#menuContent").fadeOut("fast");
			$("body").unbind("mousedown", onBodyDown);
		}
		function onBodyDown(event) {
			if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
				hideMenu();
			}
		}

		
		//-->
	</SCRIPT>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body>
<form name="form1" method="post"  id="form1"  >
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <tr>
                <td align="right" class="l-table-edit-td">角色编码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="code" type="text" id="codeId" ltype="text" validate="{required:true,minlength:3,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">角色名称:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="name" type="text" id="nameId" ltype="text" validate="{required:true,minlength:2,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
        
            <tr>
                <td align="right" class="l-table-edit-td">描述:</td>
                <td align="left" class="l-table-edit-td" colspan="2"> 
                <textarea cols="100" rows="4" class="l-textarea" name="remark" id="remark" style="width:400px" validate="{}" ></textarea>
                </td> <td align="left"></td>
            </tr>
        </table>
 <br />
<input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /> 
<input type="reset" value="重置" class="l-button"/>
    </form>
    <div style="display:none">
    <!--  数据统计代码 --></div>
    <div id="menuContent" class="menuContent" style="display:none; position: absolute;">
						<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
					</div>
</body>
</html>