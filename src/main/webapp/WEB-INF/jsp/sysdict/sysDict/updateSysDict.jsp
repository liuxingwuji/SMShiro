<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>修改数据字典</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file="/common/addOrUpdateJS.jsp"%>
	<script type="text/javascript">
	$(function () {
	$.metadata.setType("attr", "validate");
	var v = $("form").validate({
	//调试状态，不会提交数据的
	debug: true,
	errorPlacement: function (lable, element) {

	if (element.hasClass("l-textarea")) {
	element.addClass("l-textarea-invalid");
	}
	else if (element.hasClass("l-text-field")) {
	element.parent().addClass("l-text-invalid");
	}

	var nextCell = element.parents("td:first").next("td");
	nextCell.find("div.l-exclamation").remove();
	$('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip();
	},
	success: function (lable) {
	var element = $("#" + lable.attr("for"));
	var nextCell = element.parents("td:first").next("td");
	if (element.hasClass("l-textarea")) {
	element.removeClass("l-textarea-invalid");
	}
	else if (element.hasClass("l-text-field")) {
	element.parent().removeClass("l-text-invalid");
	}
	nextCell.find("div.l-exclamation").remove();
	},
	submitHandler: function (form) {
	//form.submit();
	//parent.closeWin();
	$.ajax({
	type:"post",
	url:"<%=path %>/sysDict/updateSysDict.do",
	data:$("#form1").serialize(),
	success : function(result) {//返回数据根据结果进行相应的处理
	if ( result.flag ) {
	$.ligerDialog.question(result.message,function() {
	parent.closeWin();
	});
	} else {

	}
	}

	});
	}
	});
	$("form").ligerForm();
	$(".l-button-test").click(function () {
	alert(v.element($("#txtName")));
	});
	});
	</script>
	<style type="text/css">
		body{ font-size:12px;}
		.l-table-edit {}
		.l-table-edit-td{ padding:4px;}
		.l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
		.l-verify-tip{ left:230px; top:120px;}
	</style >
</head>

<body style="background-color:#FFFFFF">
	<form name="form1" method="post"  id="form1"  >
		<input type="hidden" class="text" id="dtoid" name="id"
			   notNull="false" maxLength="11" value="${dto.id}" />
		<table cellpadding="0" cellspacing="0" class="l-table-edit" >
			<tr>
				<td align="right" class="l-table-edit-td">数据字典名称:</td>
				<td align="left" class="l-table-edit-td" style="width:160px">
					<input name="dictName" type="text" id="dictName" ltype="text" validate="{required:true,minlength:3,maxlength:10}" value="${dto.dictName}" /></td>
				<td align="left"></td>
			</tr>
			<tr>
				<td align="right" class="l-table-edit-td">数据字典名称编码:</td>
				<td align="left" class="l-table-edit-td" style="width:160px">
					<input name="dictCode" type="text" id="dictCode" ltype="text" validate="{required:true,minlength:3,maxlength:10}" value="${dto.dictCode}"/></td>
				<td align="left"></td>
			</tr>
			<tr>
				<td align="right" class="l-table-edit-td" valign="top">类型:</td>
				<td align="left" class="l-table-edit-td" style="width:160px">
					<select id="dictType" name="dictType">
						<option value=1>项目级</option>
						<option value=0>系统级</option>
					</select>
				</td><td align="left"></td>
			</tr>
			<tr>
				<td><input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /></td><td><input type="reset" value="重置" class="l-button"/></td>
			</tr>
		</table>
		<br />


	</form>
</body>

<script type="text/javascript">
	$(document).ready(function() {
		checkedInit();
	});
</script>

</html>
