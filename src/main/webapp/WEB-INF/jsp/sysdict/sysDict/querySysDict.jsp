<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html  >
<head>
  <title>数据字典</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file="/common/ligerUIJS.jsp"%>
     <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
      <style type="text/css"> 

            body{ padding:5px; margin:0; padding-bottom:15px;}
            #layout1{  width:100%;margin:0; padding:0;  }  
            .l-page-top{ height:80px; background:#f8f8f8; margin-bottom:3px;}
            h4{ margin:20px;}
       </style>
       <script type="text/javascript">
        $(function ()
                {

                    $("#layout1").ligerLayout({ 
                    leftWidth: 550,
                    InWindow:true,
                	allowLeftCollapse:false
                    });
                });
       
       var grid = null;
       var girdDetail=null;
       var win=null;
        $(function ()
        {
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '数据字典类别编码', name: 'dictCode', align: 'cneter', width: 150
                   ,  render: function (item) {
                        return "<a href='javascript:f_searchDetail("+item.id+")'>"+item.dictCode+"</a>";
                    }  } ,
                  { display: '数据字典类别名称', name: 'dictName', width: 150, align: 'cneter' },
                    { display: '数据字典类型', name: 'dictType', width: 150, align: 'cneter',
                        render: function (record, rowindex, value, column) {

                            if(value==0){
                                return '系统级'
                            }
                            if(value==1){
                                return '项目级'
                            }

                        } }
                ], 
                url:"<%=path%>/sysDict/queryListSysDict.do",
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 5, 
                pageSizeOptions :[5,10, 20, 30, 40],
                width: '99.8%', 
                height: '99%',
               onSelectRow  : function (c, rowindex, rowobj)
                {
                    $("#dictId").val(c.id);
                    f_searchDetail(c.id);
                },
            toolbar:{items:[
                	{text:'增加',click:createSysDict,icon:'add'},
                	{ line: true },
                	{text:'修改',click:editSysDict,icon:'modify'},
                	{ line: true },
                	{text:'删除',click:delSysDict,img:'<%=path %>/js/ligerUI/ligerUI/skins/icons/delete.gif'}
                	
                ]}
            }); 
        });

        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'userName', value: $("#userName").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
      
       	
        //新增数据字典
	    function createSysDict(){
	        win= $.ligerDialog.open({
             name:'sysdicts',
	         title:'新增',
	         url: "<%=path%>/sysDict/query/toAdd.do", 
	         height: 350, 
	         width: 500,
                buttons: [
                    { text: '确定', onclick: function (item, dialog) {document.getElementById('sysdicts').contentWindow.submits(); },cls:'l-dialog-btn-highlight' },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ],
                isResize: true
            });
	    }
	    //修改数据字典
    	function editSysDict(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要修改的数据！');
    			return;
    		}
    		win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/sysDict/query/toUpdate.do?id='+del.id, 
	         height: 350, 
	         width: 500, 

             isResize: true
            });
    	}
    	//删除数据字典
    	function delSysDict(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要删除的数据！');
    			return;
    		}
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysDict/deleteSysDict.do',//这里是接收数据的PHP程序
	                data:{id:del.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            f_search();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    	
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}

        //关闭弹出窗口
        function closeWinDetail(){
            win.close();
            f_searchDetail($("#dictId").val());
        }
/***************数据字典详情*******************/

    	$(function ()
        {
           girdDetail= $("#maingridDetail").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                  {display: '数据字典名称', name: 'dictDetailName', align: 'cneter', width: 150} ,
                  { display: '数据字典编码', name: 'dictDetailValue', width: 150, align: 'cneter' },
                    {display: '是否启用', name: 'validateState', align: 'cneter', width: 150,
                        render: function (record, rowindex, value, column) {

                            if(value==0){
                                return '关闭'
                            }
                            if(value==1){
                                return '启用'
                            }

                        } }
                ],
                url:"<%=path%>/sysDictDetail/queryListSysDictDetail.do",
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 5,
                pageSizeOptions :[5,10, 20, 30, 40],
                width: '99.8%',
                height: '99%',
                toolbar:{items:[
                	{text:'增加',click:createSysDictDetail,icon:'add'},
                	{ line: true },
                	{text:'修改',click:editSysDictDetail,icon:'modify'},
                	{ line: true },
                	{text:'删除',click:delSysDictDetail,img:'<%=path %>/js/ligerUI/ligerUI/skins/icons/delete.gif'}

                ]}
            });
        });





        function f_searchDetail(val)
        {

            $("#dictId").val(val)
            girdDetail.options.data = $.extend(true, {}, girdDetail);
            girdDetail.setOptions({
            parms: [
                { name: 'dictId', value: $("#dictId").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        girdDetail.loadData(); 
        } 
       	
        //新增
	    function createSysDictDetail(){
	        win= $.ligerDialog.open({
                name:'detailAdd',
	         title:'新增',
	         url: '<%=path%>/sysDictDetail/query/toAdd.do?dictId='+$("#dictId").val(), 
	         height: 350, 
	         width: 500,
                buttons: [
                    { text: '确定', onclick: function (item, dialog) {document.getElementById('detailAdd').contentWindow.submits(); },cls:'l-dialog-btn-highlight' },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ],
             isResize: true
            });
	    }
	    //修改用户
    	function editSysDictDetail(){
    		var edit=girdDetail.getSelected();
    		if(edit==null){
    			$.ligerDialog.question('请选中要修改的数据！');
    			return;
    		}
    		win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/sysDictDetail/query/toUpdate.do?id='+edit.id,
	         height: 350, 
	         width: 500, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
    	}
    	//删除用户
    	function delSysDictDetail(){
    		var del=girdDetail.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要删除的数据！');
    			return;
    		}
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysDictDetail/deleteSysDictDetail.do',//这里是接收数据的PHP程序
	                data:{id:del.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
                            f_searchDetail();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    
    </script>
</head>
<body >



 <div id="layout1">
            <div position="left" title="数据字典类型">
            	 <div id="searchbar" style="background-color: #C6E2FF;height: 30px;width: 100%">
				    帐号：<input id="userName" type="text" />
				    <input id="btnOK" type="button" value="查询" onclick="f_search()" />
				</div> 
				<div id="maingrid" style="margin: 0; padding: 0;" scrolling="no"></div>
            
            </div>
            
            <div position="center" title="数据字典详细">
           		<input type="hidden" id="dictId" name="dictId">
				<div id="maingridDetail" style="margin-left: 555; padding: 0;" scrolling="no"></div>
            </div>  
        </div> 
</body>
</html>