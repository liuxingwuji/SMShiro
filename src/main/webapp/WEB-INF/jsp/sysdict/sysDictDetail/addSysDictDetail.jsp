<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>新增数据字典明细表</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" />
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>

    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>

    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>
 <script type="text/javascript">
      var eee;
        $(function () {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                //调试状态，不会提交数据的
                debug: true,
                errorPlacement: function (lable, element) {

                    if (element.hasClass("l-textarea")) {
                        element.addClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().addClass("l-text-invalid");
                    }

                    var nextCell = element.parents("td:first").next("td");
                    nextCell.find("div.l-exclamation").remove(); 
                    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
                },
                success: function (lable) {
                    var element = $("#" + lable.attr("for"));
                    var nextCell = element.parents("td:first").next("td");
                    if (element.hasClass("l-textarea")) {
                        element.removeClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().removeClass("l-text-invalid");
                    }
                    nextCell.find("div.l-exclamation").remove();
                },
                submitHandler: function (form) {

                    $.ajax({
                    	type:"post",
                    	url:"<%=path %>/sysDictDetail/insertSysDictDetail.do",
                    	data:$("#form1").serialize(),
                    	success : function(result) {//返回数据根据结果进行相应的处理
                            if ( result.flag ) {
                                $.ligerDialog.question(result.message,function() {
                                    parent.closeWinDetail();
                                });
                            }
                        }
                    	
                    });
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function () {
                alert(v.element($("#txtName")));
            });
        });
      function submits(){
          parent.closeWinDetail();
         // document.forms['form1'].submit();
      }
    </script>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style >
</head>

<body style="background-color:#FFFFFF">
	<div id="formPageSwap">
		<br />
		<form id="form1" name="form1" >
			<input type="hidden" id="dtodictId" name="dictId" value="${dictId }" />
			<table cellpadding="0" cellspacing="0" class="l-table-edit"  align="center">
            <tr>
                <td align="right" class="l-table-edit-td">数据字典详细名称:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                <input name="dictDetailName" type="text" id="dictDetailName" ltype="text" validate="{required:true,minlength:1,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">数据字典详细编码:</td>
                <td align="left" class="l-table-edit-td" style="width:160px">
                <input name="dictDetailValue" type="text" id="dictDetailValue" ltype="text" validate="{required:true,minlength:1,maxlength:10}" /></td>
                <td align="left"></td>
            </tr>
            
            <tr>
            	<td><input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /></td><td><input type="reset" value="重置" class="l-button"/></td>
            </tr>
        </table>

			<!-- 保存 关闭 按钮 在 查询页面进行控制 -->
		</form>

	</div>

</body>


</html>
