<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>信息发布</title>
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8" src="<%=path%>/js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="<%=path%>/js/ueditor/ueditor.all.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="<%=path%>/js/ueditor/lang/zh-cn/zh-cn.js"></script>

    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerWindow.js" type="text/javascript"></script>

    <style type="text/css">
        div{
            width:100%;
        }


    </style>
    <script type="text/javascript">

        //实例化编辑器
        //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
        var ue = UE.getEditor('editor');


        function isFocus(e){
            alert(UE.getEditor('editor').isFocus());
            UE.dom.domUtils.preventDefault(e)
        }
        function setblur(e){
            UE.getEditor('editor').blur();
            UE.dom.domUtils.preventDefault(e)
        }

        function createEditor() {
            enableBtn();
            UE.getEditor('editor');
        }
        function getAllHtml() {
            alert(UE.getEditor('editor').getAllHtml())
        }
        function getContent() {
            var arr = [];
            arr.push(UE.getEditor('editor').getContent());
            alert(arr.join("\n"));
        }

        function getText() {
            //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
            var range = UE.getEditor('editor').selection.getRange();
            range.select();
            var txt = UE.getEditor('editor').selection.getText();
            alert(txt)

        }

        function submitNotice() {
            var title=$("#title").val();
            var txt=$("#editor").val();
            if(title!=null&&title.length>0){
                $.ajax({
                    type:"post",
                    url:"<%=path %>/ueditor/insertNotice.do",
                    data:$("#form1").serialize(),
                    success : function(result) {//返回数据根据结果进行相应的处理
                        if ( result.flag ) {
                            $.ligerDialog.question(result.message,function() {
                                parent.closeWin();
                            });
                        } else {
                            $.ligerDialog.question(result.message);
                        }
                    }
                });
            }else{
                $.ligerDialog.question("通知标题或内容不允许为空！！！");
            }
        }
    </script>


</head>
<body>
<div   align="center" style=" line-height:22px">
    <%--<h1>发布通知</h1>--%>
    <form name="form1" id="form1" method="post">
        <input type="hidden" name="author" value="admin">
        <input type="text" id="title" name="title" style="width: 900px;height:25px;padding-top: 10px;" placeholder="请输入标题" value=""><br><br>
        <textarea id="editor" name="contents" style="width:900px;height:500px;" align="center"></textarea>
        <%--<input type="submit" value="发布通知">--%><br>
        归属网站：
        <input id="rbtnl_0" type="radio" name="belong" value="0" checked="checked" /><label for="rbtnl_0">开德集团</label>
        <input id="rbtnl_1" type="radio" name="belong" value="1" /><label for="rbtnl_1">长城智能</label>
    </form>

</div>
 <%--   <div id="btns">
        <div>
        <button onclick="getContent()">获得内容</button>
        </div>
    </div>--%>



</body>
</html>