<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<%@ include file="/common/ligerUIJS.jsp"%>


	<script type="text/javascript">
        var grid = null;
        var win=null;
        $(function ()
        {
            grid= $("#maingrid").ligerGrid({
                checkbox: true,
                isSingleCheck :true,//单选模式
                columns: [
                    {display: '归属', name: 'belong', align: 'cneter', width: 150,render: function (item){
                        if(item.belong==1){
                            return '长城智能';
                        }else{
                            return '开德';
                        }
                    } } ,
                    { display: '超链接', name: 'url', width: 150, align: 'cneter' },
                    { display: '图片地址', name: 'imgpath', width: 350 ,align: 'cneter'},
                    { display: '状态', name: 'status', width: 150,render: function (item){
                        if(item.status==1){
							return '有效';
						}else{
                            return '无效';
						}
					} }

                ],
                url:"<%=path %>/ueditor/listBanner.do",
                rownumbers:true,
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 10,
                pageSizeOptions :[10, 20, 30, 50],
                width: '99.8%',
                height: '99%',
                toolbar:{items:[
                    {text:'新增',click:create,icon:'add'},
                    { line: true },
                    {text:'修改',click:editNews,icon:'modify'},
                    { line: true },
                    {text:'删除',click:delNotice,img:'<%=path %>/js/ligerUI/ligerUI/skins/icons/delete.gif'}
                ]}
            });
        });

        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
                parms: [
                    { name: 'title', value: $("#title").val()},
                    { name: 'phone', value: $("#phone").val()},
                    { name: 'sex', value: $("#sex").val()},
                    { name: 'name', value: $("#name").val()}
                ]
            });
            //按查询条件导入到grid中
            grid.loadData();
        }


        //新增
        function create(){
            win= $.ligerDialog.open({
                name:'notices',

                title:'Banner新增',
                url: '<%=path %>/ueditor/query/toAddBanner.do',
                height: 800,
                width: 1025,
               /* buttons: [
                    { text: '确定', onclick: function (item, dialog) {document.getElementById('notices').contentWindow.submitNotice(); },cls:'l-dialog-btn-highlight' },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ],*/
                isResize: true
            });
        }
        //修改用户
        function editNews(){
            var edit=grid.getSelected();
            if(edit==null){
                $.ligerDialog.question('请选中要修改的数据！');
                return;
            }
            win= $.ligerDialog.open({
                title:'修改',
                name:'notices',
                url: '<%=path %>/ueditor/query/toUpdateBanner.do?id='+edit.id,
                height: 800,
                width: 1025,
               /* buttons: [
                    { text: '确定', onclick: function (item, dialog) {document.getElementById('notices').contentWindow.submitNotice();  },cls:'l-dialog-btn-highlight' },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ],*/
                isResize: true
            });

        }
        //删除用户
        function delNotice(){
            var del=grid.getSelected();
            if(del==null){
                $.ligerDialog.question('请选中要删除的通知！');
                return;
            }
            if(confirm("请确认是否删除?")){
                $.ajax({
                    type:'post',//可选get
                    url:'<%=path %>/ueditor/delNotice.do',//这里是接收数据的PHP程序
                    data:{id:del.id},
                    dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
                    success:function(content){
                        $.ligerDialog.question(content.message,function() {
                            f_search();
                        });
                    },
                    error:function(){
                        $.ligerDialog.question('删除失败！');
                    }
                }) ;
            }
        }

        function show(id){
            win= $.ligerDialog.open({
                title:'详细',
                url: '<%=path %>/ueditor/query/toShowNotice.do?id='+id,
                height: 800,
                width: 1024,
                isResize: true
            });
        }
        //关闭弹出窗口
        function closeWin(){
            win.close();
            f_search();
        }


	</script>
	<style type="text/css">
		.btn
		{
			width:100px;
			height:25px;
			font-family: "Arial","Tahoma","微软雅黑","雅黑";
			color:#000;
			/* border-radius:6px; -moz-border-radius:6px; -webkit-border-radius:6px;  */
		}
		.l-text-wrapper {
			position: relative;
			width: 130px;
		}

		.page { display:block; overflow:hidden;float:left; width:100%;}
		.pageHeader { display:block; overflow:auto; margin-bottom:1px; padding:5px; border-style:solid; border-width:0 0 1px 0; position:relative;background-color: #C6E2FF;height: auto;}
		.searchBar {}
		.searchBar ul.searchContent { display:block; overflow:hidden; height:25px;}
		.searchBar ul.searchContent li { float:left; display:block; overflow:hidden; width:300px; height:21px; padding:2px 0;}
		.searchBar label { float:left; width:80px; padding:0 5px; line-height:23px;}
		.searchBar .searchContent td{padding-right:20px; white-space:nowrap; height:25px}
		.searchBar .subBar { height:25px;}
		.searchBar .subBar ul { float:right;}
		.searchBar .subBar li { float:left; margin-left:5px;}
		.pageContent { display:block;overflow:auto;position:relative;}
	</style>
</head>
<body >
<div class="pageHeader">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>
					标题：<input type="text" name="title"  id="title" />
				</td>

				<td>
					发布日期：
					<input id="beginTime"  class="Wdate" name="beginTime" type="text" onClick="WdatePicker({startDate:'%y/%M/%d',dateFmt:'yyyy/MM/dd'})" />
					~<input id="endTime"  class="Wdate" name="endTime" type="text" onClick="WdatePicker({startDate:'%y/%M/%d',dateFmt:'yyyy/MM/dd'})" />
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button class="btn" type="submit"  onclick="f_search()" >查&nbsp;&nbsp;询</button></div></div></li>
			</ul>
		</div>
	</div>
	<!-- </form> -->

</div>

<div id="maingrid" style="margin: 0; padding: 0;scrolling:no"></div>
</body>
</html>