<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>信息综合管理系统</title>
    <link rel="shortcut icon" href="<%=path %>/images/head.ico" type="image/x-icon"/>
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" id="mylink"/>
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTab.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery.cookie.js"></script>
    <script type="text/javascript">

        var tab = null;
        var accordion = null;
        var tree = null;
        var tabItems = [];
        $(function ()
        {

            //布局
            $("#layout1").ligerLayout({ leftWidth: 190, height: '100%',heightDiff:-34,space:4, onHeightChanged: f_heightChanged });

            var height = $(".l-layout-center").height();

            //Tab
            $("#framecenter").ligerTab({
                height: height,
                showSwitchInTab : true,
                showSwitch: true,
                onAfterAddTabItem: function (tabdata)
                {
                    tabItems.push(tabdata);
                    //saveTabStatus();
                },
                onAfterRemoveTabItem: function (tabid)
                {
                    for (var i = 0; i < tabItems.length; i++)
                    {
                        var o = tabItems[i];
                        if (o.tabid == tabid)
                        {
                            tabItems.splice(i, 1);
                            // saveTabStatus();
                            break;
                        }
                    }
                },
                onReload: function (tabdata)
                {
                    var tabid = tabdata.tabid;
                    addFrameSkinLink(tabid);
                }
            });

            //面板
            $("#accordion1").ligerAccordion({
                height: height - 24, speed: null
            });

            $(".l-link").hover(function ()
            {
                $(this).addClass("l-link-over");
            }, function ()
            {
                $(this).removeClass("l-link-over");
            });
            //树

            $("#tree1").ligerTree({
                url:'<%=path %>/menu/showTree.do',
                checkbox: false,
                slide: false,
                nodeWidth: 120,
                idFieldName :'menuId',
                textFieldName :'text',
                parentIDFieldName :'parent_id',
                attribute: ['nodename', 'url'],
                render : function(a){
                    if (!a.isnew) return a.text;
                    return '<a href="' + a.url + '" target="_blank">' + a.text + '</a>';
                },
                onSelect: function (node)
                {
                    if (!node.data.url) return;
                    if (node.data.isnew)
                    {
                        return;
                    }
                    var tabid = $(node.target).attr("tabid");
                    if (!tabid)
                    {
                        tabid = new Date().getTime();
                        $(node.target).attr("tabid", tabid)
                    }
                    f_addTab(tabid, node.data.text, node.data.url);
                }
            });

            function openNew(url)
            {
                var jform = $('#opennew_form');
                if (jform.length == 0)
                {
                    jform = $('<form method="post" />').attr('id', 'opennew_form').hide().appendTo('body');
                } else
                {
                    jform.empty();
                }
                jform.attr('action', url);
                jform.attr('target', '_blank');
                jform.trigger('submit');
            };


            tab = liger.get("framecenter");
            accordion = liger.get("accordion1");
            tree = liger.get("tree1");
            $("#pageloading").hide();

            css_init();
            // pages_init();
        });


        function f_heightChanged(options)
        {
            if (tab)
                tab.addHeight(options.diff);
            if (accordion && options.middleHeight - 24 > 0)
                accordion.setHeight(options.middleHeight - 24);
        }
        function f_addTab(tabid, text, url)
        {
            tab.addTabItem({
                tabid: tabid,//显示菜单标题
                text: text,
                url:'<%=path %>'+ url,
                callback: function ()
                {
                    //addShowCodeBtn(tabid);
                    addFrameSkinLink(tabid);
                }
            });
        }

        function addFrameSkinLink(tabid)
        {
            var prevHref = getLinkPrevHref(tabid) || "";
            var skin = getQueryString("skin");
            if (!skin) return;
            skin = skin.toLowerCase();
            attachLinkToFrame(tabid, prevHref + skin_links[skin]);
        }
        var skin_links = {
            "aqua": "js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css",
            "gray": "js/ligerUI/ligerUI/skins/Gray/css/all.css",
            "silvery": "js/ligerUI/ligerUI/skins/Silvery/css/style.css",
            "gray2014": "js/ligerUI/ligerUI/skins/gray2014/css/all.css"
        };
        /* function pages_init()
         {
         var tabJson = $.cookie('liger-home-tab');
         if (tabJson)
         {
         var tabitems = JSON2.parse(tabJson);
         for (var i = 0; tabitems && tabitems[i];i++)
         {
         f_addTab(tabitems[i].tabid, tabitems[i].text, tabitems[i].url);
         }
         }
         } */
        /* function saveTabStatus()
         {
         $.cookie('liger-home-tab', JSON2.stringify(tabItems));
         } */
        function css_init()
        {
            var css = $("#mylink").get(0), skin = getQueryString("skin");
            $("#skinSelect").val(skin);
            $("#skinSelect").change(function ()
            {
                if (this.value)
                {
                    location.href = "index.htm?skin=" + this.value;
                } else
                {
                    location.href = "index.htm";
                }
            });


            if (!css || !skin) return;
            skin = skin.toLowerCase();
            $('body').addClass("body-" + skin);
            $(css).attr("href", skin_links[skin]);
        }
        function getQueryString(name)
        {
            var now_url = document.location.search.slice(1), q_array = now_url.split('&');
            for (var i = 0; i < q_array.length; i++)
            {
                var v_array = q_array[i].split('=');
                if (v_array[0] == name)
                {
                    return v_array[1];
                }
            }
            return false;
        }
        function attachLinkToFrame(iframeId, filename)
        {
            if(!window.frames[iframeId]) return;
            var head = window.frames[iframeId].document.getElementsByTagName('head').item(0);
            var fileref = window.frames[iframeId].document.createElement("link");
            if (!fileref) return;
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
            head.appendChild(fileref);
        }
        function getLinkPrevHref(iframeId)
        {
            if (!window.frames[iframeId]) return;
            var head = window.frames[iframeId].document.getElementsByTagName('head').item(0);
            var links = $("link:first", head);
            for (var i = 0; links[i]; i++)
            {
                var href = $(links[i]).attr("href");
                if (href && href.toLowerCase().indexOf("ligerui") > 0)
                {
                    return href.substring(0, href.toLowerCase().indexOf("lib") );
                }
            }
        }
        //修改密码
        function upatePass(){
            win= $.ligerDialog.open({
                title:'用户 密码修改',
                url: '<%=path %>/sysUser/query/toUpdatePass.do',
                height: 300,
                width: 450,
                isResize: true
            });
        }
        //自动出发退出功能
        function autoQuit(){
            if(document.all) {
                document.getElementById("quit").click();
            }
            // 其它浏览器
            else {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                document.getElementById("quit").dispatchEvent(e);

            }
        }

        function setIframeHeight(iframe) {
            if (iframe) {
                var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    iframe.height = (iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight);

                }
            }
        };

        window.onload = function () {
            setIframeHeight(document.getElementById('home'));
        };

    </script>
    <style type="text/css">
      body,html{height:100%;}
      body{ padding:0px; margin:0;   overflow:hidden;}
      .l-link2{text-decoration:underline; color:white; margin-left:2px;margin-right:2px;}
      #pageloading{position:absolute; left:0px; top:0px; background:white url('../images/loading.gif') no-repeat center; width:100%; height:100%;z-index:99999;}
      .l-link-over{ background:#FFEEAC; border:1px solid #DB9F00;}
      .space{ color:#E7E7E7;}
      /* 顶部 */
      .l-topmenu{ margin:0; padding:0; height:31px; line-height:31px; background:url('../js/ligerUI/images/top.jpg') repeat-x bottom;  position:relative; border-top:1px solid #1D438B;  }
      .l-topmenu-logo{ color:#E7E7E7; padding-left:35px; line-height:26px;background:url('../js/ligerUI/images/topicon.gif') no-repeat 10px 5px;}
      .l-topmenu-welcome{  position:absolute; height:24px; line-height:24px;  right:30px; top:2px;color:#070A0C;}
      .l-topmenu-welcome a{ color:#E7E7E7; text-decoration:underline}
      .body-gray2014 #framecenter{
          margin-top:3px;
      }

    </style>
</head>
<body style="padding:0px;background:#EAEEF5;">
<div id="pageloading"></div>
<div id="topmenu" class="l-topmenu">
    <div class="l-topmenu-logo">信息综合管理系统</div>
    <div class="l-topmenu-welcome">
        <span class="l-link2">帐号：<shiro:principal ></shiro:principal></span>
        <span class="space">|</span>
        <span class="l-link2"><a href="#" class="l-link2" onclick="upatePass()">密码修改</a></span>
        <span class="space">|</span>
        <a href="<%=path %>/logout" class="l-link2" target="_self" id="quit">退出</a>

    </div>
</div>
<div id="layout1" style="width:99.2%; margin:0 auto; margin-top:4px; ">
    <div position="left"  title="主要菜单" id="accordion1">
        <div title="功能列表" class="l-scroll">
            <ul id="tree1" style="margin-top:3px;">
        </div>

    </div>
    <div position="center" id="framecenter">
        <div tabid="home" title="我的主页" style="min-height:600px" >
            <iframe frameborder="0" name="home" id="home" src="../welcome.jsp" onload="setIframeHeight(this)"></iframe>
        </div>
    </div>

</div>
<div  style="height:32px; line-height:32px; text-align:center;">
    齐心协力、众志成城 © 2018
</div>
<div style="display:none"></div>
</body>
</html>