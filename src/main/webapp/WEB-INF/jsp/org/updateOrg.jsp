<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Tab/css/form.css" rel="stylesheet" type="text/css" />
    <script src="<%=path %>/js/ligerUI/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>

    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.validate.min.js"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/jquery-validation/messages_cn.js" type="text/javascript"></script>

    <link rel="stylesheet" href="<%=path %>/js/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=path %>/js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=path %>/js/ztree/js/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript">
        var eee;
        $(function ()
        {
            $.metadata.setType("attr", "validate");
            var v = $("form").validate({
                debug: true,
                errorPlacement: function (lable, element)
                {
                    if (element.hasClass("l-textarea"))
                    {
                        element.ligerTip({ content: lable.html(), target: element[0] });
                    }
                    else if (element.hasClass("l-text-field"))
                    {
                        element.parent().ligerTip({ content: lable.html(), target: element[0] });
                    }
                    else
                    {
                        lable.appendTo(element.parents("td:first").next("td"));
                    }
                },
                success: function (lable)
                {
                    lable.ligerHideTip();
                    lable.remove();
                },
                submitHandler: function ()
                {
                   // $("form .l-text,.l-textarea").ligerHideTip();
                    $.ajax({
                        type:"post",
                        url:"<%=path %>/sysOrg/updateOrg.do",
                        data:$("#form1").serialize(),
                        success : function(result) {//返回数据根据结果进行相应的处理
                            if ( result.flag ) {
                                $.ligerDialog.question(result.message,function() {
                                    parent.closeWin();
                                });
                            } else {

                            }
                        }

                    });
                }
            });
            $("form").ligerForm();
            $(".l-button-test").click(function ()
            {
                alert(v.element($("#txtName")));
            });
        });
    </script>
    <script type="text/javascript">

        var setting = {
            view: {
                dblClickExpand: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            async: {
                //异步加载
                enable: true,
                contentType: "application/json",
                url: "<%=path %>/sysOrg/orgTree.do",
                autoParam: ["id", "pId", "name"]
            },
            callback: {
                beforeClick: beforeClick,
                onClick: onClick,
                onAsyncSuccess: zTreeOnAsyncSuccess
            }
        };

        /*var zNodes =[
            {id:1, pId:0, name:"北京"},
            {id:2, pId:0, name:"天津"},
            {id:3, pId:0, name:"上海"},
            {id:6, pId:0, name:"重庆"},
            {id:4, pId:0, name:"河北省", open:true},
            {id:41, pId:4, name:"石家庄"},
            {id:42, pId:4, name:"保定"},
            {id:43, pId:4, name:"邯郸"},
            {id:44, pId:4, name:"承德"},
            {id:5, pId:0, name:"广东省", open:true},
            {id:51, pId:5, name:"广州"},
            {id:52, pId:5, name:"深圳"},
            {id:53, pId:5, name:"东莞"},
            {id:54, pId:5, name:"佛山"},
            {id:6, pId:0, name:"福建省", open:true},
            {id:61, pId:6, name:"福州"},
            {id:62, pId:6, name:"厦门"},
            {id:63, pId:6, name:"泉州"},
            {id:64, pId:6, name:"三明"}
        ];*/

        function beforeClick(treeId, treeNode) {
            var check = (treeNode && !treeNode.isParent);
           // if (!check) alert("只能选择城市...");
           // return check;
            return true;
        }

        function onClick(e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
                nodes = zTree.getSelectedNodes(),
                v = "";
                p_code = "";
            nodes.sort(function compare(a,b){return a.id-b.id;});
            for (var i=0, l=nodes.length; i<l; i++) {
                v += nodes[i].name + ",";
                p_code += nodes[i].id ;
            }
            if (v.length > 0 ) v = v.substring(0, v.length-1);
            var cityObj = $("#citySel");
            var p_codev = $("#parentCode");
            cityObj.attr("value", v);
            p_codev.attr("value", p_code);
            hideMenu();
        }

        function showMenu() {
            var cityObj = $("#citySel");
            var cityOffset = $("#citySel").offset();
            $("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

            $("body").bind("mousedown", onBodyDown);
        }
        function hideMenu() {
            $("#menuContent").fadeOut("fast");
            $("body").unbind("mousedown", onBodyDown);
        }
        function onBodyDown(event) {
            if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
                hideMenu();
            }
        }
        //修改菜单默认选中
        function zTreeOnAsyncSuccess(event, treeId, treeNode, msg){
            var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
            var node = treeObj.getNodeByParam("id", $("#parentCode").val(), null);
            treeObj.selectNode(node);
            treeObj.setting.callback.onClick(null, treeObj.setting.treeId, node);//调用事件
        }
        $(document).ready(function(){
           // $.fn.zTree.init($("#treeDemo"), setting, zNodes);
            $.fn.zTree.init($("#treeDemo"), setting);

        });

	</script>
     <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test,.l-button{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;color: red}
    </style >
</head>
<body >
<form name="form1" method="post"  id="form1">
    <input type="hidden" name="ids" value="${org.id }">
    <input type="hidden" name="code" value="${org.code }">
    <div>
    </div>
    <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <tr>
            <td align="right" class="l-table-edit-td">组织编码:</td>
            <td align="left" class="l-table-edit-td">
                ${org.code }
            </td>
            <td align="left"></td>
        </tr>

        <tr>
            <td align="right" class="l-table-edit-td" valign="top">组织名称:</td>
            <td align="left" class="l-table-edit-td">
                <input name="name" type="text" id="txtMenuName" ltype="text" value="${org.name }" validate="{required:true,minlength:2}" />
            </td><td align="left"></td>
        </tr>

        <tr>
            <td align="right" class="l-table-edit-td">上级部门:</td>
            <td align="left" class="l-table-edit-td">
                <input type="hidden" name="parentCode" id="parentCode" value="${org.parent_code }">
                <input id="citySel" type="text" readonly value="" style="width:120px;" onclick="showMenu(); return false;"/>
            </td><td align="left"></td>
        </tr>
        <tr>
            <td align="right" class="l-table-edit-td">状态:</td>
            <td align="left" class="l-table-edit-td">
                <c:if test="${org.state==1}">
                    <input id="rbtnl_0" type="radio" name="state" value="1" checked /><label for="rbtnl_0">有效</label>
                    <input id="rbtnl_1" type="radio" name="state" value="0" /><label for="rbtnl_1">无效</label>
                </c:if>
                <c:if test="${org.state==0}">
                    <input id="rbtnl_0" type="radio" name="state" value="1"  /><label for="rbtnl_0">有效</label>
                    <input id="rbtnl_1" type="radio" name="state" value="0" checked/><label for="rbtnl_1">无效</label>

                </c:if>
            </td>
        </tr>
        <tr>
            <td align="right" class="l-table-edit-td">描述:</td>
            <td align="left" class="l-table-edit-td">
                <textarea cols="100" rows="4" class="l-textarea" id="address" style="width:400px" validate="{required:false}" ></textarea>
            </td><td align="left"></td>
        </tr>
    </table>
    <br />
    <input type="submit" value="提交" id="Button1" class="l-button l-button-submit" />
    <input type="reset" value="重置" class="l-button"/>
</form>
<div style="display:none">
    <!--  数据统计代码 --></div>

<div id="menuContent" class="menuContent" style="display:none; position: absolute;">
    <ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
</div>
</body>
</html>