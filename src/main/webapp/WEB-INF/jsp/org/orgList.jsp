<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link rel="stylesheet" href="<%=path %>/js/plugins/ztreeV3/css/demo.css" type="text/css">
    <link rel="stylesheet" href="<%=path %>/js/plugins/ztreeV3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.excheck.js"></script>
    <script type="text/javascript" src="<%=path %>/js/plugins/ztreeV3/js/jquery.ztree.exedit.js"></script>

    <link href="<%=path %>/js/ligerUI/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/js/ligerUI/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="<%=path %>/js/ligerUI/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="<%=path %>/js/ligerUI/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <style type="text/css">
        body{  font-size:20px;font-family:"微软雅黑","方正兰亭黑简体";}
        .ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
    </style>
    <SCRIPT type="text/javascript">

        var setting = {
            view: {
                addHoverDom: addHoverDom,
                removeHoverDom: removeHoverDom,
                selectedMulti: false
            },
            edit: {
                enable: true,
                editNameSelectAll: true,
                showRemoveBtn: showRemoveBtn,
                showRenameBtn: showRenameBtn
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            async: {
                //异步加载
                enable: true,
                contentType: "application/json",
                url: "<%=path %>/sysOrg/orgTree.do",
                autoParam: ["id", "pId", "name"]
            },
            callback: {
                beforeDrag: beforeDrag,
                beforeEditName: beforeEditName,
                beforeRemove: beforeRemove,
                beforeRename: beforeRename,
                onRemove: onRemove,
                onRename: onRename
            }
        };


        var log, className = "dark";
        function beforeDrag(treeId, treeNodes) {
            return false;
        }
        function beforeEditName(treeId, treeNode) {//修改菜单
            className = (className === "dark" ? "":"dark");
            showLog("[ "+getTime()+" beforeEditName ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.selectNode(treeNode);
            setTimeout(function() {
                if (confirm("是否对 " + treeNode.name + " 进行修改？")) {
                    /*setTimeout(function() {
                     zTree.editName(treeNode);
                     }, 0);*/
                    win=$.ligerDialog.open({
                        title:'修改',
                        url: '<%=path %>/sysOrg/query/toUpdateOrg.do?code='+treeNode.id,
                        height: 400,
                        width: 700,
                        isResize: true
                    });
                }
            }, 0);
            return false;
        }
        function beforeRemove(treeId, treeNode) {
            className = (className === "dark" ? "":"dark");
            showLog("[ "+getTime()+" beforeRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.selectNode(treeNode);
            return confirm("确认删除 节点 -- " + treeNode.name + " 吗？");
        }
        function onRemove(e, treeId, treeNode) {//删除
            // showLog("[ "+getTime()+" onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
            $.ajax({
                type:'post',//可选get
                url:'<%=path %>/sysOrg/deleteByCode.do',//这里是接收数据的PHP程序
                data:{code:treeNode.id},
                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
                success:function(content){
                    $.ligerDialog.question(content.message,function() {
                        initialize();
                    });
                },
                error:function(){
                    $.ligerDialog.question('删除失败！');
                }
            }) ;
        }
        function beforeRename(treeId, treeNode, newName, isCancel) {
            className = (className === "dark" ? "":"dark");
            showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" beforeRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
            if (newName.length == 0) {
                setTimeout(function() {
                    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                    zTree.cancelEditName();
                    alert("节点名称不能为空.");
                }, 0);
                return false;
            }
            return true;
        }
        function onRename(e, treeId, treeNode, isCancel) {
            showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
        }
        function showRemoveBtn(treeId, treeNode) {
            //return !treeNode.isFirstNode;
            return true;
        }
        function showRenameBtn(treeId, treeNode) {
            //return !treeNode.isLastNode;
            return true;

        }
        function showLog(str) {
            if (!log) log = $("#log");
            log.append("<li class='"+className+"'>"+str+"</li>");
            if(log.children("li").length > 8) {
                log.get(0).removeChild(log.children("li")[0]);
            }
        }
        function getTime() {
            var now= new Date(),
                h=now.getHours(),
                m=now.getMinutes(),
                s=now.getSeconds(),
                ms=now.getMilliseconds();
            return (h+":"+m+":"+s+ " " +ms);
        }

        var newCount = 1;
        function addHoverDom(treeId, treeNode) {
            var sObj = $("#" + treeNode.tId + "_span");
            if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
            var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
                + "' title='增加组织' onfocus='this.blur();'></span>";
            sObj.after(addStr);
            var btn = $("#addBtn_"+treeNode.tId);//*****************新增菜单
            if (btn) btn.bind("click", function(){
                /*var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                 zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
                 return false;*/
                win=$.ligerDialog.open({
                    title:'新增',
                    url: '<%=path %>/sysOrg/query/toInsertOrg.do?parentCode='+treeNode.id,
                    height: 400,
                    width: 500,
                    isResize: true
                });
            });
        };
        function removeHoverDom(treeId, treeNode) {
            $("#addBtn_"+treeNode.tId).unbind().remove();
        };
        function selectAll() {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.setting.edit.editNameSelectAll =  $("#selectAll").attr("checked");
        }
        /*默认展开  begin*/


        /*默认展开  end*/
        $(document).ready(function(){
            $.fn.zTree.init($("#treeDemo"), setting);
           // $("#selectAll").bind("click", selectAll);
        });
     function initialize(){
         $.fn.zTree.init($("#treeDemo"), setting);
     }
        var win=null;
        //关闭弹出窗口
        function closeWin(){
            win.close();
            $.fn.zTree.init($("#treeDemo"), setting);
        }
    </SCRIPT>
    <style type="text/css">
        body {font-size:25px;}
        .ztree * {padding:0; margin:0; font-size:18px; font-family: Verdana, Arial, Helvetica, AppleGothic, sans-serif}
        .ztree li span {line-height:18px; margin-right:2px}
        .ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
    </style>
</head>
<body>

<%--<h1>高级 增 / 删 / 改 节点</h1>
<h6>[ 文件路径: exedit/edit_super.html ]</h6>--%>
<div class="content_wrap">
    <div class="zTreeDemoBackground left">
        <ul id="treeDemo" class="ztree"></ul>
    </div>

</div>
</body>
</html>