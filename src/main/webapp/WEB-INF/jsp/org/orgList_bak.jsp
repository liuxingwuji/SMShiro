<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%@ include file="/common/ligerUIJS.jsp"%>
    
       <script type="text/javascript">
       var grid = null;
       var win=null;
        $(function ()
        {
           grid= $("#maingrid").ligerGrid({
           		checkbox: true,
           		isSingleCheck :true,//单选模式
                columns: [
                    { display: '组织编码', name: 'code', width: 150, align: 'cneter' },
                  { display: '组织名称', name: 'name', width: 150, align: 'cneter' },
                  
                  { display: '录入时间', name: 'createDate', width: 150, align: 'cneter', type:'date',format: 'yyyy年MM月dd',render: function (item)
                     {
                         return new Date(item.createDate).toLocaleDateString();
                     }}
                  
                ], 
                url:"<%=path %>/sysOrg/listOrg.do",
                rownumbers:true,
                root :'Rows',                       //数据源字段名
                record:'Total',
                pageSize: 1,
                pageSizeOptions :[1,10, 20, 30, 40],
                width: '99.8%', 
                height: '99%',
                //dateFormat:'yyyy-MM-dd', 
                toolbar:{items:[
                	{text:'增加',click:create,icon:'add'},
                	{ line: true },
                	{text:'修改',click:editUser,icon:'modify'},
                	{ line: true },
                	{text:'删除',click:delUser,icon:'delete'}
                	
                ]}
            }); 
        });
        
        function f_search()
        {
            grid.options.data = $.extend(true, {}, grid);
            grid.setOptions({
            parms: [
                { name: 'userName', value: $("#userName").val()}
            ]
	        });
	        //按查询条件导入到grid中
	        grid.loadData();
        }
         function itemclick(item)//测试
        {
           var del=grid.getSelected();
            alert(item.userName);
        }
       
        //新增
	    function create(){
	        win= $.ligerDialog.open({ 
	         title:'新增组织',
	         url: '<%=path %>/sysOrg/query/toInsertOrg.do',
	         height: 450, 
	         width: 700, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
	    }
	    //修改用户
    	function editUser(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要修改的数据！');
    			return;
    		}
    		win= $.ligerDialog.open({ 
	         title:'修改',
	         url: '<%=path %>/sysUser/query/edit.do?id='+del.id, 
	         height: 400, 
	         width: 500, 
	          /* buttons: [
                { text: '确定', onclick: function (item, dialog) {child.window.test(); alert(item.text); },cls:'l-dialog-btn-highlight' },
                { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
             ],  */ 
             isResize: true
            });
    	}
    	//删除用户
    	function delUser(){
    		var del=grid.getSelected();
    		if(del==null){
    			$.ligerDialog.question('请选中要删除的数据！');
    			return;
    		}
    		 if(confirm("请确认是否删除")){
	    		 $.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysUser/userDel.do',//这里是接收数据的PHP程序
	                data:{id:del.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message,function() {
	                            f_search();
	                        });
	                },
	                error:function(){
	                    $.ligerDialog.question('删除失败！');
	                }
	            }) ;
             }
    	}
    	
    	function show(id){
    		 win= $.ligerDialog.open({ 
	         title:'详细',
	         url: '<%=path %>/sysUser/query/show.do?id='+id, 
	         height: 300, 
	         width: 500, 
             isResize: true
            }); 
    	}
    	//关闭弹出窗口
    	function closeWin(){
    		win.close();
    		f_search();
    	}
    	
    	//用户赋予角色
    	function userAuthority(){
	    	var at=grid.getSelected();
	    		if(at==null){
	    			$.ligerDialog.question('请选中用户！');
	    			return;
	    		}
				win= $.ligerDialog.open({ 
		         title:'用户 角色',
		         url: '<%=path %>/sysUser/query/userAuthority.do?userName='+at.userName, 
		         height: 440, 
		         width: 300, 
	             isResize: true
	            });
    	}
    	//调出用户信息
    	function exportExcle(){
    		$.ajax({
    			type:"post",
    			url:"<%=path %>/sysUser/exportUser.do",
    			data:{userName:$("#userName").val()},
    			success : function(result) {//返回数据根据结果进行相应的处理  
                        
                    } 
    		});
    	}
    	//重置用户密码
    	function restPassword(){
    		var re=grid.getSelected();
    		if(re==null){
    			$.ligerDialog.question('请选中用户！');
    			return;
    		}
    		if(confirm("请确认是否重置用户 "+re.name+" 密码")){
    			$.ajax({
	                type:'post',//可选get
	                url:'<%=path %>/sysUser/restPassword.do',//这里是接收数据的PHP程序
	                data:{id:re.id},
	                dataType:'json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	                success:function(content){
	                    $.ligerDialog.question(content.message);
	                },
	                error:function(){
	                    $.ligerDialog.question('密码重置失败！');
	                }
	            }) ;
    		}
    	}
    	
    </script>
</head>
<body >
 <div id="searchbar" style="background-color: #C6E2FF;height: 30px;width: 100%">
    帐号：<input id="userName" type="text" />
    <input id="btnOK" type="button" value="查询" onclick="f_search()" />
    <%-- <shiro:hasPermission name="inStore:add"><input id="btnOK" type="button" value="1111" onclick="f_search()" /></shiro:hasPermission> --%>
</div> 
<div id="maingrid" style="margin: 0; padding: 0;" scrolling="no"></div>
</body>
</html>