<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>详细</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <style type="text/css">
        .titles{
            text-align: center;

        }
        span{
            color: #999;
            font: 14px Microsoft YaHei;
            margin-right: 10px;
        }
        .contents{
            font-size: 16px;
        }
    </style>
  </head>
  
  <body>
<div style="margin: 0 auto;width: 1000px;">
    <div class="titles">
       <h1>${notices.title}</h1>  <br>
        <span style="float: left">作者：${notices.author}&nbsp;&nbsp;&nbsp; 发布时间：${notices.createtime}</span><br>
    </div>
    <div class="contents">
        ${notices.contents}
    </div>
</div>
  </body>
</html>
